package tests;

import java.io.IOException;

import tankz.model.Direction;
import tankz.net.client.Client;
import tankz.net.protocol.Protocol;
import tankz.net.server.Server;

public class ClientServer {

	public static void main(String[] args) {
		final Server server = new Server("localhost", 27555);
		server.start();
		final Client client2 = new Client();
		client2.setName("TestClient");
//		client2.setServerHost(server.getHost());
//		client2.setServerPort(server.getPort());
		final Client client1 = new Client();
		client1.setName("R0flKart0ffl");
//		client1.setServerHost(server.getHost());
//		client1.setServerPort(server.getPort());

		final Client[] clients = { client2, client1 };

		//SIMPLE TEST
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (Client c : clients) {
					try {
						c.connect(server);
//						c.sendPacket(Protocol.buildConnectPacket(c.getName()));
						Thread.sleep(500);
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					for (int i = 0; i < Direction.values().length; i++) {
						try {
							c.sendPacket(Protocol.buildMovePacket(Direction.values()[i]));
							Thread.sleep(500);
						} catch (IOException e) {
							e.printStackTrace();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				for (Client c : clients) {
					c.interrupt();
				}
				server.interrupt();
			}
		}).start();

		
	}
}
