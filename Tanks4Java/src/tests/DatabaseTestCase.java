package tests;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;

import tankz.storage.Database;

public class DatabaseTestCase {

	@Test
	public void test() {
		Connection con = null;
		try {
			con = Database.getInstance().getConnection();
			con.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		assertNotNull(con);
	}

}
