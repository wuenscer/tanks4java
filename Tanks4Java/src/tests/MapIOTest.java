package tests;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import tankz.model.map.Map;
import tankz.model.map.MapIO;
import tankz.model.map.MapInfo;
import tankz.util.Utils;

public class MapIOTest {
	
	public static final File mapFile = new File(Utils.getMapsFolder().getPath()+File.separator+"campaign_1.xml");
	
	@Test
	public void testExceptions(){
		Map map;
		try {
			map = MapIO.getInstance().load(mapFile, false);
		} catch (Exception e) {
			e.printStackTrace();
			map = null;
		}
		assertNotNull(map);
	}
	
	@Test
	public void testInfo(){
		MapInfo map;
		try {
			map = MapIO.getInstance().loadInfo(mapFile, false);
			assertEquals("Map name", "Mission 2", map.getName());
			assertEquals("Map author", "Eric", map.getAuthor());
			assertEquals("Map difficulty", "Easy", map.getDifficulty());
			assertEquals("Map description", "This is the second level in the Campaign Mode", map.getDescription());
		} catch (Exception e) {
			e.printStackTrace();
			map = null;
		}
		assertNotNull(map);
	}
}
