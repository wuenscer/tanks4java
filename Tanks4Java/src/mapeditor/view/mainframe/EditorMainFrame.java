package mapeditor.view.mainframe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;

import mapeditor.view.mapframe.MapFrame;
import mapeditor.view.toolbar.EditorToolbar;
import tankz.util.ImageLoader;

public class EditorMainFrame extends JFrame {

	private static final long serialVersionUID = 3922558886850547849L;

	//private int openFrameCount = 0;
	private int xOffset = 30;
	private int yOffset = 30;
	
	private ArrayList<MapFrame> mapFrames = new ArrayList<MapFrame>();
	
	private JDesktopPane desktop = new JDesktopPane();
	private EditorToolbar toolbar = new EditorToolbar();
	
	public EditorMainFrame() {
		super("Map Editor - TankZ");
		setSize(640, 480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setIconImage(ImageLoader.getInstance().loadResourceImage("brick_edit.png"));
		desktop.setBackground(Color.DARK_GRAY);
		desktop.setLayout(null);
		setLayout(new BorderLayout());
		add(desktop, BorderLayout.CENTER);
		add(toolbar, BorderLayout.EAST);
		setJMenuBar(new MainMenuBar());
	}
	
	@Override
	public Component add(Component comp) {
		if(comp instanceof MapFrame){
			comp.setLocation(1*xOffset, 1*yOffset);
			mapFrames.add((MapFrame)comp);
			//openFrameCount++;
			return desktop.add(comp);
		}
		return super.add(comp);
	}
	
	public ArrayList<MapFrame> getMapFrames() {
		return mapFrames;
	}

	public void setMapFrames(ArrayList<MapFrame> frames) {
		this.mapFrames = frames;
	}
	
	public MapFrame getSelectedMapFrame(){
		for(MapFrame frame : mapFrames){
			if(frame.isSelected()){
				return frame;
			}
		}
		return null;
	}
	
	public int getOpenFrameCount(){
		int count = 0;
		for(MapFrame frame : mapFrames){
			if(!frame.isClosed() && !frame.isIcon()){
				count++;
			}
		}
		return count;
	}
	
	public void updateGUI(){
		MapFrame frame = null;
		if((frame = getSelectedMapFrame()) != null){
			toolbar.setMapElement(frame.getSelectedObjects()[0].getMapElement());
			frame.repaint();
		}else{
			toolbar.setMapElement(null);
		}
	}
}
