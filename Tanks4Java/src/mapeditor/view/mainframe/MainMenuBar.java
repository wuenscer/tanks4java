package mapeditor.view.mainframe;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import mapeditor.controller.EditorActions;

public class MainMenuBar extends JMenuBar {

	private static final long serialVersionUID = -111659266189879540L;

	JMenu fileMenu, editMenu, helpMenu;
	JMenuItem new_item, save_item, load_item, quit_item;
	
	public MainMenuBar(){
		createMenus();
	}

	private void createMenus() {
		fileMenu = new JMenu();
		fileMenu.setText("File");
		
		fileMenu.add(EditorActions.NEW_MAP);
		fileMenu.add(EditorActions.SAVE_MAP);
		fileMenu.add(EditorActions.SAVE_MAP_AS);
		fileMenu.add(EditorActions.LOAD_MAP);
		fileMenu.add(EditorActions.QUIT_APPLICATION);
		//and so on
		
		editMenu = new JMenu("Edit");
		editMenu.add(EditorActions.COPY_ELEMENT);
		editMenu.add(EditorActions.PASTE_ELEMENT);
		editMenu.add(EditorActions.CLEAR_ELEMENT);
		
		
		this.add(fileMenu);
		this.add(editMenu);
	}
}
