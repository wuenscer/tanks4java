package mapeditor.view.toolbar;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import tankz.util.ImageLoader;

public class TexturePreview extends JPanel {

	private static final long serialVersionUID = 1L;

	private BufferedImage previewImage = ImageLoader.getInstance().MISSING_TEXTURE;
	
	public TexturePreview(){
		
	}
	
	public TexturePreview(BufferedImage image){
		this.previewImage = image;
	}
	
	
	
	public BufferedImage getPreviewImage() {
		return previewImage;
	}

	public void setPreviewImage(BufferedImage previewImage) {
		this.previewImage = previewImage;
		repaint();
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(previewImage, 0, 0, this.getSize().width, this.getSize().height, null); 
	}
}
