package mapeditor.view.toolbar;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import tankz.model.map.Map;

public class MapPreferencesDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private JTextField mapName;
	private JTextField author;
	private JSpinner spinner_width;
	private JSpinner spinner_height;
	private JComboBox<String> difficulty_comboBox;
	private JTextArea description_area;
	
	public static int SAVE_OPTION = 1;
	public static int CANCEL_OPTION = 0;
	
	private int returnValue = CANCEL_OPTION;
	
	public MapPreferencesDialog(final Map map) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("Map Preferences");
		setModal(true);
		getContentPane().setLayout(null);
		
		JLabel lblMapPreferences = new JLabel("Map Preferences");
		lblMapPreferences.setBounds(20, 11, 114, 17);
		//lblMapPreferences.setFont(new Font("Tahoma", Font.BOLD, 14));
		getContentPane().add(lblMapPreferences);
		
		JLabel lblName = new JLabel("Name");
		//lblName.setFont(new Font("Serif", Font.PLAIN, 12));
		lblName.setBounds(20, 52, 37, 14);
		lblName.setHorizontalAlignment(SwingConstants.LEFT);
		getContentPane().add(lblName);
		
		mapName = new JTextField(map.getName());
		mapName.setBounds(62, 49, 215, 20);
		getContentPane().add(mapName);
		mapName.setColumns(10);
		
		author = new JTextField(map.getAuthor());
		author.setBounds(62, 74, 215, 20);
		getContentPane().add(author);
		author.setColumns(10);
		
		spinner_width = new JSpinner();
		spinner_width.setModel(new SpinnerNumberModel(map.getSize().width, new Integer(2), null, new Integer(1)));
		spinner_width.setBounds(62, 102, 50, 20);
		getContentPane().add(spinner_width);
		
		spinner_height = new JSpinner();
		spinner_height.setModel(new SpinnerNumberModel(map.getSize().height, new Integer(2), null, new Integer(1)));
		spinner_height.setBounds(62, 127, 50, 20);
		getContentPane().add(spinner_height);
		
		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setHorizontalAlignment(SwingConstants.LEFT);
		//lblAuthor.setFont(new Font("Serif", Font.PLAIN, 12));
		lblAuthor.setBounds(20, 77, 42, 14);
		getContentPane().add(lblAuthor);
		
		JLabel lblWidth = new JLabel("Width");
		lblWidth.setHorizontalAlignment(SwingConstants.LEFT);
		//lblWidth.setFont(new Font("Serif", Font.PLAIN, 12));
		lblWidth.setBounds(20, 103, 37, 19);
		getContentPane().add(lblWidth);
		
		JLabel lblHeight = new JLabel("Height");
		lblHeight.setHorizontalAlignment(SwingConstants.LEFT);
		//lblHeight.setFont(new Font("Serif", Font.PLAIN, 12));
		lblHeight.setBounds(20, 128, 37, 19);
		getContentPane().add(lblHeight);
		
		JLabel lblfields = new JLabel("(Fields)");
		lblfields.setHorizontalAlignment(SwingConstants.CENTER);
		//lblfields.setFont(new Font("Serif", Font.PLAIN, 12));
		lblfields.setBounds(114, 103, 50, 19);
		getContentPane().add(lblfields);
		
		JLabel label = new JLabel("(Fields)");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		//label.setFont(new Font("Serif", Font.PLAIN, 12));
		label.setBounds(114, 128, 50, 19);
		getContentPane().add(label);
		
		JLabel lblDifficulty = new JLabel("Difficulty");
		lblDifficulty.setHorizontalAlignment(SwingConstants.LEFT);
		//lblDifficulty.setFont(new Font("Serif", Font.PLAIN, 12));
		lblDifficulty.setBounds(20, 160, 60, 19);
		getContentPane().add(lblDifficulty);
		
		difficulty_comboBox = new JComboBox<String>();
		difficulty_comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Easy", "Normal", "Hard"}));
		difficulty_comboBox.setBounds(72, 160, 114, 22);
		getContentPane().add(difficulty_comboBox);
		
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setHorizontalAlignment(SwingConstants.LEFT);
		lblDescription.setBounds(20, 192, 60, 19);
		getContentPane().add(lblDescription);
		
		description_area = new JTextArea();
		description_area.setText(map.getDescription());
		description_area.setBounds(20, 212, 302, 90);
		getContentPane().add(description_area);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(223, 324, 99, 25);
		getContentPane().add(btnCancel);
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				returnValue = CANCEL_OPTION;
				dispose();
			}
		});
		
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(112, 324, 99, 25);
		getContentPane().add(btnSave);
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String errors = isInputValid();
				if(errors.isEmpty()){
					map.setName(mapName.getText());
					map.setAuthor(author.getText());
					map.setDifficulty(difficulty_comboBox.getSelectedItem().toString());
					map.setDescription(description_area.getText());
				
					Dimension newSize = new Dimension(
						Integer.parseInt(spinner_width.getValue().toString()), 
						Integer.parseInt(spinner_height.getValue().toString())
					);
					map.setSize(newSize);
					returnValue = SAVE_OPTION;
					dispose();
				}else{
					JOptionPane.showMessageDialog(MapPreferencesDialog.this, "Some inputs are invalid:\n\n"+errors, "Invalid Input", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		pack();
		setMinimumSize(new Dimension(400, 400));
		setLocationRelativeTo(null);
		setResizable(false);
	}
	
	private String isInputValid(){
		String errors = "";
		if(mapName.getText().isEmpty()){
			errors += "- The Map Name is empty!\n";
		}
		try{
			Integer.parseInt(spinner_width.getValue().toString()); 
			Integer.parseInt(spinner_height.getValue().toString());
		}catch(NumberFormatException e1){
			errors += "- the size input of the map is invalid! (width/height)\n";
		}
		
		//correct optional fields
		if(author.getText().isEmpty()){
			author.setText("Unknown");
		}
		if(description_area.getText().isEmpty()){
			description_area.setText("No description");
		}
		return errors;
	}
	
	public int getSelectedOption(){
		return returnValue;
	}
}
