package mapeditor.view.toolbar;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import mapeditor.editor.Editor;

import tankz.model.map.MapElement;
import tankz.util.ImageLoader;
import tankz.util.Utils;
import mapeditor.view.imagechooser.ImageFileView;
import mapeditor.view.imagechooser.ImageFilter;
import mapeditor.view.imagechooser.ImagePreview;
import mapeditor.view.mapframe.MapElementPanel;

public class TextureSelection extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private TexturePreview preview = new TexturePreview();
	private JComboBox<String> recent = new JComboBox<String>();
	private JButton browse = new JButton(new ImageIcon(ImageLoader.getInstance().loadResourceImage("folder.png")));
	private JButton apply = new JButton("Apply");
	private JFileChooser fc = null;

	public TextureSelection(){
		setBorder(BorderFactory.createTitledBorder("Texture"));
		setLayout(new BorderLayout());
		
		Dimension defaultSize = new Dimension(140, 200);
		Dimension preferredButtonSize = apply.getPreferredSize();
		preferredButtonSize.width = 70;
		apply.setPreferredSize(preferredButtonSize);
		apply.setHorizontalTextPosition(SwingConstants.LEFT);
		browse.setPreferredSize(preferredButtonSize);
		browse.addActionListener(browser);
		apply.addActionListener(applier);
		recent.addItem("MISSING_TEXTURE");
		recent.setSelectedIndex(0);
		recent.addItemListener(textureChangeListener);
		
		JPanel controls = new JPanel(new BorderLayout());
		controls.add(recent, BorderLayout.NORTH);
		controls.add(browse, BorderLayout.WEST);
		controls.add(apply, BorderLayout.EAST);
		
		add(preview, BorderLayout.CENTER);
		
		add(controls, BorderLayout.SOUTH);
		
		setPreferredSize(defaultSize);
		setMaximumSize(defaultSize);
	}

	
	private ItemListener textureChangeListener = new ItemListener() {
	@Override
	public void itemStateChanged(ItemEvent e) {
		if(e.getStateChange() == ItemEvent.SELECTED && e.getItem() != null){
			BufferedImage image = ImageLoader.getInstance().loadTexture(e.getItem().toString());
			preview.setPreviewImage(image);
		}
	}
	};
	
	private ActionListener applier = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try{
				for (MapElementPanel selectedElementPanel : Editor
						.getMainFrame().getSelectedMapFrame().getSelectedObjects()) {
					MapElement selectedElement = selectedElementPanel
							.getMapElement();
					selectedElement.setTexturePath(recent.getSelectedItem().toString());
				}
				Editor.getMainFrame().getSelectedMapFrame().repaint();
			}catch(NullPointerException e){
				System.err.println("Could not apply texture.\n"+e);
			}
			
		}
	}; 

	private ActionListener browser = new ActionListener() {
	@Override
	public void actionPerformed(ActionEvent arg0) {
		//Set up the file chooser.
        if (fc == null) {
            fc = new JFileChooser("textures/");
 
            //Add a custom file filter and disable the default
            //(Accept All) file filter.
            fc.addChoosableFileFilter(new ImageFilter());
            fc.setAcceptAllFileFilterUsed(false);
 
            //Add custom icons for file types.
            fc.setFileView(new ImageFileView());
 
            //Add the preview pane.
            fc.setAccessory(new ImagePreview(fc));
        }
 
        //Show it.
        int returnVal = fc.showDialog(Editor.getMainFrame(),
                                      "Choose Texture");
 
        //Process the results.
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            String path = file.getPath();
            path = path.substring(path.lastIndexOf("\\")+1);
            if(!Utils.isFileInFolder(new File("textures/"), path)){
            	path = file.getPath();
            	JOptionPane.showMessageDialog(null, "Specified textures must be located in the local game folder 'textures\\'. Otherwise the texture won't be displayed correctly.", "Texture Folder", JOptionPane.WARNING_MESSAGE);
            }else{
            	path = file.getPath().substring(file.getPath().lastIndexOf("textures\\")+9);
            }
            
            addRecentTexture(path);
        }
	}
	};
	
	public void addRecentTexture(String path){
		boolean exists = false;
		for(int i = 0; i < recent.getItemCount(); i++){
			if(recent.getItemAt(i).equals(path)){
				exists = true;
				recent.setSelectedIndex(i);
				break;
			}
		}
		if(!exists){
			recent.addItem(path);
			recent.setSelectedItem(path);
		}
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		recent.setEnabled(enabled);
		browse.setEnabled(enabled);
		apply.setEnabled(enabled);
		super.setEnabled(enabled);
	}
}
