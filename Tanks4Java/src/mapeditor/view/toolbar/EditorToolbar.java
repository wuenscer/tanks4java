package mapeditor.view.toolbar;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import mapeditor.controller.EditorActions;
import mapeditor.editor.Editor;
import mapeditor.view.mapframe.MapElementPanel;
import tankz.model.map.MapElement;

public class EditorToolbar extends JPanel {

	private static final long serialVersionUID = -3170302791047750896L;

	private JLabel mapElementInfo = new JLabel();
	private JButton editMapPreferences = new JButton("Map Preferences");

	private TextureSelection ts = new TextureSelection();

	private JCheckBox walkable = new JCheckBox("Walkable", true);
	private JCheckBox projectileCollision = new JCheckBox(
			"Projectile Collision", false);
	private JCheckBox spawnpoint = new JCheckBox("Spawnpoint", false);
	JRadioButton playerSpawn = new JRadioButton("Player");
    JRadioButton botSpawn = new JRadioButton("Bot");
    ButtonGroup spawnButtonGroup = new ButtonGroup();

	private JButton copy = new JButton("Copy");
	private JButton paste = new JButton("Paste");
	private JButton clear = new JButton("Clear");

	private ItemListener checkBoxChangeListener_walkable = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
			for (MapElementPanel selectedElementPanel : Editor
					.getMainFrame().getSelectedMapFrame().getSelectedObjects()) {
				try {
					MapElement selectedElement = selectedElementPanel
							.getMapElement();
					if(e.getStateChange() == ItemEvent.DESELECTED){
						selectedElement.setWalkable(false);
					}else{
						selectedElement.setWalkable(true);
					}
				}catch(Exception exception){
					System.err.println("Could not change walkable property.");
					exception.printStackTrace();
				}
			}
		}
	};
	
	private ItemListener checkBoxChangeListener_collision = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
			for (MapElementPanel selectedElementPanel : Editor
					.getMainFrame().getSelectedMapFrame().getSelectedObjects()) {
				try {
					MapElement selectedElement = selectedElementPanel
							.getMapElement();
					if(e.getStateChange() == ItemEvent.DESELECTED){
						selectedElement.setProjectileCollision(false);
					}else{
						selectedElement.setProjectileCollision(true);
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}
		}
	};
	
	private ItemListener checkBoxChangeListener_spawnpoint = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
			for (MapElementPanel selectedElementPanel : Editor
					.getMainFrame().getSelectedMapFrame().getSelectedObjects()) {
				try {
					MapElement selectedElement = selectedElementPanel
							.getMapElement();
					if(e.getStateChange() == ItemEvent.DESELECTED){
						selectedElement.setSpawnpoint(false);
						playerSpawn.setEnabled(false);
						botSpawn.setEnabled(false);
					}else{
						playerSpawn.setEnabled(true);
						botSpawn.setEnabled(true);
						if(selectedElement.isSpawnpoint()){
							playerSpawn.setSelected(selectedElement.isHumanSpawnpoint());
						}else{
							selectedElement.setSpawnpoint(true);
							botSpawn.setSelected(true);
						}
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}
				selectedElementPanel.repaint();
			}
		}
	};

	private ItemListener spawnTypeListener_bot = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.SELECTED){
				try{
					for (MapElementPanel selectedElementPanel : Editor
							.getMainFrame().getSelectedMapFrame().getSelectedObjects()) {
						MapElement selectedElement = selectedElementPanel
								.getMapElement();
						selectedElement.setHumanSpawnpoint(false);
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}
		}
	};
	
	private ItemListener spawnTypeListener_player = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.SELECTED){
				try{
					for (MapElementPanel selectedElementPanel : Editor
							.getMainFrame().getSelectedMapFrame().getSelectedObjects()) {
						MapElement selectedElement = selectedElementPanel
								.getMapElement();
						selectedElement.setHumanSpawnpoint(true);
					}
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}
		}
	};

	public EditorToolbar() {
		setLayout(new FlowLayout(FlowLayout.LEFT));
		setPreferredSize(new Dimension(150, 400));
		setName("Toolbar");
		
		mapElementInfo.setText(getMapElementInfo(null));
		mapElementInfo.setHorizontalTextPosition(SwingConstants.LEFT);
		mapElementInfo.setVerticalTextPosition(SwingConstants.TOP);
		add(mapElementInfo);
		
		editMapPreferences.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new MapPreferencesDialog(Editor.getMainFrame().getSelectedMapFrame().getMap()).setVisible(true);
			}
		});
		add(editMapPreferences);

		add(ts);

		walkable.addItemListener(checkBoxChangeListener_walkable);
		projectileCollision.addItemListener(checkBoxChangeListener_collision);
		spawnpoint.addItemListener(checkBoxChangeListener_spawnpoint);
		
	    botSpawn.setSelected(true);

	    //Group the radio buttons.
	    spawnButtonGroup.add(playerSpawn);
	    spawnButtonGroup.add(botSpawn);
	    
	    botSpawn.addItemListener(spawnTypeListener_bot);
		playerSpawn.addItemListener(spawnTypeListener_player);

		add(walkable);
		add(projectileCollision);
		add(spawnpoint);
		add(playerSpawn);
		add(botSpawn);

		copy.setAction(EditorActions.COPY_ELEMENT);
		paste.setAction(EditorActions.PASTE_ELEMENT);
		clear.setAction(EditorActions.CLEAR_ELEMENT);

		add(copy);
		add(paste);
		add(clear);

		setMapElement(null);
	}

	public void setMapElement(MapElement selectedElement) {
		if (selectedElement == null) {
			this.setEnabled(false);
		} else {
			mapElementInfo.setText(getMapElementInfo(selectedElement));
			walkable.setSelected(selectedElement.isWalkable());
			projectileCollision.setSelected(selectedElement
					.hasProjectileCollision());
			spawnpoint.setSelected(selectedElement.isSpawnpoint());
			ts.addRecentTexture(selectedElement.getTexturePath());
			this.setEnabled(true);
		}
	}

	private String getMapElementInfo(MapElement element) {
		String info;
		if (element == null) {
			info = "<html>Map Element:<br>X: -<br>Y: -<br>Located in map:<br>-</html>";
		} else {
			info = String.format("<html>Map Element:<br>" + "X: %d<br>"
					+ "Y: %d<br>" + "Located in map:<br>%s</html>", element
					.getLocation().x, element.getLocation().y, element
					.getParentMap().getName());
		}
		return info;
	}

	@Override
	public void setEnabled(boolean enabled) {
		mapElementInfo.setEnabled(enabled);
		editMapPreferences.setEnabled(enabled);
		walkable.setEnabled(enabled);
		projectileCollision.setEnabled(enabled);
		spawnpoint.setEnabled(enabled);
		ts.setEnabled(enabled);
		copy.setEnabled(enabled);
		paste.setEnabled(enabled);
		clear.setEnabled(enabled);
		if(enabled && spawnpoint.isSelected()){
			botSpawn.setEnabled(enabled);
			playerSpawn.setEnabled(enabled);
		}else{
			botSpawn.setEnabled(false);
			playerSpawn.setEnabled(false);
		}
	}
}
