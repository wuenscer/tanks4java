package mapeditor.view.mapframe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.ItemSelectable;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import mapeditor.editor.Editor;
import tankz.model.map.Map;
import tankz.model.map.MapElement;

public class MapFrame extends JInternalFrame implements ItemSelectable {

	private static final long serialVersionUID = -5176295662864981449L;

	private Dimension defaultSize = new Dimension(200, 200);
	private JPanel mapPanelHolder = new JPanel();
	private JScrollPane scrollPane = null;
	private ArrayList<MapElementPanel> selectedPanels = new ArrayList<MapElementPanel>();
	private Map map = null;
	private ArrayList<ItemListener> changeListener = new ArrayList<ItemListener>();
	
	private InternalFrameAdapter windowListener = new InternalFrameAdapter() {
		
		@Override
		public void internalFrameOpened(InternalFrameEvent e) {
			try{
				setSelectedPanel((MapElementPanel)mapPanelHolder.getComponentAt(10, 10));
			}catch(Exception exception){
				exception.printStackTrace();
				setSelectedPanel(null);
			}
		}
		
		@Override
		public void internalFrameClosing(InternalFrameEvent e) {
			int result = JOptionPane.showConfirmDialog(Editor.getMainFrame(), "Really want to close this map?", "Close", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(result == JOptionPane.YES_OPTION){
				dispose();
				setSelectedPanel(null);
			}
		}
	};
	
	private MouseAdapter selectionListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			//System.out.println("try to set selected element");
			try{
				MapElementPanel mep = (MapElementPanel)mapPanelHolder.getComponentAt(e.getPoint());
				if(e.isControlDown() && selectedPanels.contains(mep)){
					removeSelectedPanel(mep);
				}else if(e.isControlDown()){
					addSelectedPanel(mep);
				}else{
					setSelectedPanel(mep);
				}
			}catch(Exception exc){
				System.err.println("Could not determine map element.\n"+exc);
			}
			super.mouseClicked(e);
		}
	};
	
	public MapFrame(Map map){
		super("MapFrame", true, true, true, true);
		setMinimumSize(defaultSize);
		setLayout(new BorderLayout());
		this.setMap(map);
		if(map != null){
			setTitle(map.getName());
			mapPanelHolder.setLayout(new GridLayout(map.getSize().height, map.getSize().width));
			mapPanelHolder.setSize(map.getScreenRect().getSize());
			for(MapElement me : map.getMapElements()){
				mapPanelHolder.add(new MapElementPanel(me));
			}
			scrollPane = new JScrollPane(mapPanelHolder);
			add(scrollPane, BorderLayout.CENTER);
			mapPanelHolder.addMouseListener(selectionListener);
		}else{
			setLayout(new GridLayout(1, 1));
			add(new JLabel("There is no map specified for this frame!"));
		}
		addInternalFrameListener(windowListener);
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	@Override
	public MapElementPanel[] getSelectedObjects() {
		return selectedPanels.toArray(new MapElementPanel[selectedPanels.size()]);
	}

	@Override
	public void addItemListener(ItemListener l) {
		changeListener.add(l);
	}

	@Override
	public void removeItemListener(ItemListener l) {
		changeListener.remove(l);
	}
	
	private void addSelectedPanel(MapElementPanel mep) {
		selectedPanels.add(mep);
		mep.setSelected(true);
		Editor.getMainFrame().updateGUI();
	}

	private void removeSelectedPanel(MapElementPanel mep) {
		selectedPanels.remove(mep);
		mep.setSelected(false);
		Editor.getMainFrame().updateGUI();
	}
	
	private void setSelectedPanel(MapElementPanel selectedPanel) {
		if(selectedPanel != null){
			for(MapElementPanel panel : selectedPanels){
				panel.setSelected(false);
			}
			selectedPanels.clear();
			selectedPanel.setSelected(true);
			selectedPanels.add(selectedPanel);
		}else{
			selectedPanels.clear();
		}
		try {
			setSelected(true);
		} catch (PropertyVetoException e) {
			System.err.println("Could not select MapFrame");
			e.printStackTrace();
		}
		Editor.getMainFrame().updateGUI();
	}
}
