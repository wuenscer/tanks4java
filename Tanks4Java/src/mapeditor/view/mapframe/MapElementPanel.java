package mapeditor.view.mapframe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

import tankz.model.game.Game;
import tankz.model.map.MapElement;
import tankz.util.ImageLoader;

public class MapElementPanel extends JPanel {

	private static final long serialVersionUID = -1896298464154798634L;
	
	private MapElement mapElement = null;
	private boolean selected = false;

	public MapElementPanel(MapElement mapElement){
		this.mapElement = mapElement;
		setLayout(new BorderLayout());
		setBackground(Color.BLACK);
		setPreferredSize(new Dimension(Game.getFieldSize(), Game.getFieldSize()));
	}
	
	public MapElement getMapElement() {
		return mapElement;
	}

	public void setMapElement(MapElement mapElement) {
		this.mapElement = mapElement;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(ImageLoader.getInstance().loadTexture(this.mapElement.getTexturePath()), 0, 0, getWidth(), getHeight(), null);
		if(mapElement.isHumanSpawnpoint()){
			g.drawImage(ImageLoader.getInstance().loadResourceImage("asterisk_yellow.png"), 0, 0, getWidth(), getHeight(), null);
		}else if(mapElement.isSpawnpoint()){
			g.drawImage(ImageLoader.getInstance().loadResourceImage("asterisk_red.png"), 0, 0, getWidth(), getHeight(), null);
		}
		g.setColor(Color.BLACK);
		g.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
		g.drawString("("+mapElement.getLocation().x+" | "+mapElement.getLocation().y+")", 4, 20);
		if(isSelected()){
			g.setColor(Color.RED);
			g.drawRect(0, 0, getWidth()-1, getHeight()-1);
		}
	}
}
