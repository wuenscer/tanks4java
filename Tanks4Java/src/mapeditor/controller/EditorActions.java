package mapeditor.controller;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import mapeditor.editor.Editor;
import mapeditor.view.mapframe.MapElementPanel;
import mapeditor.view.mapframe.MapFrame;
import mapeditor.view.toolbar.MapPreferencesDialog;
import tankz.main.Main;
import tankz.model.map.Map;
import tankz.model.map.MapElement;
import tankz.model.map.MapIO;
import tankz.util.ImageLoader;
import tankz.util.Utils;

public class EditorActions {
	
	public static final AbstractAction QUIT_APPLICATION = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		{
			setEnabled(true);
			putValue(NAME, "Quit");
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK));
			putValue(ACTION_COMMAND_KEY, "QUIT_APPLICATION");
			putValue(LARGE_ICON_KEY, new ImageIcon(ImageLoader.getInstance().loadResourceImage("door_in.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
			putValue(SMALL_ICON, new ImageIcon(ImageLoader.getInstance().loadResourceImage("door_in.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
		}
	};
	
	public static final AbstractAction NEW_MAP = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		{
			setEnabled(true);
			putValue(NAME, "New Map");
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));
			putValue(ACTION_COMMAND_KEY, "NEW_MAP");
			putValue(LARGE_ICON_KEY, new ImageIcon(ImageLoader.getInstance().loadResourceImage("asterisk_yellow.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
			putValue(SMALL_ICON, new ImageIcon(ImageLoader.getInstance().loadResourceImage("asterisk_yellow.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Map newMap = new Map(new Dimension(10, 10), "New Map");
			MapPreferencesDialog dialog = new MapPreferencesDialog(newMap);
			dialog.setVisible(true);
			if(dialog.getSelectedOption() == MapPreferencesDialog.SAVE_OPTION){
				MapFrame mapFrame = new MapFrame(newMap);
				Editor.getMainFrame().add(mapFrame);
				mapFrame.pack();
				mapFrame.setVisible(true);
			}
		}
	};
	
	public static final AbstractAction SAVE_MAP = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		{
			setEnabled(true);
			putValue(NAME, "Save Map");
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
			putValue(ACTION_COMMAND_KEY, "SAVE_MAP");
			putValue(LARGE_ICON_KEY, new ImageIcon(ImageLoader.getInstance().loadResourceImage("disk.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
			putValue(SMALL_ICON, new ImageIcon(ImageLoader.getInstance().loadResourceImage("disk.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser saveDialog = new JFileChooser(Utils.getFolderOfJAR().getParentFile());
			int result = saveDialog.showSaveDialog(Editor.getMainFrame());
			if(result == JFileChooser.APPROVE_OPTION){
				File destination = saveDialog.getSelectedFile();
				Map mapToSave = Editor.getMainFrame().getSelectedMapFrame().getMap();
				try {
					MapIO.getInstance().save(mapToSave, destination);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(Editor.getMainFrame(), e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(Editor.getMainFrame(), e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
				
		}
	};
	
	public static final AbstractAction SAVE_MAP_AS = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		{
			setEnabled(false);
			putValue(NAME, "Save Map as...");
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK));
			putValue(ACTION_COMMAND_KEY, "SAVE_MAP_AS");
			putValue(LARGE_ICON_KEY, new ImageIcon(ImageLoader.getInstance().loadResourceImage("disk_multiple.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
			putValue(SMALL_ICON, new ImageIcon(ImageLoader.getInstance().loadResourceImage("disk_multiple.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JOptionPane.showMessageDialog(Editor.getMainFrame(), "Not implemented yet...", "Not Implemented", JOptionPane.INFORMATION_MESSAGE);
		}
	};
	
	public static final AbstractAction LOAD_MAP = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		{
			setEnabled(true);
			putValue(NAME, "Load Map");
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK));
			putValue(ACTION_COMMAND_KEY, "SAVE_MAP");
			putValue(LARGE_ICON_KEY, new ImageIcon(ImageLoader.getInstance().loadResourceImage("folder_brick.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
			putValue(SMALL_ICON, new ImageIcon(ImageLoader.getInstance().loadResourceImage("folder_brick.png").getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(Utils.getFolderOfJAR().getParent()+File.separator+"maps/"));
			int result = fileChooser.showOpenDialog(Main.getMainFrame());
			if(result == JFileChooser.APPROVE_OPTION){
				File mapFile = fileChooser.getSelectedFile();
				MapFrame mapFrame;
				try {
					mapFrame = new MapFrame(MapIO.getInstance().load(mapFile, true));
					Editor.getMainFrame().add(mapFrame);
					mapFrame.pack();
					mapFrame.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(Editor.getMainFrame(), "Could not load map:\n\n"+e, "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	};
	
	public static final AbstractAction CLEAR_ELEMENT = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			for (MapElementPanel selectedElementPanel : (MapElementPanel[]) Editor
					.getMainFrame().getSelectedMapFrame().getSelectedObjects()) {
				try {
					MapElement selectedElement = selectedElementPanel
							.getMapElement();
					selectedElement.clear();
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}
		}
		
		{
			putValue(NAME, "Clear");
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK));
		}
	};
	
	public static final AbstractAction COPY_ELEMENT = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try{
				Editor.copiedElement = ((MapElementPanel)Editor.getMainFrame().getSelectedMapFrame().getSelectedObjects()[0]).getMapElement();
			}catch(Exception exception){
				exception.printStackTrace();
			}
		}
		
		{
			putValue(NAME, "Copy");
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK));
		}
	};
	
	public static final AbstractAction PASTE_ELEMENT = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			for (MapElementPanel selectedElementPanel : (MapElementPanel[]) Editor
					.getMainFrame().getSelectedMapFrame().getSelectedObjects()) {
				try {
					MapElement selectedElement = selectedElementPanel
							.getMapElement();
					selectedElement.paste(Editor.copiedElement);
				}catch(Exception exception){
					exception.printStackTrace();
				}
			}
			Editor.getMainFrame().updateGUI();
		}
		
		{
			putValue(NAME, "Paste");
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK));
		}
	};
}
