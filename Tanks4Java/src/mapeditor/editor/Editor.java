package mapeditor.editor;

import tankz.model.map.MapElement;
import mapeditor.view.mainframe.EditorMainFrame;

public class Editor {
	private static EditorMainFrame mainFrame = null;
	public static MapElement copiedElement = null;
	
	public static final void main(String[] args){
		mainFrame = new EditorMainFrame();
		mainFrame.setVisible(true);
	}

	public static EditorMainFrame getMainFrame() {
		return mainFrame;
	}
}
