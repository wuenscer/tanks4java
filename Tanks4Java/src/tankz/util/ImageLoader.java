package tankz.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;


public class ImageLoader {

	/**
	 * prefix for resource images
	 */
	private static final String resourceImagePath = "/resources/images/";
	
	/**
	 * prefix for texture files (they should only be located in the "textures/" folder
	 */
	private static final String texturesPath = Utils.getFolderOfJAR().getParent()+File.separator+"textures"+File.separator;
	
	/**
	 * the instance of the {@link ImageLoader}
	 */
	private static ImageLoader instance = null;
	
	/**
	 * an image with black background and a red "ERROR" string
	 */
	public BufferedImage ERROR_IMAGE = createErrorImage();
	/**
	 * a pink and black checkered image which shall symbolize a missing texture
	 */
	public BufferedImage MISSING_TEXTURE = createMissingTexture();

	/**
	 * {@link HashMap}<br>
	 * <b>Key:</b> image path ({@link String})<br>
	 * <b>Value:</b> loaded image ({@link BufferedImage})<br>
	 * <br>
	 * Once an image is loaded it can be reused. To afford this, each loaded image
	 * will be stored in this {@link HashMap}. If an image should be loaded again,
	 * the {@link ImageLoader} will look up the path in this {@link HashMap} at first. 
	 */
	private HashMap<String, BufferedImage> images = new HashMap<String, BufferedImage>();

	private ImageLoader() {
		images.put("MISSING_TEXTURE", MISSING_TEXTURE);
		images.put("ERROR_IMAGE", ERROR_IMAGE);
	}

	public static ImageLoader getInstance() {
		if (instance == null) {
			instance = new ImageLoader();
		}
		return instance;
	}

	/**
	 * creates a an image with black background and a red "ERROR" string
	 * 
	 * @return {@link BufferedImage}
	 */
	private BufferedImage createErrorImage() {
		BufferedImage error = new BufferedImage(20, 20,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) error.getGraphics();
		g.setBackground(Color.BLACK);
		g.setColor(Color.RED);
		g.drawString("ERROR", 5, 15);
		return error;
	}

	/**
	 * creates a pink and black checkered image which shall symbolize a missing texture
	 * @return {@link BufferedImage}
	 */
	private BufferedImage createMissingTexture() {
		BufferedImage missingTexture = new BufferedImage(4, 4,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) missingTexture.getGraphics();
		g.setBackground(Color.BLACK);
		g.setColor(Color.PINK);
		g.fillRect(0, 0, 2, 2);
		g.fillRect(2, 2, 2, 2);
		g.fillRect(0, 4, 2, 2);
		return missingTexture;
	}

	/**
	 * Searches for an image in the "textures/" folder
	 *  
	 * @param path - path to the image e.g. "file.ext" or "subfolder/file.ext"
	 * @return the loaded image or the {@link #MISSING_TEXTURE} if the image could not be loaded.
	 */
	public BufferedImage loadTexture(String path) {
		path = texturesPath+path;
		BufferedImage img = images.get(path);
		if (img == null) {
			try {
				img = ImageIO.read(new File(path));
			} catch (IOException e) {
				System.err.println("Could not load image: "+path);
				img = MISSING_TEXTURE;
			}
			images.put(path, img);
		}
		return img;
	}
	
	/**
	 * Searches for an image at the specified path on the operating system
	 *  
	 * @param path - path to the image
	 * @return the loaded image or the {@link #MISSING_TEXTURE} if the image could not be loaded.
	 */
	public BufferedImage loadImage(String path) {
		BufferedImage img = images.get(path);
		if (img == null) {
			try {
				img = ImageIO.read(new File(path));
			} catch (IOException e) {
				System.err.println("Could not load image: "+path);
				img = MISSING_TEXTURE;
			}
			images.put(path, img);
		}
		return img;
	}
	
	/**
	 * Searches for an image in the "resources/images" folder within the project
	 *  
	 * @param path - path to the image e.g. "file.ext" or "subfolder/file.ext"
	 * @return the loaded image or the {@link #MISSING_TEXTURE} if the image could not be loaded.
	 */
	public BufferedImage loadResourceImage(String fileName) {
		fileName = resourceImagePath+fileName;
		BufferedImage img = images.get(fileName);
		if (img == null) {
			try {
				img = ImageIO.read(this.getClass().getResource(fileName));
				if(img == null){
					System.err.println("Could not load image: "+fileName);
					img = MISSING_TEXTURE;
				}
			} catch (Exception e) {
				System.err.println("Could not load image: "+fileName);
				e.printStackTrace();
				img = MISSING_TEXTURE;
			}
			images.put(fileName, img);
		}
		return img;
	}

}
