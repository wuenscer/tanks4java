package tankz.util;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.BooleanControl;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

import tankz.controller.console.ConsoleCommand;




public class Sounds {
	private static String audioResourcePath = "/resources/sounds/";
	private static String audioPath = Utils.getFolderOfJAR().getParent()+File.separator+"sounds"+File.separator;
	
	public static synchronized void playResourceSound(final String fileName) {
	    new Thread(new Runnable() {
	      public void run() {
	        try {
	          Clip clip = AudioSystem.getClip();
	          AudioInputStream inputStream = AudioSystem.getAudioInputStream(
	        		  Sounds.class.getResource(audioResourcePath+fileName));
	          clip.open(inputStream);
	          FloatControl control = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
	          control.setValue(ConsoleCommand.SOUND_VOLUME.getValue());
	          BooleanControl muteControl = (BooleanControl) clip.getControl(BooleanControl.Type.MUTE);
	          muteControl.setValue(!ConsoleCommand.SOUND_ENABLED.getValue());
	          
	          clip.start(); 
	        } catch (Exception e) {
	         	e.printStackTrace();
	        }
	      }
	    }).start();
	}
	
	public static synchronized void playSound(final String fileName) {
	    new Thread(new Runnable() {
	      public void run() {
	        try {
	          Clip clip = AudioSystem.getClip();
	          AudioInputStream inputStream = AudioSystem.getAudioInputStream(
	        		  new File(audioPath+fileName));
	          clip.open(inputStream);
	          clip.start(); 
	        } catch (Exception e) {
	         	e.printStackTrace();
	        }
	      }
	    }).start();
	  }
}
