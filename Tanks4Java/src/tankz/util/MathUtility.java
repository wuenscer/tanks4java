package tankz.util;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import tankz.model.Direction;

public class MathUtility {

	/**
	 * 
	 * @param impactedRectangle
	 * @param impactingRectangle
	 * @return Direction from which the impactingRectangle came from OR
	 *         <code>NULL</code> IF THE IMPACTINGRECTANGLE HITS THE EDGE OF THE
	 *         IMPACTEDRECTANGLE
	 */
	public static Direction getImpactDirection(Rectangle impactedRectangle,
			Rectangle impactingRectangle) {
		// EDGES OF ELEMENT
		int x0 = impactedRectangle.x;
		int y0 = impactedRectangle.y;
		int x1 = impactedRectangle.x + impactedRectangle.width;
		int y1 = impactedRectangle.y + impactedRectangle.height;

		Line2D north = new Line2D.Double(x0, y0, x1, y0);
		Line2D south = new Line2D.Double(x0, y1, x1, y1);
		Line2D east = new Line2D.Double(x1, y0, x1, y1);
		Line2D west = new Line2D.Double(x0, y0, x0, y1);

		// TOP
		if (impactingRectangle.intersectsLine(north)) {
			return Direction.NORTH;
		}

		// BOTTOM
		if (impactingRectangle.intersectsLine(south)) {
			return Direction.SOUTH;
		}

		// LEFT
		if (impactingRectangle.intersectsLine(west)) {
			return Direction.WEST;
		}

		// RIGHT
		if (impactingRectangle.intersectsLine(east)) {
			return Direction.EAST;
		}
		return null;
	}

	public static Direction getImpactDirection(Rectangle impactedRectangle,
			Line2D impactingLine) throws IllegalStateException {
		// EDGES OF ELEMENT
		int x0 = impactedRectangle.x;
		int y0 = impactedRectangle.y;
		int x1 = impactedRectangle.x + impactedRectangle.width;
		int y1 = impactedRectangle.y + impactedRectangle.height;

		Line2D north = new Line2D.Double(x0, y0, x1, y0);
		Line2D south = new Line2D.Double(x0, y1, x1, y1);
		Line2D east = new Line2D.Double(x1, y0, x1, y1);
		Line2D west = new Line2D.Double(x0, y0, x0, y1);

		// TOP
		if (impactingLine.intersectsLine(north)) {
			return Direction.NORTH;
		}

		// BOTTOM
		if (impactingLine.intersectsLine(south)) {
			return Direction.SOUTH;
		}

		// LEFT
		if (impactingLine.intersectsLine(west)) {
			return Direction.WEST;
		}

		// RIGHT
		if (impactingLine.intersectsLine(east)) {
			return Direction.EAST;
		}

		throw new IllegalStateException(
				"Line does not intersect a side of the rectangle");
	}

	public static Line2D getImpactLine(Rectangle impactedRectangle,
			Line2D impactingLine) {
		// EDGES OF ELEMENT
		int x0 = impactedRectangle.x;
		int y0 = impactedRectangle.y;
		int x1 = impactedRectangle.x + impactedRectangle.width;
		int y1 = impactedRectangle.y + impactedRectangle.height;

		Line2D north = new Line2D.Double(x0, y0, x1, y0);
		Line2D south = new Line2D.Double(x0, y1, x1, y1);
		Line2D east = new Line2D.Double(x1, y0, x1, y1);
		Line2D west = new Line2D.Double(x0, y0, x0, y1);

		// TOP
		if (impactingLine.intersectsLine(north)) {
			return north;
		}

		// BOTTOM
		if (impactingLine.intersectsLine(south)) {
			return south;
		}

		// LEFT
		if (impactingLine.intersectsLine(west)) {
			return west;
		}

		// RIGHT
		if (impactingLine.intersectsLine(east)) {
			return east;
		}

		throw new IllegalStateException(
				"Line does not intersect a side of the rectangle");
	}

	public static Point getIntersectionPoint(Line2D line1, Line2D line2) {
		try {
			double x1a = line1.getX1();
			double y1a = line1.getY1();
			double x2a = line1.getX2();
			double y2a = line1.getY2();

			double x1b = line2.getX1();
			double y1b = line2.getY1();
			double x2b = line2.getX2();
			double y2b = line2.getY2();

			double ma = (y2a - y1a) / (x2a - x1a);
			double mb = (y2b - y1b) / (x2b - x1b);

			double ca = -(ma * x1a - y1a);
			double cb = -(mb * x1b - y1b);

			double x = (cb - ca) / (ma - mb);
			double y = ma * x + ca;

			return new Point((int) x, (int) y);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Point2D.Double intersectLines(Line2D l, Line2D m)
			throws IllegalArgumentException {
		// Wegen der Lesbarkeit
		double x1 = l.getX1();
		double x2 = l.getX2();
		double x3 = m.getX1();
		double x4 = m.getX2();
		double y1 = l.getY1();
		double y2 = l.getY2();
		double y3 = m.getY1();
		double y4 = m.getY2();

		// Zaehler
		double zx = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2)
				* (x3 * y4 - y3 * x4);
		double zy = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2)
				* (x3 * y4 - y3 * x4);

		// Nenner
		double n = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

		// Koordinaten des Schnittpunktes
		double x = zx / n;
		double y = zy / n;

		// Vielleicht ist bei der Division durch n etwas schief gelaufen
		if (Double.isNaN(x) & Double.isNaN(y)) {
			throw new IllegalArgumentException("Schnittpunkt nicht eindeutig.");
		}
		// Test ob der Schnittpunkt auf den angebenen Strecken liegt oder
		// au�erhalb.
		if ((x - x1) / (x2 - x1) > 1 || (x - x3) / (x4 - x3) > 1
				|| (y - y1) / (y2 - y1) > 1 || (y - y3) / (y4 - y3) > 1) {
			throw new IllegalArgumentException("Schnittpunkt liegt au�erhalb.");
		}
		return new Point2D.Double(x, y);
	}

	public static Point getIntersectionPoint(Rectangle impactedRectangle,
			Line2D line) throws IllegalArgumentException {
		Line2D intersectionSide = getImpactLine(impactedRectangle, line);
		Point2D intersection = intersectLines(line, intersectionSide); // getIntersectionPoint(line,
																		// intersectionSide);

		return new Point((int) intersection.getX(), (int) intersection.getY());
	}

}
