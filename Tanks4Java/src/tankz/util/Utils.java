/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package tankz.util;

import java.io.File;
import java.io.FileFilter;
import java.net.URISyntaxException;
import java.security.CodeSource;

import javax.swing.ImageIcon;

import tankz.main.Main;


/* Utils.java is used by FileChooserDemo2.java. */
public class Utils {
    public final static String jpeg = "jpeg";
    public final static String jpg = "jpg";
    public final static String gif = "gif";
    public final static String tiff = "tiff";
    public final static String tif = "tif";
    public final static String png = "png";

    /*
     * Get the extension of a file.
     */
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }

    /** Returns an ImageIcon, or null if the path was invalid. */
    public static ImageIcon createImageIcon(String fileName) {
        return new ImageIcon(ImageLoader.getInstance().loadResourceImage(fileName));
    }
    
    public static boolean isFileInFolder(File folder, String file){
    	File checkFile = new File(folder, file);
    	if(checkFile.exists()){
    		return true;
    	}
    	File[] folders = folder.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isDirectory();
			}
		});
    	for(File f : folders){
    		if(isFileInFolder(f, file)){
    			return true;
    		}
    	}
    	return false;
    }
    
    public static boolean isFileInTexturesFolder(String path){
    	return false;
    }
    
    public static File getFolderOfJAR(){
    	String jarDir = "";
    	String notAFileError = "The following path is not a jar file";
    	File jarFile = null;
		try {
			CodeSource codeSource = Main.class.getProtectionDomain().getCodeSource();
			jarFile = new File(codeSource.getLocation().toURI().getPath());
			if(!jarFile.isFile()){
				throw new URISyntaxException(jarFile.getPath(), notAFileError);
			}
			jarFile = jarFile.getParentFile();
		} catch (URISyntaxException e1) {
			if(!e1.getMessage().startsWith(notAFileError)){
				e1.printStackTrace();
			}else{
				System.err.println(notAFileError+": "+jarFile.getPath()+"\nTrying to get path by resource...");
			}
			try {
				jarDir = ClassLoader.getSystemClassLoader().getResource(".").toURI().getPath();
				System.out.println("Resolved binary folder: "+jarDir);
				return new File(jarDir);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		return jarFile;
    }
    
    public static File getMapsFolder(){
    	return new File(Utils.getFolderOfJAR().getParent()+File.separator+"maps/");
    }
}
