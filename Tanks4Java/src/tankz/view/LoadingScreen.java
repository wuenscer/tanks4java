package tankz.view;

import java.awt.FlowLayout;

import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

import tankz.view.gamemenu.GameMenuPanel;

public class LoadingScreen extends GameMenuPanel {

	private static final long serialVersionUID = 1L;

	
	private JProgressBar progressbar = new JProgressBar(SwingConstants.HORIZONTAL);
	
	//INIT 
	{
		progressbar.setBorderPainted(true);
		progressbar.setIndeterminate(false);
		progressbar.setString("Loading...");
		progressbar.setStringPainted(true);
		progressbar.setMinimum(0);
		progressbar.setMaximum(100);
		progressbar.setValue(50);
		progressbar.setIndeterminate(true);
//		progressbar.setUI(ProgressBarUI.
	}
	
	public LoadingScreen(){
		super("Loading...", null);
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 20, 20));
		add(progressbar);
	}
}
