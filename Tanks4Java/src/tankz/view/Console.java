package tankz.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import tankz.controller.console.ConsoleCommand;
import tankz.controller.input.ConsoleKeyBindings;
import tankz.view.gamemenu.GameMenu;

public class Console extends JPanel{

	private static final long serialVersionUID = 1L;

	ArrayList<String> messages = new ArrayList<String>();
	
	private JTextArea logArea = new JTextArea();
	private JTextField inputLine = new JTextField();
	
	private JScrollPane logAreaScroll = new JScrollPane(logArea);
	
	private KeyAdapter autoCompletionListener = new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent e) {
			createAutoCompletionPopup().show(inputLine, 0, 0);
		}
	};
	
	public Console(){
		setBackground(Color.BLACK);
		setOpaque(false);
		setLayout(new BorderLayout());
		
		logArea.setBackground(GameMenu.defaultBGColor);
		logArea.setForeground(GameMenu.defaultFGColor);
		logArea.setBorder(BorderFactory.createLineBorder(GameMenu.defaultHoverColor, 2));
		logArea.setEditable(false);
		logArea.setLineWrap(true);
		logArea.setWrapStyleWord(true);
		
		inputLine.setBackground(GameMenu.defaultBGColor);
		inputLine.setForeground(GameMenu.defaultFGColor);
		inputLine.setCaretColor(GameMenu.defaultHoverColor);
		inputLine.addKeyListener(autoCompletionListener);
		inputLine.setBorder(BorderFactory.createLineBorder(GameMenu.defaultHoverColor, 2));
		ConsoleKeyBindings.applyBindingsTo(inputLine);
		
		add(logAreaScroll, BorderLayout.CENTER);
		add(inputLine, BorderLayout.SOUTH);
		
		addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {}
			
			@Override
			public void focusGained(FocusEvent e) {
				inputLine.requestFocusInWindow();
			}
		});
	}
	
	@SuppressWarnings("serial")
	private JPopupMenu createAutoCompletionPopup() {
		JPopupMenu menu = new JPopupMenu();
		String input = inputLine.getText();
		for(final ConsoleCommand<?> cmd : ConsoleCommand.getCVARS()){
			if(cmd.getName().startsWith(input)){
				menu.add(new JMenuItem(new AbstractAction(cmd.getName()) {
					@Override
					public void actionPerformed(ActionEvent e) {
						inputLine.setText(cmd.getName() + " ");
					}
				}){{
					setToolTipText(cmd.getDescription());
				}});
			}
		}
		return menu;
	}

	public void log(String message){
		logArea.append(message+"\n");
		logArea.setCaretPosition(logArea.getText().length());
	}
	
	public void logInput(){
		logArea.append(inputLine.getText()+"\n");
		logArea.setCaretPosition(logArea.getText().length());
		ConsoleCommand.parseArguments(inputLine.getText().split(" "));
		inputLine.setText("");
	}
}
