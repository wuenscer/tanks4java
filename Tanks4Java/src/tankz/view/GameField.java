package tankz.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Collections;

import javax.swing.JPanel;

import tankz.model.Projectile;
import tankz.model.Tank;
import tankz.model.game.Game;
import tankz.model.map.MapElement;

public class GameField extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private Game game = null;

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
		if(game.getActualMap()!=null) {
			this.setPreferredSize(game.getActualMap().getScreenRect().getSize());
		}
	}

	public GameField(){
		setBackground(Color.BLACK);
		setForeground(Color.BLACK);
		setMinimumSize(new Dimension(100, 100));
		setSize(getMinimumSize());
		setPreferredSize(getMinimumSize());
		setMaximumSize(getMinimumSize());
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
	}
	
	public GameField(Game game) {
		this();
		setGame(game);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if(game.getActualMap()!=null){
			for(MapElement me : game.getActualMap().getMapElements()){
				me.render((Graphics2D)g);
			}
			
			for(Tank t : game.getTanks()){
				t.render((Graphics2D)g);
			}
			
			for(Projectile p : Collections.unmodifiableList(game.PROJECTILES)){
				p.render((Graphics2D)g);
			}
			
			if(game.isGamePaused()){
				g.setColor(Color.RED);
				g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
				g.drawString(game.getPauseMessage(), 10, this.getSize().height - 100);
			}else if(game.isGameOver()){
				g.setColor(Color.RED);
				g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
				g.drawString(game.getGameOverMessage(), 10, this.getSize().height - 100);
			}
		}
	}
	
	
	
}
