package tankz.view;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import tankz.controller.console.ConsoleCommand;
import tankz.controller.input.InputListener;
import tankz.controller.input.MenuKeyBindings;
import tankz.model.game.Game;
import tankz.util.ImageLoader;
import tankz.view.gamemenu.GameMenu;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = -7541068142691441480L;

	public static final String GAMEFIELD_CARD_ID = "GAMEFIELD";
	public static final String GAMEMENU_CARD_ID = "GAMEMENU";
	public static final String CONSOLE_CARD_ID = "CONSOLE";

	private String previousComponentID = GAMEMENU_CARD_ID;
	private String currentComponentID = GAMEMENU_CARD_ID;
	
	private GameField gameField = new GameField(new Game("standard_game"));
	private Console console = new Console();
	private JPanel gameFieldCenterer = new JPanel(new GridBagLayout());
	private JScrollPane gameFieldHolder = new JScrollPane(gameFieldCenterer,
			JScrollPane.VERTICAL_SCROLLBAR_NEVER,
			JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	private InputListener inputListener = new InputListener();

	private CardLayout cardLayout = new CardLayout();

	private boolean fullscreen = false;	
	
	public MainFrame() {
		this.setBackground(Color.BLACK);
		setExtendedState(NORMAL);
    	setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.getContentPane().setLayout(cardLayout);
		this.getContentPane().setBackground(Color.BLACK);
		this.setTitle("TankZ!");
		this.setIconImage(ImageLoader.getInstance().loadResourceImage("icon.png"));

		this.addKeyListener(inputListener);
		this.addMouseMotionListener(inputListener);
		gameField.addMouseListener(inputListener);
		
		inputListener.setComponent(gameField);
		
//		inputListener.registerTo(this);
		
		gameFieldCenterer.setBackground(Color.BLACK);
		gameFieldCenterer.setForeground(Color.BLACK);
		gameFieldCenterer.add(gameField);
		gameFieldCenterer.revalidate();

		gameFieldHolder.setBackground(Color.BLACK);
		gameFieldHolder.setForeground(Color.BLACK);
		gameFieldHolder.setBorder(BorderFactory.createEmptyBorder());
		gameFieldHolder.revalidate();

		this.getContentPane().add(gameFieldHolder, GAMEFIELD_CARD_ID);
		this.getContentPane().add(GameMenu.getInstance(), GAMEMENU_CARD_ID);
		this.getContentPane().add(console, CONSOLE_CARD_ID);

		MenuKeyBindings.applyBindingsTo(getRootPane());

		setMenuVisible(true);
		
		setLocationRelativeTo(null);
		
		requestFocus();
	}

	public String getPreviousComponentID() {
		return previousComponentID;
	}

	public void setPreviousComponentID(String previousComponentID) {
		this.previousComponentID = previousComponentID;
		setDebugValue("Previous Component ID: " + getPreviousComponentID() + "\nCurrent Component ID: " + getCurrentComponentID());
	}

	public String getCurrentComponentID() {
		return currentComponentID;
	}

	public void setCurrentComponentID(String currentComponentID) {
		this.currentComponentID = currentComponentID;
		setDebugValue("Previous Component ID: " + getPreviousComponentID() + "\nCurrent Component ID: " + getCurrentComponentID());
	}

	public void setMenuVisible(boolean visible) {
		if (visible) {
			cardLayout.show(this.getContentPane(), GAMEMENU_CARD_ID);
			setPreviousComponentID(getCurrentComponentID());
			setCurrentComponentID(GAMEMENU_CARD_ID);
			GameMenu.getInstance().getGameMenuPanel().reload();
		} else {
			cardLayout.show(this.getContentPane(), GAMEFIELD_CARD_ID);
			setPreviousComponentID(getCurrentComponentID());
			setCurrentComponentID(GAMEFIELD_CARD_ID);
		}
		requestFocus();
	}
	
	public void setConsoleVisible(boolean visible) {
		if (visible) {
			cardLayout.show(this.getContentPane(), CONSOLE_CARD_ID);
			if(!getCurrentComponentID().equals(CONSOLE_CARD_ID)){
				setPreviousComponentID(getCurrentComponentID());
			}
			setCurrentComponentID(CONSOLE_CARD_ID);
			console.requestFocusInWindow();
		} else {
			cardLayout.show(this.getContentPane(), getPreviousComponentID());
			setCurrentComponentID(getPreviousComponentID());
		}
		requestFocus();
	}

	public GameField getGameField() {
		return gameField;
	}

	public Console getConsole() {
		return console;
	}

	public void setConsole(Console console) {
		this.console = console;
	}

	public InputListener getInputListener() {
		return this.inputListener;
	}

	public void centerMap() {
		gameFieldCenterer.revalidate();
		gameFieldHolder.revalidate();
	}

	public void centerView() {
		Rectangle tank = getInputListener().getTankToControl().getRect();
		Rectangle view = gameFieldHolder.getVisibleRect();
		
		view.x = (int) (tank.x - ((view.getWidth() - tank.getWidth())/2));
		view.y = (int) (tank.y - ((view.getHeight() - tank.getHeight())/2));

		gameField.scrollRectToVisible(view);
		gameField.revalidate();
		gameFieldHolder.revalidate();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (ConsoleCommand.DEBUG.getValue()) {
			g.setColor(Color.RED);
			drawString(g, debugValue, 10, 20);
		}
	}

	private String debugValue = "Debug Info";

	public void setDebugValue(String debugString) {
		this.debugValue = debugString;
		repaint();
	}

	private void drawString(Graphics g, String text, int x, int y) {
		for (String line : text.split("\n"))
			g.drawString(line, x, y += g.getFontMetrics().getHeight());
	}
	
	/**
	 * Method allows changing whether this
	 * window is displayed in fullscreen or
	 * windowed mode.
	 *
	 * @param fullscreen true = change to fullscreen,
	 *                   false = change to windowed
	 */
	public void setFullscreen(boolean fullscreen) {
	   if( this.fullscreen != fullscreen ) {
	      this.fullscreen = fullscreen;
	      if(!fullscreen) {
	    	setVisible(false);
	    	dispose();
	    	setUndecorated(false);
	    	setExtendedState(NORMAL);
	    	setSize(800,600);
	    	setLocationRelativeTo(null);
	    	setVisible(true);
	      }else{
	    	  setVisible(false);
	    	  dispose();
	    	  setUndecorated(true);
	    	  setExtendedState(MAXIMIZED_BOTH);
	    	  setVisible(true);
	      }
	      revalidate();
	      repaint();
	   }
	}
}
