package tankz.view.gamemenu;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class GameMenuPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private GameMenuPanel previousMenu = null;
	
	private JLabel titleLabel = new JLabel("DEFAULT TITLE");
	private TranslucentPanel contentPane = new TranslucentPanel();
	private GameMenuButton defaultButton = new GameMenuButton("Back", null);
	private boolean trackInHistory = true;
	
	public GameMenuPanel(String title, Action defaultButtonAction) {
		setOpaque(false);
		setLayout(new BorderLayout(30, 30));
		
		this.titleLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
		this.titleLabel.setForeground(GameMenu.defaultFGColor);
		this.titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		this.titleLabel.setText(title);
		this.defaultButton.setAction(defaultButtonAction);
		
		TranslucentPanel bottom = new TranslucentPanel();
		bottom.setLayout(new GridLayout(1, 3));
		bottom.add(new TranslucentPanel());
		bottom.add(this.defaultButton);
		bottom.add(new TranslucentPanel());
		
		add(this.titleLabel, BorderLayout.NORTH);
		add(this.contentPane, BorderLayout.CENTER);
		add(bottom, BorderLayout.SOUTH);
	}
	
	public TranslucentPanel getContentPane(){
		return this.contentPane;
	}
	
	public void setTitle(String title){
		this.titleLabel.setText(title);
	}
	
	public String getTitle(){
		return this.titleLabel.getText();
	}

	public GameMenuPanel getPreviousMenu() {
		return this.previousMenu;
	}

	public void setPreviousMenu(GameMenuPanel previousMenu) {
		this.previousMenu = previousMenu;
	}
	
	public void setDefaultButtonAction(Action action){
		this.defaultButton.setAction(action);
	}
	
	public Action getDefaultButtonAction(){
		return this.defaultButton.getAction();
	}
	
	@Override
	public Component add(Component comp) {
		return contentPane.add(comp);
	}

	public boolean isTrackInHistory() {
		return trackInHistory;
	}

	public void setTrackInHistory(boolean trackInHistory) {
		this.trackInHistory = trackInHistory;
	}

	public GameMenuButton getDefaultButton() {
		return this.defaultButton;
	}
	
	public void reload(){
		revalidate();
		repaint();
	}
}
