package tankz.view.gamemenu;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;

public class GameMenuButton extends JButton {
	private static final long serialVersionUID = 1L;
	
	private static final Dimension preferredSize = new Dimension(200, 100);
	
	public GameMenuButton(String caption) {
		super(caption);
		setText(caption);
		addMouseListener(defaultMouseListener);
		setBackground(GameMenu.defaultBGColor);
		setForeground(GameMenu.defaultFGColor);
		setBorderPainted(true);
		setBorder(BorderFactory.createSoftBevelBorder(BevelBorder.RAISED));
		setFont(new Font(Font.SANS_SERIF, Font.BOLD, 24));
		setPreferredSize(preferredSize);
	}
	
	public GameMenuButton(String caption, Action a){
		this(caption);
		setAction(a);
		setActionCommand(caption);
		setText(caption);
	}
	
	public GameMenuButton(String caption, ActionListener actionListener) {
		this(caption);
		addActionListener(actionListener);
	}

	@Override
	public void setAction(Action a) {
		if(a != null){
			super.setAction(a);
		}else{
			super.setAction(defaultAction);
		}
	}

	public Action defaultAction = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		
		{
			setEnabled(true);
			putValue(NAME, "Back");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equalsIgnoreCase("back")){
				GameMenu.getInstance().loadPrevious();
			}else{
				try{
					Class<?> x = getClass().getClassLoader().loadClass("tankz.view.gamemenu.menus."+e.getActionCommand()+"Menu");
					GameMenu.getInstance().load((GameMenuPanel) x.getDeclaredConstructor().newInstance());
				}catch(Exception e1){
					System.err.println("Could not load menu: "+e.getActionCommand()+"Menu ("+e1.getMessage()+")");
					e1.printStackTrace();
				}
			}
		}
	};
	
	MouseListener defaultMouseListener = new MouseAdapter() {		
		@Override
		public void mouseExited(MouseEvent e) {
			setBackground(GameMenu.defaultBGColor);
			repaint();
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			setBackground(GameMenu.defaultHoverColor);
			repaint();
		}
		
		public void mouseClicked(MouseEvent e) {
			setBackground(GameMenu.defaultBGColor);
			repaint();
		};
	};
}
