package tankz.view.gamemenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import tankz.util.ImageLoader;
import tankz.view.gamemenu.menus.EntryMenu;

public class GameMenu extends JPanel {

	private static final long serialVersionUID = 1L;

	public static final Color defaultBGColor = Color.DARK_GRAY;
	public static final Color defaultHoverColor = Color.decode("32768");
	public static final Color defaultFGColor = Color.WHITE;

	private static GameMenu instance = null;

	private GameMenuPanel gameMenuPanel;

	public static GameMenu getInstance() {
		if (instance == null) {
			instance = new GameMenu();
		}
		return instance;
	}

	private GameMenu() {
		setLayout(new BorderLayout());
		JLabel heading = new JLabel("TankZ", new ImageIcon(ImageLoader
				.getInstance().loadResourceImage("icon.png")),
				SwingConstants.CENTER);
		heading.setForeground(Color.WHITE);
		heading.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 58));
		add(heading, BorderLayout.NORTH);

		load(new EntryMenu());
	}

	public GameMenuPanel getGameMenuPanel() {
		return this.gameMenuPanel;
	}

	public void load(GameMenuPanel menu) {
		if (menu != null && !menu.equals(gameMenuPanel)) {
			try {
				remove(gameMenuPanel);
			} catch (NullPointerException e) {
			}
			if (gameMenuPanel != null && gameMenuPanel.isTrackInHistory()) {
				menu.setPreviousMenu(gameMenuPanel);
			} else if (gameMenuPanel != null) {
				menu.setPreviousMenu(gameMenuPanel.getPreviousMenu());
			}
			gameMenuPanel = menu;
			add(gameMenuPanel, BorderLayout.CENTER);
		}
		revalidate();
		repaint();
	}

	@Override
	public void paint(Graphics g) {
		paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());

		g.setColor(Color.decode("32768")); // dark green
		for (int y = 20; y < getHeight(); y += 20) {
			g.drawLine(0, y, getWidth(), y);
		}
		for (int x = 20; x < getWidth(); x += 20) {
			g.drawLine(x, 0, x, getHeight());
		}
		paintBorder(g);
		paintChildren(g);
	}

	public boolean loadPrevious() {
		GameMenuPanel previous = gameMenuPanel.getPreviousMenu();
		if (previous != null) {
			try {
				remove(gameMenuPanel);
			} catch (NullPointerException e) {
			}
			gameMenuPanel = previous;
			add(gameMenuPanel, BorderLayout.CENTER);
			revalidate();
			repaint();
			return true;
		}
		return false;
	}

}
