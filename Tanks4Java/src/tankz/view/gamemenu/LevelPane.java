package tankz.view.gamemenu;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.InputStream;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import tankz.main.Main;
import tankz.model.game.CampaignGame;
import tankz.model.map.MapIO;
import tankz.model.map.MapInfo;
import tankz.storage.Database;

public class LevelPane extends TranslucentPanel {
	private static final long serialVersionUID = 1L;
	
	private boolean locked 		= true;
	private String 	mapName 	= "Unknown";
	private String 	difficulty 	= "Unknown";
	private int 	enemies		= 0;
	private int 	levelID 	= -1;
	private MapInfo levelInfo 	= null;
	private Exception levelException = null; 
	
	private final JLabel label_heading = new JLabel("LOCKED");
	private final JLabel label_mapname = new JLabel("Name: -");
	private final JLabel label_enemies = new JLabel("Enemies: -");
	private final JLabel label_difficulty = new JLabel("Difficulty: -");
	
	public LevelPane(final String mapPath){
		try {
			levelID = Integer.parseInt(mapPath.substring(mapPath.lastIndexOf("_")+1, mapPath.lastIndexOf(".")));
			if(Database.getInstance().isLevelUnlocked(levelID)){
				this.locked = false;
			}
			InputStream mapIS = this.getClass().getResourceAsStream(mapPath);
			levelInfo = MapIO.getInstance().loadInfo(mapIS, false);
			mapName = levelInfo.getName();
			enemies = levelInfo.getNumberOfSpawns()-1;
			difficulty = levelInfo.getDifficulty();
		} catch (Exception e1){
			e1.printStackTrace();
			levelException = e1;
		}
		this.setLayout(new GridLayout(0, 1));
		this.setPreferredSize(new Dimension(180, 120));
		this.setLocked(locked);
		this.label_heading.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
		this.label_heading.setBackground(GameMenu.defaultBGColor);
		this.label_heading.setForeground(Color.RED);
		this.label_heading.setHorizontalAlignment(SwingConstants.CENTER);
		if(!locked){
			this.label_heading.setForeground(GameMenu.defaultFGColor);
			this.label_heading.setText("Level "+levelID);
		}
		this.label_heading.setPreferredSize(new Dimension(160, 50));
		this.label_mapname.setText("Name: "+mapName);
		this.label_mapname.setForeground(GameMenu.defaultHoverColor);
		this.label_enemies.setText("Enemies: "+enemies);
		this.label_enemies.setForeground(GameMenu.defaultHoverColor);
		this.label_difficulty.setText("Difficulty: "+difficulty);
		this.label_difficulty.setForeground(GameMenu.defaultHoverColor);
		this.setBorder(BorderFactory.createLineBorder(GameMenu.defaultBGColor, 2, true));
		this.setBackground(GameMenu.defaultBGColor);
		this.add(this.label_heading);
		this.add(this.label_mapname);
		this.add(this.label_enemies);
		this.add(this.label_difficulty);
		this.setOpaque(true);
		this.addMouseListener(new MouseListener() {
			@Override public void mouseReleased(MouseEvent e) {}
			@Override public void mousePressed(MouseEvent e) {}
			@Override public void mouseExited(MouseEvent e) {
				setBorder(BorderFactory.createLineBorder(GameMenu.defaultBGColor, 2, true));
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
			@Override public void mouseEntered(MouseEvent e) {
				if(!isLocked()){
					setBorder(BorderFactory.createLineBorder(GameMenu.defaultHoverColor, 2, true));
					setCursor(new Cursor(Cursor.HAND_CURSOR));
				}
			}
			@Override public void mouseClicked(MouseEvent e) {
				if(!isLocked()){
					if(levelInfo != null){
						try {
							InputStream mapIS = this.getClass().getResourceAsStream(mapPath);
							CampaignGame singleplayer = new CampaignGame(levelID);
							singleplayer.setMap(MapIO.getInstance().load(mapIS, false));
							Main.getMainFrame().getGameField().setGame(singleplayer);
							singleplayer.start();
						} catch (Exception e1) {
							e1.printStackTrace();
							JOptionPane.showMessageDialog(Main.getMainFrame(), "Could not load this level!\n\n"+e1, "Error", JOptionPane.ERROR_MESSAGE);
						}
					}else{
						JOptionPane.showMessageDialog(Main.getMainFrame(), "Could not load this level!\n\n"+levelException, "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
	}


	public boolean isLocked() {
		return locked;
	}


	public void setLocked(boolean locked) {
		this.locked = locked;
	}
}
