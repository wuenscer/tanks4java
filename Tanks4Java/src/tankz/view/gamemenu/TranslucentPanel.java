package tankz.view.gamemenu;

import javax.swing.JPanel;

public class TranslucentPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public TranslucentPanel(){
		setOpaque(false);
	}
}
