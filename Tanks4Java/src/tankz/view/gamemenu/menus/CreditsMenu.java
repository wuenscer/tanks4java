package tankz.view.gamemenu.menus;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import resources.messages.Messages;
import tankz.view.gamemenu.GameMenuPanel;

public class CreditsMenu extends GameMenuPanel {

	private static final long serialVersionUID = 1L;
	
	public CreditsMenu(){
		super(Messages.getString("CreditsMenu.Title"), null); //$NON-NLS-1$
		getContentPane().setLayout(new BorderLayout());
		JLabel developed = new JLabel(Messages.getString("CreditsMenu.HTMLText")); //$NON-NLS-1$
		
		developed.setForeground(Color.WHITE);
		developed.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 24));
		developed.setHorizontalAlignment(SwingConstants.CENTER);
		
		add(developed, BorderLayout.CENTER);
	}
}
