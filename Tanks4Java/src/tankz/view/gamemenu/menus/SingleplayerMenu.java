package tankz.view.gamemenu.menus;

import java.awt.GridLayout;

import resources.messages.Messages;
import tankz.view.gamemenu.GameMenuButton;
import tankz.view.gamemenu.GameMenuPanel;
import tankz.view.gamemenu.TranslucentPanel;
import tankz.controller.actions.Actions;

public class SingleplayerMenu extends GameMenuPanel {

	private static final long serialVersionUID = 1L;
	
	public SingleplayerMenu(){
		super(Messages.getString("SingleplayerMenu.Title"), null); //$NON-NLS-1$
		getContentPane().setLayout(new GridLayout(0, 3, 30, 30));
		add(new TranslucentPanel());
		add(new GameMenuButton(Messages.getString("SingleplayerMenu.btn_campaign_txt"), null)); //$NON-NLS-1$
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new GameMenuButton(Messages.getString("SingleplayerMenu.btn_load_map_txt"), Actions.LOAD_MAP)); //$NON-NLS-1$
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
	}
}
