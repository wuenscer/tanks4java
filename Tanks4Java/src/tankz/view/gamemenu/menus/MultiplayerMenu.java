package tankz.view.gamemenu.menus;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import tankz.controller.configuration.Config;
import tankz.main.Main;
import tankz.model.game.MPClientGame;
import tankz.model.game.MPServerGame;
import tankz.model.map.Map;
import tankz.model.map.MapIO;
import tankz.net.client.Client;
import tankz.net.server.Server;
import tankz.util.Utils;
import tankz.view.gamemenu.GameMenuButton;
import tankz.view.gamemenu.GameMenuPanel;
import tankz.view.gamemenu.TranslucentPanel;

public class MultiplayerMenu extends GameMenuPanel {

	private static final long serialVersionUID = 1L;

	public MultiplayerMenu(){
		super("Multiplayer", null);
		getContentPane().setLayout(new GridLayout(0, 3, 30, 30));
		add(new TranslucentPanel());
		add(new GameMenuButton("CO-OP", null));
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new GameMenuButton("Versus", null));
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new GameMenuButton("Test", new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(Utils.getFolderOfJAR()
						.getParent() + File.separator + "maps/"));
				int result = fileChooser.showOpenDialog(Main.getMainFrame());
				if (result == JFileChooser.APPROVE_OPTION) {
					File mapFile = fileChooser.getSelectedFile();
					try {
						System.out.println("Create local server");
						Server localServer = new Server("localhost", Server.MASTER_SERVER_PORT);
						localServer.start();
						System.out.println("Creating server game");
						MPServerGame serverGame = new MPServerGame(localServer);
						System.out.println("Loading map");
						Map map = MapIO.getInstance().load(mapFile, false);
						System.out.println("Setting map to server game");
						serverGame.setMap(map);
						System.out.println("Set game to server");
						localServer.setGame(serverGame);
//						serverGame.start();
						System.out.println("Create client");
						Client client = new Client();
						client.setName(Config.get(Config.PLAYERNAME));
						System.out.println("Creating client game");
						MPClientGame clientGame = new MPClientGame(client);
						System.out.println("Setting map to client game");
						clientGame.setMap(map);
						System.out.println("Setting game to client");
						client.setGame(clientGame);
						System.out.println("Show Game to user");
						Main.getMainFrame().getGameField().setGame(clientGame);
						Main.getMainFrame().setMenuVisible(false);
						System.out.println("Connecting client");
						client.connect(localServer);
						System.out.println("Connected client");
//						clientGame.start();
					} catch (Exception e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(Main.getMainFrame(),
								"An error ocurred!\n\n" + e.getMessage(), "Error",
								JOptionPane.ERROR_MESSAGE);
						Main.getMainFrame().setMenuVisible(true);
					} finally {
						Main.getMainFrame().requestFocus();
					}
				}
			}
		}));
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
	}
}
