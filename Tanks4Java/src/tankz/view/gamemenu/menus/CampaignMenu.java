package tankz.view.gamemenu.menus;

import java.awt.FlowLayout;

import tankz.view.gamemenu.GameMenuPanel;
import tankz.view.gamemenu.LevelPane;

public class CampaignMenu extends GameMenuPanel{

	private static final long serialVersionUID = 1L;
	
	public CampaignMenu(){
		super("Campaign", null);
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 20, 20));
		
		loadCampaignLevels();
	}

	private void loadCampaignLevels() {
		getContentPane().removeAll();
		int levelNo = 0;
		String path = "/resources/maps/campaign_%d.xml";
		while(this.getClass().getResource(String.format(path, levelNo)) != null){
			add(new LevelPane(String.format(path, levelNo)));
			levelNo++;
		}
	}
	
	@Override
	public void reload() {
		loadCampaignLevels();
		super.reload();
	}
}
