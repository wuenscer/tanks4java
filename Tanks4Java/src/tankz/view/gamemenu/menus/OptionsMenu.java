package tankz.view.gamemenu.menus;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import tankz.controller.configuration.Config;
import tankz.controller.console.ConsoleCommand;
import tankz.main.Main;
import tankz.view.gamemenu.GameMenu;
import tankz.view.gamemenu.GameMenuButton;
import tankz.view.gamemenu.GameMenuPanel;
import tankz.view.gamemenu.TranslucentPanel;

public class OptionsMenu extends GameMenuPanel {

	private static final long serialVersionUID = 1L;
	
	private final Action fullscreenAction = new AbstractAction("Fullscreen") {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			ConsoleCommand.FULLSCREEN.setValue(fullscreenCheckbox.isSelected());
		}
	};
	
	private final Action soundAction = new AbstractAction("Sound Enabled") {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			ConsoleCommand.SOUND_ENABLED.setValue(soundCheckbox.isSelected());
		}
	};
	
	private final ChangeListener soundVolumeChangeListener = new ChangeListener() {
		@Override
		public void stateChanged(ChangeEvent e) {
			ConsoleCommand.SOUND_VOLUME.setValue((float)volumeSlider.getValue());
		}
	};
	
	private final Action saveAction = new AbstractAction("Save") {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Config.set(Config.PLAYERNAME, playername.getText());
				Config.saveConfig();
			} catch (Exception e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(Main.getMainFrame(), "Could not save to configuration file!\n\n"+e1, "Save Configuration", JOptionPane.ERROR_MESSAGE);
			}
		}
	};
	
	private JCheckBox fullscreenCheckbox = new JCheckBox(fullscreenAction);
	private JCheckBox soundCheckbox = new JCheckBox(soundAction);
	private JSlider volumeSlider = new JSlider(SwingConstants.HORIZONTAL,
			(int)(float)ConsoleCommand.SOUND_VOLUME.getMinimumPossibleValue(),
			(int)(float)ConsoleCommand.SOUND_VOLUME.getMaximumPossibleValue(), 
			(int)(float)ConsoleCommand.SOUND_VOLUME.getValue());
	
	private JTextField playername = new JTextField();
	
	public OptionsMenu() {
		super("Options", null);
		getContentPane().setLayout(new GridLayout(0, 3, 30, 30));
		fullscreenCheckbox.setSelected(ConsoleCommand.FULLSCREEN.getValue());
		fullscreenCheckbox.setOpaque(false);
		fullscreenCheckbox.setForeground(GameMenu.defaultFGColor);
		soundCheckbox.setSelected(ConsoleCommand.SOUND_ENABLED.getValue());
		soundCheckbox.setOpaque(false);
		soundCheckbox.setForeground(GameMenu.defaultFGColor);
		volumeSlider.setOpaque(false);
		volumeSlider.setForeground(GameMenu.defaultFGColor);
		volumeSlider.addChangeListener(soundVolumeChangeListener);
		playername.setText(Config.get(Config.PLAYERNAME));
		
		add(new TranslucentPanel());
		add(fullscreenCheckbox);
		add(new TranslucentPanel());
		
		add(new TranslucentPanel());
		add(soundCheckbox);
		add(new TranslucentPanel());
		
		add(new TranslucentPanel());
		add(volumeSlider);
		add(new TranslucentPanel());
		
		add(new TranslucentPanel());
		add(playername);
		add(new TranslucentPanel());
		
		add(new TranslucentPanel());
		add(new GameMenuButton("Save", saveAction));
		add(new TranslucentPanel());
	}
}