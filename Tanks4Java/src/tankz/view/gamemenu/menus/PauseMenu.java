package tankz.view.gamemenu.menus;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import tankz.view.gamemenu.GameMenuButton;
import tankz.view.gamemenu.GameMenuPanel;
import tankz.view.gamemenu.TranslucentPanel;
import tankz.controller.actions.Actions;
import tankz.controller.input.MenuKeyBindings;

public class PauseMenu extends GameMenuPanel {

	private static final long serialVersionUID = 1L;

	public PauseMenu(){
		super("Pause", Actions.UNPAUSE_GAME);
		setTrackInHistory(false);
		getContentPane().setLayout(new GridLayout(0, 3, 30, 30));
		add(new TranslucentPanel());
		add(new GameMenuButton("End Game", Actions.END_GAME));
		add(new TranslucentPanel());
//		add(new TranslucentPanel());
//		add(new GameMenuButton("Options", null));
//		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), MenuKeyBindings.ESCAPE_ACTION_KEY);
		getActionMap().put(MenuKeyBindings.ESCAPE_ACTION_KEY, Actions.UNPAUSE_GAME);
	}
}
