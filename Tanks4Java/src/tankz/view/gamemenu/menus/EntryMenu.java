package tankz.view.gamemenu.menus;

import java.awt.GridLayout;

import tankz.view.gamemenu.GameMenuButton;
import tankz.view.gamemenu.GameMenuPanel;
import tankz.view.gamemenu.TranslucentPanel;
import tankz.controller.actions.Actions;

public class EntryMenu extends GameMenuPanel {

	private static final long serialVersionUID = 1L;

	public EntryMenu(){
		super("Menu", Actions.EXIT);
		getContentPane().setLayout(new GridLayout(0, 3, 30, 30));
		add(new TranslucentPanel());
		add(new GameMenuButton("Singleplayer", null));
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new GameMenuButton("Multiplayer", null));
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new GameMenuButton("Map Editor", Actions.LAUNCH_EDITOR));
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new GameMenuButton("Options", null));
		add(new TranslucentPanel());
		add(new TranslucentPanel());
		add(new GameMenuButton("Credits", null));
		add(new TranslucentPanel());
	}
}
