package tankz.net.protocol;

import java.awt.Point;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;

import tankz.model.Direction;

public class Protocol {

	public static final int BUFFER_LENGTH = Integer.BYTES * 4;
	public static final int NAME_LENGTH = BUFFER_LENGTH - Integer.BYTES;

	public static enum ACTION {
		CONNECT, MOVE, AIM, SHOOT, SET_CONTROLLED_TANK, SPAWN_TANK, SPAWN_BULLET, SET_TANK_NAME, SET_POS_TANK, SET_POS_PROJECTILE, SET_POS, START_GAME, STOP_GAME, PAUSE_GAME, DISCONNECT
	};

	public static DatagramPacket buildPositionPacket(int entityID, Point point) {
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_LENGTH);
		buffer.putInt(ACTION.SET_POS.ordinal());
		buffer.putInt(entityID);
		buffer.putInt(point.x);
		buffer.putInt(point.y);
		DatagramPacket pointPacket = new DatagramPacket(buffer.array(), BUFFER_LENGTH);
		return pointPacket;
	}

	public static DatagramPacket buildPointPacket(ACTION action, Point point) {
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_LENGTH);
		buffer.putInt(action.ordinal());
		buffer.putInt(point.x);
		buffer.putInt(point.y);
		DatagramPacket pointPacket = new DatagramPacket(buffer.array(), BUFFER_LENGTH);
		return pointPacket;
	}

	public static DatagramPacket buildAimPacket(Point aimPoint) {
		return buildPointPacket(ACTION.AIM, aimPoint);
	}

	public static DatagramPacket buildShootPacket(Point aimPoint) {
		return buildPointPacket(ACTION.SHOOT, aimPoint);
	}

	public static DatagramPacket buildMovePacket(Direction direction) {
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_LENGTH);
		buffer.putInt(ACTION.MOVE.ordinal());
		buffer.putInt(direction.ordinal());
		DatagramPacket movePacket = new DatagramPacket(buffer.array(), BUFFER_LENGTH);
		return movePacket;
	}

	public static boolean isConnectPacket(DatagramPacket packet) {
		return false;
	}

	public static DatagramPacket buildConnectPacket(String name) {
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_LENGTH);
		buffer.putInt(ACTION.CONNECT.ordinal());
		buffer.put(encodeName(name));
		DatagramPacket connectPacket = new DatagramPacket(buffer.array(), BUFFER_LENGTH);
		return connectPacket;
	}

	public static DatagramPacket buildDisconnectPacket(String name) {
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_LENGTH);
		buffer.putInt(ACTION.DISCONNECT.ordinal());
		buffer.put(encodeName(name));
		DatagramPacket connectPacket = new DatagramPacket(buffer.array(), BUFFER_LENGTH);
		return connectPacket;
	}

	public static DatagramPacket buildPacket(ACTION action, int[] data) {
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_LENGTH);
		buffer.putInt(action.ordinal());
		if (data != null) {
			int max = BUFFER_LENGTH / Integer.BYTES;
			if (max > data.length) {
				max = data.length;
			}
			for (int i = 1; i < max; i++) {
				buffer.putInt(data[i - 1]);
			}
		}
		DatagramPacket packet = new DatagramPacket(buffer.array(), BUFFER_LENGTH);
		return packet;
	}

	public static byte[] encodeName(String name) {
		String onlineName = name.replaceAll("[^\\p{IsAlphabetic}^\\p{IsDigit}]", "");
		onlineName = onlineName.trim();
		byte[] nameBuffer = onlineName.getBytes(Charset.forName("UTF-8"));
		int length = nameBuffer.length;
		if (length > NAME_LENGTH) {
			length = NAME_LENGTH;
		}
		return Arrays.copyOf(nameBuffer, length);
	}

	public static String decodeName(byte[] name) {
		String decname = new String(name, Charset.forName("UTF-8"));
		decname = decname.replaceAll("[^\\p{IsAlphabetic}^\\p{IsDigit}]", "");
		decname = decname.trim();
		return decname;
	}
}
