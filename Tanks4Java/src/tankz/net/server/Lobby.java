package tankz.net.server;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Lobby {
	
	public Lobby(){
		try {
			Socket socket = new Socket(Server.MASTER_SERVER_ADDRESS, Server.MASTER_SERVER_PORT);
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
