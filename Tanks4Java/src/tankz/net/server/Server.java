package tankz.net.server;

import java.awt.Point;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import tankz.model.Direction;
import tankz.model.Tank;
import tankz.model.game.Game;
import tankz.net.client.Client;
import tankz.net.protocol.Protocol;
import tankz.net.protocol.Protocol.ACTION;

public class Server extends Thread {

	public static final String MASTER_SERVER_ADDRESS = "tanks.ewproductions.de";
	public static final int MASTER_SERVER_PORT = 5599;

	private HashMap<String, Client> clients = new HashMap<String, Client>();
	private DatagramSocket socket = null;
	private int port = MASTER_SERVER_PORT;
	private String host = MASTER_SERVER_ADDRESS;

	private ArrayList<DatagramPacket> packetBuffer = new ArrayList<DatagramPacket>();

	private Game game = null;

	public HashMap<String, Client> getClients() {
		return clients;
	}

	public void setClients(HashMap<String, Client> clients) {
		this.clients = clients;
	}

	public Server() {
		setName("Server");
	}

	public Server(String host, int port) {
		this();
		setHost(host);
		setPort(port);
	}

	public int getPort() {
		return this.port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public DatagramSocket getSocket() {
		return socket;
	}

	public void setSocket(DatagramSocket socket) {
		this.socket = socket;
	}

	public ArrayList<DatagramPacket> getPacketBuffer() {
		return packetBuffer;
	}

	public void setPacketBuffer(ArrayList<DatagramPacket> packetBuffer) {
		this.packetBuffer = packetBuffer;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	@Override
	public void run() {
		try {
			socket = new DatagramSocket(port);
			System.out.println("SERVER - started at "+socket.getLocalAddress()+":"+socket.getLocalPort());
			while (!isInterrupted()) {
				try {
					byte[] buffer = new byte[Protocol.BUFFER_LENGTH];
					DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
					socket.receive(receivePacket);
					// System.out.println("Received data");
					Server.this.receive(receivePacket);
				} catch (IOException e) {
					e.printStackTrace();
					System.err.println("SERVER - Stopping Server listener");
				}
			}
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
	}

	private void receive(DatagramPacket packet) throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(Protocol.BUFFER_LENGTH);
		buffer.put(packet.getData());
		buffer.position(0);
		ACTION action = ACTION.values()[buffer.getInt()];
//		System.out.println("SERVER - Packet Action: "+action.name());
		Client client = getClient(packet);
//		if(client == null) {
//			System.out.println("SERVER - No registered client found for packet");
//		}
		if (client != null || action.compareTo(ACTION.CONNECT) == 0) {
			switch (action) {
			case MOVE:
				Direction direction = Direction.values()[buffer.getInt()];
				System.out.println("SERVER - " + client.getName() + " moved " + direction);
				break;
			case AIM:
				Point aimPoint = new Point();
				aimPoint.setLocation(buffer.getInt(), buffer.getInt());
				System.out.println("SERVER - " + client.getName() + " aimed at [" + aimPoint.x + "|" + aimPoint.y + "]");
				break;
			case SHOOT:
				Point shootPoint = new Point();
				shootPoint.setLocation(buffer.getInt(), buffer.getInt());
				System.out.println("SERVER - " + client.getName() + " shot at [" + shootPoint.x + "|" + shootPoint.y + "]");
				break;
			case CONNECT:
//				System.out.println("SERVER - New connection request...");
//				System.out.println("SERVER - Connected clients:");
//				printRegisteredClients();
				byte[] nameBuffer = new byte[buffer.remaining()];
				buffer.get(nameBuffer);
//				sendPacket(packet); - generate new packet object otherwise following activity won't be valid on same object
				sendPacket(Protocol.buildConnectPacket(new String(nameBuffer)));
				String cID = getClientID(packet);
				clients.put(cID, new Client(Protocol.decodeName(nameBuffer), packet.getAddress(), packet.getPort()));
				client = clients.get(cID);
				System.out.println("SERVER - " + client.getName()+" ("+cID+") connected");
				printRegisteredClients();
				if (getGame() != null) {
					if (getGame().getActualMap().getHumanSpawnPoints().size() > getGame().getTanks().size()) {
						Tank t = new Tank(getGame().getNextEntityID());
						t.setName(client.getName());
						t.setHuman(true);
						t.setPosition(getGame().getActualMap().getHumanSpawnPoints().get(getGame().getTanks().size())
								.getLocationOnScreen());
						getGame().getTanks().add(t);
						client.setControlledTank(t);
						sendPacket(Protocol.buildPacket(ACTION.SPAWN_TANK,
								new int[] { t.getID(), t.getPosition().x, t.getPosition().y }));
						sendPacket(Protocol.buildPacket(ACTION.SET_CONTROLLED_TANK,
								new int[] { t.getID(), t.getPosition().x, t.getPosition().y }), packet.getAddress().getHostAddress(), packet.getPort());
						if(getGame().getActualMap().getHumanSpawnPoints().size() == getGame().getTanks().size()) {
							System.out.println("SERVER - Last human spawnpoint was set - starting game now!");
							getGame().start();
						}
					} else {
						sendPacket(Protocol.buildDisconnectPacket(client.getName()),
								packet.getAddress().getHostAddress(), packet.getPort());
					}
				}
				break;
			case DISCONNECT:
				Client clientDis = clients.get(getClientID(packet));
				clients.remove(getClientID(packet));
				sendPacket(Protocol.buildDisconnectPacket(clientDis.getName()), packet.getAddress().getHostAddress(),
						packet.getPort());
				break;
			default:
				System.out.println("SERVER - Received unknown packet from client " + packet.getAddress().getHostAddress() + ":"
						+ packet.getPort() + " ("+client.toString()+")");
				break;
			}
		} else {
			System.out.println("SERVER - Received packet from unknown client " + packet.getAddress().getHostAddress() + ":"
					+ packet.getPort() + " (unknown)");
			printRegisteredClients();
		}
	}

	private void printRegisteredClients() {
		System.out.print("SERVER - Registered clients: [");
		for(Client c : clients.values()) {
			System.out.print(c.toString() + "; ");
		}
		System.out.println("]");
	}

	public String getClientID(DatagramPacket packet) {
		String clientID = packet.getAddress().getHostAddress() + ":" + packet.getPort();
//		System.out.println("Generated client ID from packet: " + clientID);
		return clientID;
	}

	public Client getClient(DatagramPacket packet) {
		return clients.get(getClientID(packet));
	}

	public void sendPacket(DatagramPacket packet) throws IOException {
//		System.out.println("SERVER - Sending packet to all clients");
		for (Client client : clients.values()) {
			System.out.println("SERVER - Sending packet to "+client.toString());
			sendPacket(packet, client.getLocalAddress().getHostAddress(), client.getLocalPort());
		}
	}

	public void sendPacket(DatagramPacket packet, String host, int port) throws IOException {
		packet.setAddress(InetAddress.getByName(host));
		packet.setPort(port);
		socket.send(packet);
	}

	public static void main(String[] args) {
		Server server = new Server();
		server.start();
	}

	@Override
	public void interrupt() {
		super.interrupt();
		socket.close();
	}
}
