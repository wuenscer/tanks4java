package tankz.net.client;

import java.awt.Point;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import tankz.main.Main;
import tankz.model.Tank;
import tankz.model.game.Game;
import tankz.model.game.GameEntity;
import tankz.net.protocol.Protocol;
import tankz.net.protocol.Protocol.ACTION;
import tankz.net.server.Server;

public class Client extends Thread {

	private String serverHost = Server.MASTER_SERVER_ADDRESS;
	private int serverPort = Server.MASTER_SERVER_PORT;
	private InetAddress localAddress = null;
	private int localPort = 0;

	private ArrayList<DatagramPacket> packetBuffer = new ArrayList<DatagramPacket>();

	private DatagramSocket socket = null;

	private Game game = null;

	private Tank controlledTank = null;

	public Client() {
		setName("Client");
	}

	public Client(String name) {
		setName(name);
	}
	
	public Client(String name, InetAddress address, int port) {
		setName(name);
		setLocalAddress(address);
		setLocalPort(port);
	}
	
	public Client(String name, String serverAddress, int serverPort) {
		setName(name);
		setServerHost(serverAddress);
		setServerPort(serverPort);
	}

	public void sendPacket(DatagramPacket packet, String host, int port) throws IOException {
		System.out.println("CLIENT - Sending packet to "+host+":"+port);
		packet.setAddress(InetAddress.getByName(host));
		packet.setPort(port);
		initSocket();
		socket.send(packet);
	}

	public void sendPacket(DatagramPacket packet) throws IOException {
		sendPacket(packet, serverHost, serverPort);
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public InetAddress getLocalAddress() {
		return localAddress;
	}

	public void setLocalAddress(InetAddress localAddress) {
		this.localAddress = localAddress;
	}

	public int getLocalPort() {
		return localPort;
	}

	public void setLocalPort(int localPort) {
		this.localPort = localPort;
	}

	public ArrayList<DatagramPacket> getPacketBuffer() {
		return this.packetBuffer;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Tank getControlledTank() {
		return controlledTank;
	}

	public void setControlledTank(Tank controlledTank) {
		this.controlledTank = controlledTank;
	}

	@Override
	public void run() {
		initSocket();

		System.out.println("CLIENT - Starting client listener");
		while (!isInterrupted()) {
			try {
				byte[] buffer = new byte[Protocol.BUFFER_LENGTH];
				DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
				socket.receive(receivePacket);
				// System.out.println(getName() + ": Received data");
				Client.this.receive(receivePacket);
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("CLIENT - Interrupting client listener");
				interrupt();
			}
		}
		System.err.println("CLIENT - Stopped client listener");
	}

	private void initSocket() {
		if (socket == null) {
			try {
				socket = new DatagramSocket();
				System.out.println("CLIENT - Initiated client socket ("+socket.getLocalAddress()+":"+socket.getLocalPort()+").");
//				start();
			} catch (SocketException e1) {
				e1.printStackTrace();
				System.err.println("CLIENT - Could not initialize socket");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void receive(DatagramPacket packet) {
		System.out.println("CLIENT - Received packet");
		packetBuffer.add(packet);
		if (packetBuffer.size() > 60) {
			packetBuffer.remove(0);
		}
		ByteBuffer buffer = ByteBuffer.allocate(Protocol.BUFFER_LENGTH);
		buffer.put(packet.getData());
		buffer.position(0);
		ACTION action = ACTION.values()[buffer.getInt()];
		System.out.println("CLIENT - Received " + action.name() + " packet");
		switch (action) {
		case START_GAME:
			System.out.println("CLIENT - received START_GAME Action Packet");
			getGame().start();
			packetBuffer.remove(packet);
			break;
		case SPAWN_TANK:
			System.out.println("CLIENT - received SPAWN_TANK Action Packet");
			int tankID = buffer.getInt();
			if (getGame() != null) {
				Tank t = new Tank(tankID);
				t.setName("Tank" + tankID);
				t.setHuman(false);
				t.setPosition(new Point(buffer.getInt(), buffer.getInt()));
				getGame().getTanks().add(t);
				System.out.println("CLIENT - spawned tank "+t.getName()+" ("+t.getPosition().x+"|"+t.getPosition().y+")");
			}
			packetBuffer.remove(packet);
			break;
		case SET_CONTROLLED_TANK:
			System.out.println("CLIENT - received SET_CONTROLLED_TANK Action Packet");
			tankID = buffer.getInt();
			Tank tank = getGame().getTankByID(tankID);
			Main.getMainFrame().getInputListener().setTankToControl(tank);
			tank.setName(getName());
			tank.setHuman(true);
			System.out.println("CLIENT - Set Tank "+tank.toString()+" to be the controlled tank");
			packetBuffer.remove(packet);
			break;
		case CONNECT:
			byte[] nameBuffer = new byte[buffer.remaining()];
			buffer.get(nameBuffer);
			String name = Protocol.decodeName(nameBuffer);
			System.out.println("CLIENT - Player " + name + " connected");
			packetBuffer.remove(packet);
			break;
		case DISCONNECT:
			nameBuffer = new byte[buffer.remaining()];
			buffer.get(nameBuffer);
			name = Protocol.decodeName(nameBuffer);
			System.out.println("CLIENT - " + name + " disconnected");
			packetBuffer.remove(packet);
			break;
		case SET_POS:
			System.out.println("CLIENT - Received SET POS packet");
			int entityID = buffer.getInt();
			int posX = buffer.getInt();
			int posY = buffer.getInt();
			GameEntity entity;
			try {
				entity = getGame().getEntityByID(entityID);
				entity.setPosition(new Point(posX, posY));
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		default:
			System.out.println("CLIENT - Received unknown packet from server " + packet.getAddress().getHostAddress() + ":"
					+ packet.getPort() + ": " + Arrays.toString(buffer.array()));
			break;
		}

	}

	@Override
	public void interrupt() {
		super.interrupt();
		socket.close();
	}

	public void connect(Server server) throws IOException {
		initSocket();
		socket.connect(InetAddress.getByName(server.getHost()), server.getPort());
		setServerHost(server.getHost());
		setServerPort(server.getPort());
		start();
		sendPacket(Protocol.buildConnectPacket(getName()));
	}
	
	@Override
	public String toString() {
		String representation = getName();
		if(getLocalAddress()!=null) {
			 representation += "("+getLocalAddress().getHostAddress()+":"+getLocalPort()+")";
		}
		return representation;
	}
}
