package tankz.main;

import java.io.IOException;

import tankz.controller.configuration.Config;
import tankz.controller.console.ConsoleCommand;
import tankz.view.MainFrame;

public class Main {

	private static MainFrame mainFrame = null;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//LOAD CONFIG FILE
		try {
			Config.loadConfig();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		mainFrame = new MainFrame();
		
		Config.applyProperties();
		
		//GET ARGUMENTS
		ConsoleCommand.parseArguments(args);
//		String settings = ConsoleCommand.getCVARS_ShortString();
//		System.out.println(settings);
//		Main.getMainFrame().getConsole().log(settings);
		
		//INIT GUI AND GAME
		if(ConsoleCommand.GUI_ENABLED.getValue()){
			mainFrame.setVisible(true);
			mainFrame.setFullscreen(ConsoleCommand.FULLSCREEN.getValue());
		}
	}

	public static MainFrame getMainFrame(){
		return mainFrame;
	}
}