package tankz.controller.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import tankz.controller.console.ConsoleCommand;
import tankz.util.Utils;

/**
 * Provides methods to load and store configuration values into java properties
 * file
 * 
 * @author Eric W�nsche
 * 
 */
public class Config {

	private static Properties configProps = new Properties();
	
	public static final String UI_FULLSCREEN = "game.ui.fullscreen";
	public static final String UI_ENABLED = "game.ui.enabled";
	public static final String SOUND_ENABLED = "game.sound.enabled";
	public static final String SOUND_VOLUME = "game.sound.volume";
	public static final String AI_ENABLED = "game.ai.enabled";
	public static final String AI_DEBUG = "game.ai.debug";
	public static final String DEBUG_ENABLED = "game.debug.enabled";
	public static final String FPS_LIMIT = "game.ui.fps.limit";
	public static final String FPS_LIMITED = "game.ui.fps.limited";
	public static final String TICKRATE = "game.tickrate";
	public static final String PLAYERNAME = "game.playername";

	/**
	 * Path to the standard configuration properties file
	 */
	public static final String configFilePath = Utils.getFolderOfJAR().getParent() + File.separator + "config"
			+ File.separator + "config.properties";

	/**
	 * Tries to load the config XML file from its default path. It separates all
	 * entries into commands and passes them to the CVARS class to load the values
	 * into the application
	 * 
	 * @throws JDOMException
	 *             if the XML document isn't valid
	 * @throws IOException
	 *             if there is an issue regarding the access of the XML file
	 */
	public static void loadConfig() throws IOException {
		File configFile = new File(configFilePath);

		if (configFile.exists()) {
			try {
				FileInputStream fis = new FileInputStream(configFile);
				configProps.load(fis);
				fis.close();
			} catch (IOException e) {
				System.err.println("Could not load default properties file (" + configFile.getAbsolutePath() + ")");
				e.printStackTrace();
				loadDefaultProps();
			}
		} else {
			loadDefaultProps();
		}
		//applyProperties();
	}
	
	public static void applyProperties() {
		String args = "";
		for (String key : configProps.stringPropertyNames()) {
			args += "-" + key + " " + configProps.getProperty(key) + " ";
		}
		ConsoleCommand.parseArguments(args.split(" "));
	}

	/**
	 * Tries to store all configuration values into the default configuration
	 * properties file.
	 * 
	 * @throws IOException
	 *             if there is an issue regarding the access of the XML file
	 * 
	 */
	public static void saveConfig() throws IOException {
		File configFile = new File(configFilePath);
		if (!configFile.exists()) {
			configFile.createNewFile();
		}
		FileOutputStream fos = new FileOutputStream(configFile);
		configProps.store(fos, "TANKZ PROPERTIES");
	}

	/**
	 * Restores all default configuration values and writes them to the default
	 * configuration file.
	 * 
	 * @throws IOException 
	 */
	public static void restoreDefaultConfig() throws IOException {
		loadDefaultProps();
		saveConfig();
	}
	
	public static void loadDefaultProps() throws IOException {
		System.out.println("Load default properties...");
		configProps.load(Config.class.getResourceAsStream("/config/config.properties"));
	}
	
	public static String get(String key) {
		return configProps.getProperty(key);
	}
	
	public static void set(String key, String value) {
		configProps.setProperty(key, value);
	}
}
