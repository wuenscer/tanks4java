package tankz.controller.console;

import java.awt.ItemSelectable;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import tankz.main.Main;
import tankz.model.map.MapIO;
import tankz.util.Utils;

/**
 * This class represents a command or variable, that can have some values. A
 * {@link ConsoleCommand} has a name, which represents the command. So, if we
 * have a command that would be typed into the console with "-status" its name
 * is "-status" (dash included!). A command / variable can have a value. The
 * value can be one of some defined possible values, or an undefined value. In
 * case we have an enable/disable command like "-sound_enabled", we only want it
 * to hold true and false (or maybe 0 and 1). Therefore we define those values
 * as possible values and the command won't accept any other values. In case we
 * have an echo command, where you want to pass any kind of text, we won't
 * define any values and set "undefinedValues" to true.
 * 
 * A {@link ConsoleCommand} fires an ItemEvent to all ItemListeners, if the
 * value changed.
 * 
 * Any command or variable is instantiated in the {@link CVARS} class.
 * 
 * @author Eric W�nsche
 * 
 * @param <T>
 *            Type of the value that can be set to the variable/command (String
 *            for text input; Integer/int/float for numbers etc.)
 */
public class ConsoleCommand<T> implements ItemSelectable {

	/**
	 * list of all initialized {@link ConsoleCommand}s
	 */
	private static ArrayList<ConsoleCommand<?>> cvars = new ArrayList<ConsoleCommand<?>>();
	
	public static ArrayList<ConsoleCommand<?>> getCVARS() {
		if(cvars.isEmpty()) {
			initCVARS();
		}
		return cvars;
	}
	
	public static final String CMDKEY = "-";

	/**
	 * {@link ConsoleCommand} to enable or disable Artificial Intelligence of
	 * CPU controlled tanks
	 */
	public final static ConsoleCommand<Boolean> AI_ENABLED = new ConsoleCommand<Boolean>(
			Boolean.class, "game.ai.enabled", true, false) {
		{
			setStoreInConfig(false);
			setDescription("Enable (true) or disable (false) the artificial intelligence");
		}
	};

	/**
	 * {@link ConsoleCommand} to enable or disable the user interface
	 */
	public final static ConsoleCommand<Boolean> GUI_ENABLED = new ConsoleCommand<Boolean>(
			Boolean.class, "game.ui.enabled", true, false) {
		{
			setStoreInConfig(false);
			setDescription("Enable (true) or disable (false) the whole user interface");
		}
	};

	/**
	 * {@link ConsoleCommand} to enable or disable fullscreen mode
	 */
	public final static ConsoleCommand<Boolean> FULLSCREEN = new ConsoleCommand<Boolean>(
			Boolean.class, "game.ui.fullscreen", true, false) {
		{
			setStoreInConfig(true);
			setDescription("Enable (true) or disable (false) fullscreen mode");
			addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					Main.getMainFrame().setFullscreen(getValue());
				}
			});
		}
	};

	/**
	 * {@link ConsoleCommand} to enable or disable the display of debug messages
	 */
	public final static ConsoleCommand<Boolean> DEBUG = new ConsoleCommand<Boolean>(
			Boolean.class, "game.debug.enabled", false, true) {
		{
			setStoreInConfig(false);
			setDescription("Enable (true) or disable (false) debug messages on the main frame");
		}
	};

	/**
	 * {@link ConsoleCommand} to set the level of debugging AI actions
	 */
	public final static ConsoleCommand<String> DEBUG_AI = new ConsoleCommand<String>(
			String.class, "game.ai.debug", "none", "all", "console", "visual") {
		{
			setStoreInConfig(false);
			setDescription("Define the level of debugging for the artificial intelligence");
		}
	};

	/**
	 * {@link ConsoleCommand} to enable or disable sound output
	 */
	public final static ConsoleCommand<Boolean> SOUND_ENABLED = new ConsoleCommand<Boolean>(
			Boolean.class, "game.sound.enabled", true, false) {
		{
			setStoreInConfig(true);
			setDescription("Enable (true) or disable (false) the sound");
		}
	};

	/**
	 * {@link ConsoleCommand} to adjust the volume of sound output
	 */
	public final static ConsoleCommand<Float> SOUND_VOLUME = new ConsoleCommand<Float>(
			Float.class, "game.sound.volume", (float) 0.0) {
		{
			ArrayList<Float> values = new ArrayList<Float>();
			for (int i = -80; i < 7; i++) {
				values.add((float) i);
			}
			setPossibleValues(values);
			setValue((float)0.0);
			setStoreInConfig(true);
			setDescription("Define the sound volume");
		}
	};

	/**
	 * {@link ConsoleCommand} which will list all commands and variables with
	 * their current value
	 */
	public final static ConsoleCommand<String> STATUS = new ConsoleCommand<String>(
			String.class, "game.status", "status") {
		{
			setStoreInConfig(false);
			setDescription("Print all variables with their values");
		}
	};

	/**
	 * {@link ConsoleCommand} which will load the given map name if possible
	 */
	public final static ConsoleCommand<String> LOAD_MAP = new ConsoleCommand<String>(
			String.class, "game.map", "map_name") {
		{
			setStoreInConfig(false);
			setDescription("Provide a map name of a map that is included in the external 'maps' folder to load and start it.");
			setUndefinedValues(true);
			addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent arg0) {
					try {
						File mapFile = new File(Utils.getMapsFolder().getPath()
								+ File.separator + getValue() + ".xml");
						Main.getMainFrame().getGameField().getGame().setMap(MapIO.getInstance().load(mapFile,
								false));
						Main.getMainFrame().getGameField().getGame().start();
					} catch (Exception e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(Main.getMainFrame(),
								"An error ocurred!\n\n" + e.getMessage(),
								"Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
	};
	
	/**
	 * Adds all {@link ConsoleCommand}s to the local cvars list
	 */
	private static void initCVARS() {
		cvars.add(AI_ENABLED);
		cvars.add(GUI_ENABLED);
		cvars.add(FULLSCREEN);
		cvars.add(DEBUG);
		cvars.add(DEBUG_AI);
		cvars.add(SOUND_ENABLED);
		cvars.add(SOUND_VOLUME);
		cvars.add(STATUS);
		cvars.add(LOAD_MAP);
	}
	
	/**
	 * Name of the command / variable - representation in console: -name
	 */
	private String name = "";

	/**
	 * Description of the command (what is the use of it)
	 */
	private String description = "No description";

	/**
	 * A list of values that are accepted by this command. This list is only
	 * being used, if undefinedValues is set to false
	 */
	private ArrayList<T> possibleValues = new ArrayList<T>();

	/**
	 * States whether the command/variable allows undefined input or not
	 */
	private boolean undefinedValues = false;

	/**
	 * States whether this command and its current value should be stored in the
	 * configuration file or not
	 */
	private boolean storeInConfig = true;

	/**
	 * The current value of the command/variable
	 */
	private T value = null;

	/**
	 * Class/Type of the value
	 */
	private Class<T> type = null;

	/**
	 * List of item listeners that should receive an {@link ItemEvent} if the
	 * value of the command changes or is reset
	 */
	private ArrayList<ItemListener> itemlisteners = new ArrayList<ItemListener>();

	/**
	 * Constructor for a {@link ConsoleCommand} with certain possible values.
	 * The command will be initialized with the first value of the
	 * "possible value" list.
	 * 
	 * If you want to instantiate a {@link ConsoleCommand} that can have
	 * undefined values, you also need to provide at least one possible value
	 * for intialization purposes. Afterwards you can set the command to accept
	 * any value by using {@link ConsoleCommand#setUndefinedValues(true)}
	 * 
	 * @param type
	 *            - the type/class of the values that are accepted by this
	 *            command (String.class / Integer.class / byte / etc. - see
	 *            {@link ConsoleCommand#interpretValue(String)} for simply
	 *            supported classes
	 * @param name
	 *            - the name of the command (and representation)
	 * @param possibleValues
	 *            - comma seperated list of values, that should be accepted by
	 *            the command
	 * @throws IllegalArgumentException
	 *             if no possible value is given
	 */
	public ConsoleCommand(Class<T> type, String name, T... possibleValues)
			throws IllegalArgumentException {
		if (possibleValues == null || possibleValues.length == 0) {
			throw new IllegalArgumentException(
					"No possible value for the command was given!");
		}
		this.type = type;
		for (T value : possibleValues) {
			this.possibleValues.add(value);
		}
		this.value = this.possibleValues.get(0);
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<T> getPossibleValues() {
		return possibleValues;
	}

	public void setPossibleValues(ArrayList<T> possibleValues) {
		this.possibleValues = possibleValues;
	}

	public T getValue() {
		return value;
	}

	/**
	 * Sets the given value to the command, if the value is contained in the
	 * "possible value" list or the command is declared to accept undefined
	 * values. After the value was set, the command will fire an
	 * {@link ItemEvent} to its listeners.
	 * 
	 * @param value
	 *            the value that should be set
	 * @throws IllegalArgumentException
	 *             if the given value is not defined in the "possible value"
	 *             list
	 */
	public void setValue(T value) throws IllegalArgumentException {
		if (hasUndefinedValues() || possibleValues.contains(value)) {
			// if (this.value == null || !this.value.equals(value)) {
			this.value = value;
			for (ItemListener il : itemlisteners) {
				ItemEvent ie = new ItemEvent(this, 0, value, ItemEvent.SELECTED);
				il.itemStateChanged(ie);
			}
			// }
		} else {
			throw new IllegalArgumentException(
					"Given value ("
							+ value
							+ ") is not a possible one for this variable (possible values: "
							+ possibleValues + ").");
		}
	}

	public boolean hasUndefinedValues() {
		return undefinedValues;
	}

	public void setUndefinedValues(boolean unlimitedValues) {
		this.undefinedValues = unlimitedValues;
	}

	public boolean isStoreInConfig() {
		return storeInConfig;
	}

	public void setStoreInConfig(boolean storeInConfig) {
		this.storeInConfig = storeInConfig;
	}

	/**
	 * Returns a string representation of this command with following pattern: <br>
	 * <i>name</i> (<i>Description</i>); Value: <i>value</i>; Possible Values:
	 * [Any value of type <i>type</i> is possible] or [value1, value2, ...]
	 */
	@Override
	public String toString() {
		String represent = "";
		represent += "Key: " + getName() + "; ";
		represent += "Description: " + getDescription() + "; ";
		represent += "Value: " + getValue() + "; ";
		represent += "Possible Values: ";
		if (hasUndefinedValues()) {
			represent += "Any value of type '" + this.type.getSimpleName()
					+ "' is possible;";
		} else {
			represent += getPossibleValues() + ";";
		}
		return represent;
	}

	/**
	 * @return a string representation of this command with following pattern: <br>
	 *         <i>name</i> <i>value</i>;
	 */
	public String toShortString() {
		String represent = "";
		represent += getName();
		represent += " " + getValue() + ";";
		return represent;
	}

	/**
	 * Tries to interpret a string as a value, based upon class casting methods
	 * based on the type variable of the {@link ConsoleCommand}. <br>
	 * <br>
	 * Supported Casts of Strings:<br>
	 * <ul>
	 * <li>Boolean</li>
	 * <li>String</li>
	 * <li>Integer</li>
	 * <li>Float</li>
	 * <li>Double</li>
	 * <li>Long</li>
	 * </ul>
	 * 
	 * @param value
	 *            a compatible value for this command written as string
	 * @return the interpreted value
	 * @throws IllegalArgumentException
	 *             if the given string could not be interpreted
	 */
	@SuppressWarnings("unchecked")
	public T interpretValue(String value) throws IllegalArgumentException {
		Object returnValue = null;
		boolean interpreted = true;

		if (type == Boolean.class) {
			if (value.equalsIgnoreCase("true")) {
				returnValue = true;
			} else if (value.equalsIgnoreCase("false")) {
				returnValue = false;
			} else {
				interpreted = false;
			}
		} else if (type == String.class) {
			if (hasUndefinedValues()) {
				returnValue = value;
			} else if (possibleValues.contains(value)) {
				returnValue = value;
			} else {
				interpreted = false;
			}
		} else if (type == Integer.class) {
			try {
				returnValue = Integer.parseInt(value);
				if (!hasUndefinedValues()
						&& !possibleValues.contains(returnValue)) {
					interpreted = false;
				}
			} catch (NumberFormatException e) {
				interpreted = false;
			}
		} else if (type == Float.class) {
			try {
				returnValue = Float.parseFloat(value);
				if (!hasUndefinedValues()
						&& !possibleValues.contains(returnValue)) {
					interpreted = false;
				}
			} catch (NumberFormatException e) {
				interpreted = false;
			}
		} else if (type == Double.class) {
			try {
				returnValue = Double.parseDouble(value);
				if (!hasUndefinedValues()
						&& !possibleValues.contains(returnValue)) {
					interpreted = false;
				}
			} catch (NumberFormatException e) {
				interpreted = false;
			}
		} else if (type == Long.class) {
			try {
				returnValue = Long.parseLong(value);
				if (!hasUndefinedValues()
						&& !possibleValues.contains(returnValue)) {
					interpreted = false;
				}
			} catch (NumberFormatException e) {
				interpreted = false;
			}
		}

		if (!interpreted) {
			throw new IllegalArgumentException("Could not interpret value '"
					+ value + "' for parameter " + getName()
					+ ". Use one of the possible values: " + possibleValues);
		} else {
			return (T) returnValue;
		}
	}

	/**
	 * Sorts the "possible value" list from low to high and returns the lowest
	 * or highest possible value.
	 * <ul>
	 * <li>Use SwingConstants.TOP for the maximum value.</li>
	 * <li>Use SwingConstants.BOTTOM for the minimum value.</li>
	 * </ul>
	 * 
	 * This operation is only possible if the list is holding one of the
	 * following types:<br>
	 * <ul>
	 * <li>String</li>
	 * <li>Integer</li>
	 * <li>Float</li>
	 * <li>Double</li>
	 * <li>Long</li>
	 * </ul>
	 * Otherwise the function will return the actual value of this command.
	 * 
	 * @param TOPorBOTTOM
	 *            SwingConstant that defines whether max or min value should be
	 *            returned
	 * @return maximum or minimum possible value
	 */
	@SuppressWarnings("unchecked")
	private T getValue(int TOPorBOTTOM) {
		T min;
		T max;
		if (hasUndefinedValues()) {
			return getValue();
		} else {
			if (type == String.class) {
				ArrayList<String> copy = (ArrayList<String>) possibleValues
						.clone();
				Collections.sort(copy);
				min = (T) copy.get(0);
				max = (T) copy.get(copy.size() - 1);
			} else if (type == Integer.class) {
				ArrayList<Integer> copy = (ArrayList<Integer>) possibleValues
						.clone();
				Collections.sort(copy);
				min = (T) copy.get(0);
				max = (T) copy.get(copy.size() - 1);
			} else if (type == Float.class) {
				ArrayList<Float> copy = (ArrayList<Float>) possibleValues
						.clone();
				Collections.sort(copy);
				min = (T) copy.get(0);
				max = (T) copy.get(copy.size() - 1);
			} else if (type == Double.class) {
				ArrayList<Double> copy = (ArrayList<Double>) possibleValues
						.clone();
				Collections.sort(copy);
				min = (T) copy.get(0);
				max = (T) copy.get(copy.size() - 1);
			} else if (type == Long.class) {
				ArrayList<Long> copy = (ArrayList<Long>) possibleValues.clone();
				Collections.sort(copy);
				min = (T) copy.get(0);
				max = (T) copy.get(copy.size() - 1);
			} else {
				return getValue();
			}
			if (TOPorBOTTOM == SwingConstants.TOP) {
				return max;
			} else {
				return min;
			}
		}
	}

	/**
	 * @return the minimum possible value of this command. This operation is
	 *         only possible if the list is holding one of the following types:<br>
	 *         <ul>
	 *         <li>String</li>
	 *         <li>Integer</li>
	 *         <li>Float</li>
	 *         <li>Double</li>
	 *         <li>Long</li>
	 *         </ul>
	 *         Otherwise the function will return the actual value of this
	 *         command.
	 */
	public T getMinimumPossibleValue() {
		return getValue(SwingConstants.BOTTOM);
	}

	/**
	 * @return the maximum possible value of this command. This operation is
	 *         only possible if the list is holding one of the following types:<br>
	 *         <ul>
	 *         <li>String</li>
	 *         <li>Integer</li>
	 *         <li>Float</li>
	 *         <li>Double</li>
	 *         <li>Long</li>
	 *         </ul>
	 *         Otherwise the function will return the actual value of this
	 *         command.
	 */
	public T getMaximumPossibleValue() {
		return getValue(SwingConstants.TOP);
	}

	public ArrayList<ItemListener> getItemListeners() {
		return itemlisteners;
	}

	public void addItemListener(ItemListener itemListener) {
		this.itemlisteners.add(itemListener);
	}

	/**
	 * returns the actual value of this command
	 */
	@Override
	public Object[] getSelectedObjects() {
		return new Object[] { getValue() };
	}

	@Override
	public void removeItemListener(ItemListener itemListener) {
		this.itemlisteners.remove(itemListener);
	}
	
	/**
	 * tries to parse a list of string arguments into {@link ConsoleCommand}s
	 * and sets their values. <br>
	 * The string[] should look somethng like this:<br>
	 * <i>["-cmd", "value", "-cmd", "value",...]</i>
	 * 
	 * @param args
	 *            a String[] which holds command names followed by their value
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void parseArguments(String[] args) {
		String errorMessage = "";
		for (int i = 0; i < args.length; i++) {
			String arg = args[i].trim();
			boolean cmdFound = false;

			for (ConsoleCommand cmd : getCVARS()) {
				cmdFound = false;
//				if (cmd.getName().equalsIgnoreCase(STATUS.getName())
//						&& arg.equalsIgnoreCase(STATUS.getName())) {
//					System.out.println(getCVARS_String());
//					Main.getMainFrame().getConsole().log(getCVARS_String());
//					cmdFound = true;
//				} else 
				if ((CMDKEY+cmd.getName()).equalsIgnoreCase(arg)) {
					cmdFound = true;
					try {
						cmd.setValue(cmd.interpretValue(args[i + 1]));
						i += 1;
						System.out.println(cmd);
						Main.getMainFrame().getConsole().log(cmd.toString());
					} catch (IndexOutOfBoundsException indexException) {
						errorMessage = "Missing value for command " + cmd;
						System.err.println(errorMessage);
						Main.getMainFrame().getConsole().log(errorMessage);
					} catch (IllegalArgumentException argumentException) {
						errorMessage = argumentException.getMessage();
						System.err.println(errorMessage);
						Main.getMainFrame().getConsole().log(errorMessage);
					}
				}
				if (cmdFound) {
					break;
				}
			}

			if (!cmdFound) {
				errorMessage = "Unknown command: " + arg;
				System.err.println(errorMessage);
				Main.getMainFrame().getConsole().log(errorMessage);
			}
		}
	}
}
