package tankz.controller.actions;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import mapeditor.editor.Editor;
import tankz.main.Main;
import tankz.model.game.Game;
import tankz.model.map.MapIO;
import tankz.util.Utils;
import tankz.view.gamemenu.GameMenu;
import tankz.view.gamemenu.menus.PauseMenu;

/**
 * Contains all default actions, that can be independantly executed from
 * anywhere. These Actions are mostly combined with visual elements.
 * 
 * @author Eric W�nsche
 * 
 */
public class Actions {

	/**
	 * Brings up a confirm dialog to close the application. if answered with
	 * YES, the application will shut down with the System.exit(0) command
	 */
	public static final Action EXIT = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		{
			setEnabled(true);
			putValue(NAME, "Exit");
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int result = JOptionPane.showConfirmDialog(Main.getMainFrame(),
					"Do you want to exit the game?", "Exit",
					JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.YES_OPTION) {
				System.exit(0);
			}
		}
	};

	/**
	 * Brings up a FileChooser for a xml map file, loads the given map and
	 * starts a new game on it
	 */
	public static Action LOAD_MAP = new AbstractAction() {

		private static final long serialVersionUID = 1L;

		{
			setEnabled(true);
			putValue(NAME, "Load Map");
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(Utils.getFolderOfJAR()
					.getParent() + File.separator + "maps/"));
			int result = fileChooser.showOpenDialog(Main.getMainFrame());
			if (result == JFileChooser.APPROVE_OPTION) {
				File mapFile = fileChooser.getSelectedFile();
				try {
					Game singleplayer = new Game("Singleplayer");
					singleplayer.setMap(MapIO.getInstance().load(mapFile, false));
					Main.getMainFrame().getGameField().setGame(singleplayer);
					singleplayer.start();
//					DATA.setActualMap(MapIO.getInstance().load(mapFile, false));
//					GameControl.startGame(false, -1);
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(Main.getMainFrame(),
							"An error ocurred!\n\n" + e.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
					Main.getMainFrame().setMenuVisible(true);
				} finally {
					Main.getMainFrame().requestFocus();
				}
			}
		}
	};

	/**
	 * This action will dispose the MainFrame of the game and execute the
	 * Editor.main() function
	 */
	public static Action LAUNCH_EDITOR = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		{
			setEnabled(true);
			putValue(NAME, "Map Editor");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Main.getMainFrame().dispose();
			} catch (Exception exc) {
				exc.printStackTrace();
			} finally {
				Editor.main(null);
			}
		}
	};

	
	/**
	 * Calls GameControl.pause() and opens the PauseMenu
	 */
	public static Action PAUSE_GAME = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		{
			setEnabled(true);
			putValue(NAME, "Pause");
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("PAUSE");
			Main.getMainFrame().getGameField().getGame().pauseGame();
			GameMenu.getInstance().load(new PauseMenu());
			Main.getMainFrame().setMenuVisible(true);
			Main.getMainFrame().requestFocus();
		}
	};

	
	/**
	 * Calls GameControl.unpause() and hides the menu
	 */
	public static Action UNPAUSE_GAME = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		{
			setEnabled(true);
			putValue(NAME, "Continue");
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("UNPAUSE");
			Main.getMainFrame().getGameField().getGame().unpauseGame();
			Main.getMainFrame().setMenuVisible(false);
			Main.getMainFrame().requestFocus();
		}
	};

	/**
	 * Calls GameControl.cancelGame() and opens the last menu
	 */
	public static Action END_GAME = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		{
			setEnabled(true);
			putValue(NAME, "End Game");
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("END GAME");
			Main.getMainFrame().getGameField().getGame().stopGame();
			GameMenu.getInstance().loadPrevious();
			Main.getMainFrame().setMenuVisible(true);
			Main.getMainFrame().requestFocus();
		}
	};
}
