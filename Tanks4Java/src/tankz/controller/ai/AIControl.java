package tankz.controller.ai;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import tankz.controller.console.ConsoleCommand;
import tankz.model.Direction;
import tankz.model.Tank;
import tankz.model.game.Game;
import tankz.model.graph.Graph;
import tankz.model.map.MapElement;

/**
 * Holds methods that control CPU controlled tanks
 * 
 * @author Eric W�nsche
 *
 */
public class AIControl {
	// check if pseudo aiming line crosses any map element
	// if not, then shoot with prediction
	// walk random
	// try to avoid collision with bullets

	// Walking prediction when shooting
	// Walking direction of tank is necessary
	
	private Game game = null;
	
	public AIControl() {}
	
	public AIControl(Game game) {
		setGame(game);
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * Tries to obtain the shortest path between two given points from the maps
	 * {@link Graph} object.
	 * 
	 * @param start
	 *            {@link MapElement} at which the AI object is starting
	 * @param end
	 *            {@link MapElement} to which the AI object should walk
	 * @return List of {@link MapElement} which represents the path towards the
	 *         end element
	 * @throws Exception
	 *             if start or end element are null or not walkable
	 */
	public ArrayList<MapElement> getShortestPath(MapElement start,
			MapElement end) throws Exception {
		if (start == null || end == null) {
			throw new Exception("start or end point == NULL");
		} else if (!start.isWalkable() || !end.isWalkable()) {
			throw new Exception("start or end point aren't walkable!\nStart: "
					+ start + "\nEnd: " + end);
		} else {
			return getGame().getActualMap().getGraph().getShortestPath(start, end);
		}
	}

	/**
	 * Iterates through the Games Tank-List and returns a human tank, that is
	 * next to the given CPU controlled tank
	 * 
	 * @param CPUTank
	 *            - a CPU controlled tank, from which the next human tank should
	 *            be calculated
	 * @return a human controlled tank that is next to the given CPU controlled
	 *         tank
	 */
	private Tank getHumanTarget(Tank CPUTank) {
		// get nearest target
		Tank target = null;
		Double targetDistance = Double.POSITIVE_INFINITY;
		for (Tank possibleTarget : getGame().getTanks()) {
			if (!possibleTarget.equals(CPUTank) && possibleTarget.isHuman()
					&& !possibleTarget.isDead()) {
				double distance = CPUTank.getCenter().distance(
						possibleTarget.getCenter());
				if (distance < targetDistance) {
					target = possibleTarget;
				}
			}
		}
		return target;
	}

	/**
	 * If AI is enabled, the nearest enemy {@link Tank} to the given
	 * {@link Tank} is calculated as the possible target. if the tank is able to
	 * shoot directly at the possible target, he shoots at three prediction
	 * points. Otherwise the tank is told to walk towards this enemy.
	 * 
	 * @param t
	 *            CPU {@link Tank} that should execute his next action
	 */
	public void manoeuvre(Tank t) {
		if (ConsoleCommand.AI_ENABLED.getValue().equals((Boolean)true)) {

			// get nearest target
			Tank target = getHumanTarget(t);

			if (target != null) {
				t.setAimingPoint(target.getCenter());
				if (canShoot(t, target)) {
					ArrayList<Point> targetPoints = getPredictionPoints(t,
							target);
					for (Point targetPoint : targetPoints) {
						if (ConsoleCommand.DEBUG.getValue().toString()
								.equalsIgnoreCase("ai_console")) {
							System.out.println("Tank '" + t.getName()
									+ "' shoots at Tank '" + target.getName());
						}
						t.shoot(targetPoint);
					}
				} else {
					move(t, false);
				}
			}
		}
	}

	/**
	 * Calculates the prediction points that the CPU tank should shoot at, to
	 * hit his target. At the moment there is no prediction. The method only
	 * returns the center point of the target.
	 * 
	 * @param cpu
	 *            CPU {@link Tank} that should shoot at a target
	 * @param target
	 *            {@link Tank} that should be shot by the CPU
	 * @return List of {@link Point}s
	 */
	private static ArrayList<Point> getPredictionPoints(Tank cpu, Tank target) {
		ArrayList<Point> points = new ArrayList<Point>();
		points.add(target.getCenter());

		// TODO GET WALKING DIRECTION AND PREDICT SHOT

		return points;
	}

	/**
	 * Calculates the nearest possible human target of the given CPU controlled
	 * tank and moves the CPU controlled tank towards the target. <br/>
	 * <br/>
	 * The parameter ceilTankLocation - determines how the location of the CPU
	 * controlled tank is calculated. This parameter is controlled by the method
	 * itself. When calling the method directly, use false as value.
	 * 
	 * @param t
	 *            - CPU controlled tank
	 * @param ceilTankLocation
	 *            (see method description)
	 */
	private void move(Tank t, boolean ceilTankLocation) {
		Tank target = getHumanTarget(t);
		if (target == null) {
			return;
		}

		MapElement start = getGame().getActualMap().getMapElementByLocation(
				t.getArrayLocation(ceilTankLocation));
		MapElement end = getGame().getActualMap().getMapElementByLocation(
				target.getArrayLocation(false));

		try {
			if (isDebugConsole())
				System.out.println("Get shortest path (ceiling = "
						+ ceilTankLocation + ") from\n" + start + "\nTo\n"
						+ end);
			ArrayList<MapElement> path = getShortestPath(start, end);

			Point m_location;
			if (path.get(0).equals(start)) {
				m_location = path.get(1).getLocationOnScreen();
			} else {
				m_location = path.get(0).getLocationOnScreen();
			}
			Point t_location = t.getPosition();
			if (m_location.x == t_location.x && m_location.y < t_location.y) {
				t.move(Direction.NORTH, true);
			} else if (m_location.x < t_location.x
					&& m_location.y < t_location.y) {
				t.move(Direction.NORTH_WEST, true);
			} else if (m_location.x > t_location.x
					&& m_location.y < t_location.y) {
				t.move(Direction.NORTH_EAST, true);
			} else if (m_location.x == t_location.x
					&& m_location.y > t_location.y) {
				t.move(Direction.SOUTH, true);
			} else if (m_location.x > t_location.x
					&& m_location.y > t_location.y) {
				t.move(Direction.SOUTH_EAST, true);
			} else if (m_location.x < t_location.x
					&& m_location.y > t_location.y) {
				t.move(Direction.SOUTH_WEST, true);
			} else if (m_location.x < t_location.x
					&& m_location.y == t_location.y) {
				t.move(Direction.WEST, true);
			} else if (m_location.x > t_location.x
					&& m_location.y == t_location.y) {
				t.move(Direction.EAST, true);
			}
		} catch (Exception e) {
			if (!ceilTankLocation) {
				move(t, true);
			} else {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Checks whether the CPU controlled tank can shoot at the other given tank.
	 * This is done by creating a rectangle that has the width of a projectile
	 * and is as long as the distance between the two tanks. If this rectangle
	 * intersects with map elements that have projectile collision, than the
	 * method returns false, otherwise if no map elements with projectile
	 * collision are between the two tanks, it returns true.
	 * 
	 * @param cpuTank
	 *            - CPU controlled tank that should shoot at the target tank
	 * @param target
	 *            - target tank, at which the CPU controlled tank should shoot
	 * @return true, if no map elements with projectile collision are between
	 *         the two tanks; false otherwise
	 */
	public boolean canShoot(Tank cpuTank, Tank target) {
		Rectangle lane = new Rectangle();
		lane.width = cpuTank.getProjectileType().getSize().width + 1; // +1
																		// because
																		// of
																		// not
																		// precise
																		// firing
																		// of
																		// bullets
		lane.height = (int) cpuTank.getCenter().distance(
				(Point2D) target.getCenter());
		lane.x = cpuTank.getCenter().x - (lane.width / 2);
		lane.y = cpuTank.getCenter().y - lane.height;

		Point center = new Point(cpuTank.getCenter().x, cpuTank.getCenter().y);
		Point end = target.getCenter();
		double angle = (Math.atan2(end.x - center.x, -(end.y - center.y)) - (Math.PI / 2));
		angle = Math.toRadians(Math.toDegrees(angle) + 90);

		Area a = new Area(lane);
		AffineTransform af = new AffineTransform();
		af.rotate(angle, lane.getCenterX(), lane.getMaxY());
		a = a.createTransformedArea(af);

		for (MapElement me : getGame().getActualMap().getMapElements()) {
			if (me.hasProjectileCollision()
					&& a.intersects(me.getRect().getBounds2D())) {
				return false;
			}
		}

		for (Tank t : getGame().getTanks()) {
			if (!t.equals(target) && !t.equals(cpuTank) && !t.isDead()
					&& a.intersects(t.getRect())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks whether the value of the convar "DEBUG_AI" is set to "console" or "all"
	 *  
	 * @return true if the value DEBUG_AI is set to "console" or "all", false otherwise
	 */
	public static boolean isDebugConsole() {
		if (ConsoleCommand.DEBUG_AI.getValue().equalsIgnoreCase("console")
				|| ConsoleCommand.DEBUG_AI.getValue().equalsIgnoreCase(
						"all")) {
			return true;
		}
		return false;
	}

	/**
	 * Checks whether the value of the convar "DEBUG_AI" is set to "visual" or "all"
	 *  
	 * @return true if the value DEBUG_AI is set to "visual" or "all", false otherwise
	 */
	public static boolean isDebugVisual() {
		if (ConsoleCommand.DEBUG_AI.getValue().equalsIgnoreCase("visual")
				|| ConsoleCommand.DEBUG_AI.getValue().equalsIgnoreCase(
						"all")) {
			return true;
		}
		return false;
	}

}
