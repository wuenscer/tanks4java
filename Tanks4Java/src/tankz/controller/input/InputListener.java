package tankz.controller.input;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

import tankz.main.Main;
import tankz.model.Direction;
import tankz.model.Tank;

public class InputListener implements KeyListener, MouseListener, MouseMotionListener {

	/**
	 * if the game is running, the mouse will be restricted to the area of the component
	 */
	private boolean restrictMousePostion = false;
	private Point mousePosition = new Point(0, 0);
	private boolean[] buttonState = new boolean[] { false, false, false };
	private boolean[] buttonClickState = new boolean[] { false, false, false };
	private int[] buttonClickCount = new int[] { 0, 0, 0 };
	private Map<Integer, Boolean> fKeyStore = new HashMap<Integer, Boolean>();

	/**
	 * The component to which inputs are relatively calculated
	 */ 
	private Component component;
	
	private Tank tankToControl = null;

	private Robot mouseRobot = null;

	public InputListener() {
		try {
			this.mouseRobot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public InputListener(Component component) {
		this();
		registerTo(component);
	}
	
	public InputListener(Component component, boolean restrictMousePosition) {
		this();
		registerTo(component);
		setRestrictMousePostion(restrictMousePosition);
	}

	public InputListener(Tank tank, Component component) {
		this();
		this.setTankToControl(tank);
		registerTo(component);
	}
	
	public InputListener(Tank tank, Component component, boolean restrictMousePosition) {
		this();
		this.setTankToControl(tank);
		setRestrictMousePostion(restrictMousePosition);
		registerTo(component);
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	public boolean isRestrictMousePostion() {
		return restrictMousePostion;
	}

	public void setRestrictMousePostion(boolean restrictMousePostion) {
		this.restrictMousePostion = restrictMousePostion;
	}

	public void resetMouseAndKeyboardStates() {
		buttonState = new boolean[] { false, false, false };
		buttonClickState = new boolean[] { false, false, false };
		buttonClickCount = new int[] { 0, 0, 0 };
		fKeyStore = new HashMap<Integer, Boolean>();
	}

	public void registerTo(Component component) {
		component.setFocusable(true);
		component.requestFocus();
		component.addMouseListener(this);
		component.addMouseMotionListener(this);
		component.addKeyListener(this);
		if(getComponent() == null){
			setComponent(component);
		}
	}

	// ******************************************//
	// MOUSE LISTENER //
	// ******************************************//

	@Override
	public void mousePressed(MouseEvent e) {
		int button = e.getButton();
		if (button > 0 && button < 4) {
			buttonState[button - 1] = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		int button = e.getButton();
		if (button > 0 && button < 4) {
			buttonState[button - 1] = false;
			buttonClickState[button - 1] = true;
			buttonClickCount[button - 1] += 1;
		}
	}

	@Override
	public void mouseClicked(MouseEvent mouseEvent) {
		/*
		 * int button = mouseEvent.getButton(); if (button > 0 && button < 4) {
		 * buttonClickState[button-1] = true; buttonClickCount[button-1] =
		 * mouseEvent.getClickCount(); }
		 */
	}

	@Override
	public void mouseEntered(MouseEvent mouseEvent) {
	}

	@Override
	public void mouseExited(MouseEvent mouseEvent) {

	}

	@Override
	public void mouseDragged(MouseEvent mouseEvent) {
	}

	@Override
	public void mouseMoved(MouseEvent mouseEvent) {
		String debug = "";
		if (isRestrictMousePostion() && getComponent() != null
				&& getComponent().isShowing() && mouseRobot != null) {
			PointerInfo pointerInfo = MouseInfo.getPointerInfo();
			Point mousePoint = new Point(pointerInfo.getLocation());
			debug += "MousePoint: " + mousePoint + "\n";
			int mouseXnew = mousePoint.x, mouseYnew = mousePoint.y;

			int compMinX = 0, compMaxX = 0, compMinY = 0, compMaxY = 0;
			Point compLocation = getComponent().getLocationOnScreen();
			compMinX = compLocation.x;
			compMinY = compLocation.y;
			compMaxX = compMinX + getComponent().getWidth();
			compMaxY = compMinY + getComponent().getHeight();
			debug += "Comp Coords: " + compMinX + ", " + compMinY + ", " + compMaxX + ", " + compMaxY + "\n";

			if (mousePoint.x <= compMinX) {
				mouseXnew = compMinX + 1;
			}
			if (mousePoint.x >= compMaxX) {
				mouseXnew = compMaxX - 1;
			}
			if (mousePoint.y <= compMinY) {
				mouseYnew = compMinY + 1;
			}
			if (mousePoint.y >= compMaxY) {
				mouseYnew = compMaxY - 1;
			}

			Point mouseNew = new Point(mouseXnew, mouseYnew);
			debug += "New Mouse: " + mouseNew + "\n";
			mouseRobot.mouseMove(mouseNew.x, mouseNew.y);

			Main.getMainFrame().setDebugValue(debug);
		}
	}

	public boolean isMouseButtonPressed(int mouseButton) {
		return (mouseButton < buttonClickState.length && mouseButton > 0 && buttonState[mouseButton - 1]);
	}

	public boolean wasMouseButtonClicked(int mouseButton) {
		if (mouseButton < buttonClickState.length && mouseButton > 0 && buttonClickState[mouseButton - 1]) {
			buttonClickState[mouseButton - 1] = false;
			buttonClickCount[mouseButton - 1] = 0;
			return true;
		}
		return false;
	}

	/**
	 * Method to check for double click etc. <br>
	 * <br>
	 * Example - Check for double click: <br>
	 * wasMouseButtonClicked(MouseEvent.BUTTON1, 2)
	 * 
	 * @param mouseButton
	 * @param clickCount
	 * @return
	 */
	public boolean wasMouseButtonClicked(int mouseButton, int clickCount) {
		if (mouseButton < buttonClickState.length && mouseButton > 0 && buttonClickState[mouseButton - 1]
				&& buttonClickCount[mouseButton - 1] == clickCount) {
			buttonClickState[mouseButton - 1] = false;
			buttonClickCount[mouseButton - 1] = 0;
			return true;
		}
		return false;
	}

	public Point getMousePosition() {
		PointerInfo a = MouseInfo.getPointerInfo();
		Point point = new Point(a.getLocation());
		try {
			SwingUtilities.convertPointFromScreen(point, getComponent());
			correctMouseLocation(point);
		} catch (Exception e) {
			e.printStackTrace();
		}
		setMousePosition(point);
		return mousePosition;
	}

	/**
	 * This function is used to extend the aiming line out of the rectangle of
	 * the tank, so an intersection is assured.
	 * 
	 * @param mousePoint
	 */
	public void correctMouseLocation(Point mousePoint) {
		Rectangle tankRect = tankToControl.getRect();
		if (tankRect.contains(mousePoint)) {
			if (tankRect.getMinX() <= mousePoint.getX()
					&& tankRect.width / 2 + tankRect.getMinX() >= mousePoint.getX()) {
				mousePoint.x = (int) tankRect.getMinX() - 1;
			} else {
				mousePoint.x = (int) tankRect.getMaxX() + 1;
			}
		}
	}

	public void setMousePosition(Point mousePosition) {
		this.mousePosition = mousePosition;
	}

	// ******************************************//
	// KEYBOARD LISTENER //
	// ******************************************//

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		fKeyStore.put(e.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		fKeyStore.put(e.getKeyCode(), false);
	}

	public Map<Integer, Boolean> getKeyStore() {
		return fKeyStore;
	}

	public boolean isKeyPressed(int keyCode) {
		if (fKeyStore.containsKey(keyCode)) {
			return fKeyStore.get(keyCode).booleanValue();
		}
		return false;
	}

	public boolean isKeyReleased(int keyCode) {
		return (fKeyStore.containsKey(keyCode) && !fKeyStore.get(keyCode).booleanValue());
	}

	public Tank getTankToControl() {
		return tankToControl;
	}

	public void setTankToControl(Tank tankToControl) {
		this.tankToControl = tankToControl;
	}

	public Direction getMoveDirection(){
		if ((isKeyPressed(KeyEvent.VK_LEFT) || isKeyPressed(KeyEvent.VK_A))
				&& (isKeyPressed(KeyEvent.VK_UP) || isKeyPressed(KeyEvent.VK_W))) {
			return Direction.NORTH_WEST;
		} else if ((isKeyPressed(KeyEvent.VK_LEFT) || isKeyPressed(KeyEvent.VK_A))
				&& (isKeyPressed(KeyEvent.VK_DOWN) || isKeyPressed(KeyEvent.VK_S))) {
			return Direction.SOUTH_WEST;
			
		} else if ((isKeyPressed(KeyEvent.VK_RIGHT) || isKeyPressed(KeyEvent.VK_D))
				&& (isKeyPressed(KeyEvent.VK_UP) || isKeyPressed(KeyEvent.VK_W))) {
			return Direction.NORTH_EAST;
		} else if ((isKeyPressed(KeyEvent.VK_RIGHT) || isKeyPressed(KeyEvent.VK_D))
				&& (isKeyPressed(KeyEvent.VK_DOWN) || isKeyPressed(KeyEvent.VK_S))) {
			return Direction.SOUTH_EAST;
		} else if (isKeyPressed(KeyEvent.VK_LEFT) || isKeyPressed(KeyEvent.VK_A)) {
			return Direction.WEST;
		} else if (isKeyPressed(KeyEvent.VK_RIGHT) || isKeyPressed(KeyEvent.VK_D)) {
			return Direction.EAST;
		} else if (isKeyPressed(KeyEvent.VK_UP) || isKeyPressed(KeyEvent.VK_W)) {
			return Direction.NORTH;
		} else if (isKeyPressed(KeyEvent.VK_DOWN) || isKeyPressed(KeyEvent.VK_S)) {
			return Direction.SOUTH;
		}else{
			return null;
		}
	}
}
