package tankz.controller.input;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import tankz.main.Main;
import tankz.view.MainFrame;
import tankz.view.gamemenu.GameMenu;
import tankz.view.gamemenu.GameMenuButton;
import tankz.view.gamemenu.menus.PauseMenu;

public class MenuKeyBindings {
	public static final String ESCAPE_ACTION_KEY = "ESCAPE_ACTION";
	public static final String CONSOLE_ACTION_KEY = "CONSOLE_ACTION";
	
	private final static AbstractAction ESCAPE_ACTION = new AbstractAction(ESCAPE_ACTION_KEY) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			if(Main.getMainFrame().getCurrentComponentID().equalsIgnoreCase(MainFrame.CONSOLE_CARD_ID)){
				Main.getMainFrame().setConsoleVisible(false);
			}else if (Main.getMainFrame().getCurrentComponentID().equalsIgnoreCase(MainFrame.GAMEFIELD_CARD_ID)) {
				if(Main.getMainFrame().getGameField().getGame().isGameRunning()){
					Main.getMainFrame().getGameField().getGame().pauseGame();
					GameMenu.getInstance().load(new PauseMenu());
					Main.getMainFrame().setMenuVisible(true);
				}else{
					GameMenu.getInstance().loadPrevious();
					Main.getMainFrame().setMenuVisible(true);
				}
			} else if (Main.getMainFrame().getCurrentComponentID().equalsIgnoreCase(MainFrame.GAMEMENU_CARD_ID)){
				GameMenuButton button = GameMenu.getInstance().getGameMenuPanel().getDefaultButton();
				ActionEvent ae = new ActionEvent(button, 0, button.getActionCommand());
				boolean previousLoaded = GameMenu.getInstance().loadPrevious();
				if(!previousLoaded){
					GameMenu.getInstance().getGameMenuPanel().getDefaultButtonAction().actionPerformed(ae);
				}
			}
		}
	};
	
	private final static AbstractAction CONSOLE_ACTION = new AbstractAction(CONSOLE_ACTION_KEY) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			Main.getMainFrame().setConsoleVisible(true);
		}
	};
	
	public static void applyBindingsTo(JComponent component){
		InputMap inputMap = component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap actionMap = component.getActionMap();
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), ESCAPE_ACTION_KEY);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DEAD_CIRCUMFLEX, 0), CONSOLE_ACTION_KEY);
		actionMap.put(ESCAPE_ACTION_KEY, ESCAPE_ACTION);
		actionMap.put(CONSOLE_ACTION_KEY, CONSOLE_ACTION);
	}
}
