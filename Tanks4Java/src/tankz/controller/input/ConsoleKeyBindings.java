package tankz.controller.input;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import tankz.main.Main;

public class ConsoleKeyBindings {
	
	public static final String ENTER_ACTION_KEY = "ENTER_ACTION";

	public static void applyBindingsTo(JComponent component){
		InputMap inputMap = component.getInputMap(JComponent.WHEN_FOCUSED);
		ActionMap actionMap = component.getActionMap();
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), ENTER_ACTION_KEY);
		actionMap.put(ENTER_ACTION_KEY, new AbstractAction(ENTER_ACTION_KEY) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.getMainFrame().getConsole().logInput();
			}
		});
	}
	
}
