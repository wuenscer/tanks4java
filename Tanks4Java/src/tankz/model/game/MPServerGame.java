package tankz.model.game;

import java.awt.Point;
import java.io.IOException;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;

import tankz.controller.input.InputListener;
import tankz.model.Direction;
import tankz.model.Projectile;
import tankz.model.Tank;
import tankz.model.map.MapElement;
import tankz.model.map.NoMapException;
import tankz.net.client.Client;
import tankz.net.protocol.Protocol;
import tankz.net.protocol.Protocol.ACTION;
import tankz.net.server.Server;

public class MPServerGame extends Game {
	private Server server = null;
	
	public MPServerGame(Server server) {
		setName("MPServerGame");
		setServer(server);
	}
	
	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}
	
	@Override
	public void init() throws NoMapException {
		if(getActualMap() != null){
			for(MapElement start : getActualMap().getSpawnPoints()){
				if(!start.isHumanSpawnpoint()){
					Tank t = new Tank(getNextEntityID());
					t.setPosition(start.getLocationOnScreen());
					t.setName("Tank "+t.getID());
					getTanks().add(t);
					try {
						server.sendPacket(Protocol.buildPacket(ACTION.SPAWN_TANK,
								new int[] { t.getID(), t.getPosition().x, t.getPosition().y }));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			try {
				server.sendPacket(Protocol.buildPacket(ACTION.START_GAME, null));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			throw new NoMapException("There was no map loaded!");
		}
	}
	
	@Override
	public void executeInputActions(InputListener inputListener) {
		for(DatagramPacket packet : server.getPacketBuffer()) {
			try {
				receive(packet);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void receive(DatagramPacket packet) throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(Protocol.BUFFER_LENGTH);
		buffer.put(packet.getData());
		buffer.position(0);
		ACTION action = ACTION.values()[buffer.getInt()];
		Client client = server.getClient(packet);
		if (client != null || action.compareTo(ACTION.CONNECT) == 0) {
			switch (action) {
			case MOVE:
				Direction direction = Direction.values()[buffer.getInt()];
				client.getControlledTank().move(direction, true);
				System.out.println(client.getName() + " moved " + direction);
				break;
			case AIM:
				Point aimPoint = new Point();
				aimPoint.setLocation(buffer.getInt(), buffer.getInt());
				client.getControlledTank().setAimingPoint(aimPoint);
				System.out.println(client.getName() + " aimed at [" + aimPoint.x + "|" + aimPoint.y + "]");
				break;
			case SHOOT:
				Point shootPoint = new Point();
				shootPoint.setLocation(buffer.getInt(), buffer.getInt());
				client.getControlledTank().shoot(shootPoint);
				System.out.println(client.getName() + " shot at [" + shootPoint.x + "|" + shootPoint.y + "]");
				break;
			default:
				break;
			}
		}
	}
	
	@Override
	public void update() {
		executeInputActions(null);
		
		if (projectilesToFire.size() > 0) {
			PROJECTILES.addAll(projectilesToFire);
			projectilesToFire.clear();
		}

		if (projectilesToRemove.size() > 0) {
			PROJECTILES.removeAll(projectilesToRemove);
			projectilesToRemove.clear();
		}

		for (Projectile p : PROJECTILES) {
			p.increasePosition(true);
			try {
				server.sendPacket(Protocol.buildPositionPacket(p.getId(), p.getPosition()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		for (Tank t : getTanks()) {
			if (!t.isHuman() && !t.isDead()) {
				AIControl.manoeuvre(t);
				try {
					server.sendPacket(Protocol.buildPositionPacket(t.getID(), t.getPosition()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		checkGameState();
	}
	
	@Override
	public void checkGameState() {
		int actualTanksAlive = 0;
		Tank lastOne = null;

		for (Tank t : getTanks()) {
			if (!t.isDead()) {
				actualTanksAlive += 1;
				lastOne = t;
			}
		}
		if(actualTanksAlive == 1){
			setGameOverMessage("'" + lastOne.getName() + "' has won!");
			setGameWon(true);
			stopGame();
		}
	}
	
	@Override
	public void render() {
		//NOTHING TO RENDER AT SERVER
	}
}
