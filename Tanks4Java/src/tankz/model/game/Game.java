package tankz.model.game;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import tankz.controller.ai.AIControl;
import tankz.controller.configuration.Config;
import tankz.controller.console.ConsoleCommand;
import tankz.controller.input.InputListener;
import tankz.main.Main;
import tankz.model.Direction;
import tankz.model.Projectile;
import tankz.model.Tank;
import tankz.model.map.Map;
import tankz.model.map.MapElement;
import tankz.model.map.NoMapException;

public class Game extends Thread {
	
	private int tickrate = 64;
	
	private int entityID = 0;

	public int getNextEntityID() {
		return entityID+=1;
	}

	private boolean FPSlimited = true;
	private int FPSlimit = 120;
	private long lastFPSTime = System.nanoTime();
	private long currentFPSTime = System.nanoTime();
	private long fpsDelta = 0;
	private boolean doRender = true;

	private int currentTick = 0;
	private long lastTickTime = System.nanoTime();
	private long currentTickTime = System.nanoTime();
	private long tickDelta = 0;

	private long lastTime = System.currentTimeMillis();
	private long currentTime = System.currentTimeMillis();

	private int currentFPS = 0;

	// 1 nanosecond = 1000000 miliseconds - 1 second = 1000000*1000
	private long tickrateNS = ((1000000 * 1000) / tickrate);
	private long fpsNS = ((1000000 * 1000) / FPSlimit);

	protected ArrayList<Projectile> projectilesToFire = new ArrayList<Projectile>();
	protected ArrayList<Projectile> projectilesToRemove = new ArrayList<Projectile>();
	private boolean gameOver = false;
	private boolean gamePaused = false;
	private boolean gameWon = false;
	private boolean gameLost = false;
	
	private String gameOverMessage = "Game Over!";
	private String pauseMessage = "Player X paused the game!";
	private String loseMessage = "You have lost!";
	
	public static int fieldSize = 50;
	
	private ArrayList<Tank> TANKS = new ArrayList<Tank>();
	public ArrayList<Projectile> PROJECTILES = new ArrayList<Projectile>();
	
	public ArrayList<GameEntity> entities = new ArrayList<GameEntity>();
	
	private Map actualMap = null;
	
	protected AIControl AIControl = new AIControl(this);
	
	public boolean isFPSlimited() {
		return FPSlimited;
	}

	public void setFPSlimited(boolean fPSlimited) {
		FPSlimited = fPSlimited;
	}

	public int getFPSlimit() {
		return FPSlimit;
	}

	public void setFPSlimit(int fPSlimit) {
		FPSlimit = fPSlimit;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	public boolean isGamePaused() {
		return gamePaused;
	}

	public void setGamePaused(boolean gamePaused) {
		this.gamePaused = gamePaused;
	}

	public String getLoseMessage() {
		return loseMessage;
	}

	public void setLoseMessage(String loseMessage) {
		this.loseMessage = loseMessage;
	}
	
	public int getTickrate() {
		return this.tickrate;
	}

	public void setTickrate(int tick) {
		if (tick > 0) {
			this.tickrate = tick;
		} else {
			System.err.println("INVALID TICK AMOUNT (" + tick + " <= 0)");
		}
	}
	
	public static int getFieldSize() {
		return fieldSize;
	}

	public void setFieldSize(int fieldSize) {
		Game.fieldSize = fieldSize;
	}

	public Map getActualMap() {
		return actualMap;
	}

	public void setMap(Map actualMap) {
		this.actualMap = actualMap;
	}

	public String getPauseMessage() {
		return pauseMessage;
	}

	public void setPauseMessage(String pauseMessage) {
		this.pauseMessage = pauseMessage;
	}

	public String getGameOverMessage() {
		return gameOverMessage;
	}

	public void setGameOverMessage(String gameOverMessage) {
		this.gameOverMessage = gameOverMessage;
	}

	public boolean isGameWon() {
		return gameWon;
	}

	public void setGameWon(boolean gameWon) {
		this.gameWon = gameWon;
	}

	public boolean isGameLost() {
		return gameLost;
	}

	public void setGameLost(boolean gameLost) {
		this.gameLost = gameLost;
	}
	
	public ArrayList<Tank> getTanks() {
		return TANKS;
	}
	
	/**
	 * 
	 * @param entityID
	 * @return the tank with a matching ID or null if no tank with the given ID exists
	 */
	public Tank getTankByID(int entityID) {
		for(Tank t : getTanks()) {
			if(t.getID()==entityID) {
				return t;
			}
		}
		return null;
	}
	
	public ArrayList<Projectile> getProjectiles() {
		return PROJECTILES;
	}
	
	/**
	 * 
	 * @param entityID
	 * @return the projectile with the matching ID or null if no projectile with the given ID exists
	 */
	public Projectile getProjectileByID(int entityID) {
		for(Projectile p : getProjectiles()) {
			if(p.getID()==entityID) {
				return p;
			}
		}
		return null;
	}

	public Game() {
		this("Game");
	}
	
	public Game(String name) {
		setName(name);
		tickrate = Integer.parseInt(Config.get(Config.TICKRATE));
		FPSlimited = Boolean.parseBoolean(Config.get(Config.FPS_LIMITED));
		FPSlimit = Integer.parseInt(Config.get(Config.FPS_LIMIT));
	}
	
	@Override
	public void run() {
		try {
			init();
			Main.getMainFrame().setMenuVisible(false);
			setGameOver(false);
			setGamePaused(false);
			lastTime = System.currentTimeMillis();
			while (!isGameOver()/* && GameControl.isGameRunning() */) {
				if (!isGamePaused()) {
					currentTickTime = System.nanoTime();
					tickDelta = (currentTickTime - lastTickTime);
					if (tickDelta >= tickrateNS) {
						update();
						currentTick++;
						lastTickTime = System.nanoTime();
					}
				}
				currentFPSTime = System.nanoTime();
				fpsDelta = (currentFPSTime - lastFPSTime);
				if (fpsDelta >= fpsNS || !FPSlimited) {
					doRender = true;
				} else {
					doRender = false;
				}
				if (doRender && !gamePaused) {
					render();
					currentFPS++;
					lastFPSTime = System.nanoTime();
				}
				currentTime = System.currentTimeMillis();
				if ((currentTime - lastTime) >= 1000) {
					System.out.println("Ticks: " + currentTick + "; FPS: " + currentFPS);
					currentTick = 0;
					currentFPS = 0;
					lastTime = System.currentTimeMillis();
				}
			}
			afterGameAction();
		} catch (NoMapException e) {
			e.printStackTrace();
		}
	}

	public void removeProjectile(Projectile projectile) {
		projectilesToRemove.add(projectile);
		entities.remove(projectile);
	}

	public void fireProjectile(Projectile projectile) {
		projectilesToFire.add(projectile);
		entities.add(projectile);
	}

	public void executeInputActions(InputListener inputListener) {
		if (inputListener.getTankToControl() != null) {
			// Set aiming point of controlled tank
			Point aimPoint = inputListener.getMousePosition();
			Tank tank = inputListener.getTankToControl();
			if (!tank.getAimingPoint().equals(aimPoint)) {
				tank.setAimingPoint(aimPoint);
			}
			// Move Tank
			Direction direction = inputListener.getMoveDirection();
			inputListener.getTankToControl().move(direction, true);

			// Mouse Actions
			if (inputListener.wasMouseButtonClicked(MouseEvent.BUTTON1)) {
				inputListener.getTankToControl().shoot(aimPoint);
			}
		} else {
			System.out.println("No tank defined to control.");
		}
	}

	public void update() {
		InputListener inputListener = Main.getMainFrame().getInputListener();
		executeInputActions(inputListener);

		if (projectilesToFire.size() > 0) {
			PROJECTILES.addAll(projectilesToFire);
			projectilesToFire.clear();
		}

		if (projectilesToRemove.size() > 0) {
			PROJECTILES.removeAll(projectilesToRemove);
			projectilesToRemove.clear();
		}

		for (Projectile p : PROJECTILES) {
			p.increasePosition(true);
		}

		for (Tank t : TANKS) {
			if (!t.equals(Main.getMainFrame().getInputListener().getTankToControl()) && !t.isHuman() && !t.isDead()) {
				AIControl.manoeuvre(t);
			}
		}
		checkGameState();
	}

	public void render() {
		Main.getMainFrame().getGameField().repaint();
		Main.getMainFrame().centerView();
	}
	
	public void init() throws NoMapException{
		if(getActualMap() != null){
			if(ConsoleCommand.GUI_ENABLED.getValue()){
				Main.getMainFrame().centerMap();
			}
			
			for(MapElement start : getActualMap().getSpawnPoints()){
				Tank t = new Tank(getNextEntityID());
				t.setPosition(start.getLocationOnScreen());
				t.setName("Tank "+t.getID());
				TANKS.add(t);
				entities.add(t);
				if(start.isHumanSpawnpoint()){
					t.setHuman(true);
					InputListener inputListener = Main.getMainFrame().getInputListener();
					inputListener.resetMouseAndKeyboardStates();
					inputListener.setTankToControl(t);
					inputListener.setRestrictMousePostion(true);
					t.setName(Config.get(Config.PLAYERNAME));
				}
			}
		}else{
			throw new NoMapException("There was no map loaded!");
		}
	}

	public void pauseGame() {
		setGamePaused(true);
	}

	public void unpauseGame() {
		setGamePaused(false);
	}

	public void stopGame() {
		setGamePaused(false);
		setGameOver(true);
	}
	
	public void checkGameState() {
		int actualTanksAlive = 0;
		Tank lastOne = null;

		for (Tank t : TANKS) {
			if (!t.isDead()) {
				actualTanksAlive += 1;
				lastOne = t;
			}
		}
		if (Main.getMainFrame().getInputListener().getTankToControl().isDead()) {
			setGameOverMessage("You have lost!");
			setGameLost(true);
			stopGame();
		}else if(actualTanksAlive == 1){
			setGameOverMessage("'" + lastOne.getName() + "' has won!");
			setGameWon(true);
			stopGame();
		}
	}
	
	public void afterGameAction(){
		if(isGameLost() || isGameWon()){
			int result = JOptionPane.showConfirmDialog(Main.getMainFrame(), 
					getGameOverMessage()+"\n\nWould you like to retry this level?",
					"Retry?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(result == JOptionPane.OK_OPTION){
				try {
					Game newGame = new Game(this.getName());
					newGame.setMap(getActualMap());
					Main.getMainFrame().getGameField().setGame(newGame);
					newGame.start();
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(Main.getMainFrame(), "An error occurred!\n\n"+e);
				}
			}else{
				Main.getMainFrame().setMenuVisible(true);
			}
		}else{
			JOptionPane.showMessageDialog(Main.getMainFrame(), getGameOverMessage());
			Main.getMainFrame().setMenuVisible(true);
		}
	}

	public boolean isGameRunning() {
		return (!isInterrupted() && !isGamePaused());
	}
	
	public GameEntity getEntityByID(int entityID) throws Exception {
		for(GameEntity entity : entities) {
			if(entity.getID()==entityID) {
				return entity;
			}
		}
		throw new Exception("Given entity ID ("+entityID+") not found in this game instance");
	}
}
