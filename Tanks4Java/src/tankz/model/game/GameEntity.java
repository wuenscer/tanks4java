package tankz.model.game;

import java.awt.Graphics2D;
import java.awt.Point;

public class GameEntity {
	private int ID = 0;
	private Point position = new Point(0,0);
	
	public GameEntity(int ID) {
		this.ID = ID;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}
	
	public void render(Graphics2D g) {
		
	}
	
	public void update() {
		
	}
}
