package tankz.model.game;

import java.io.InputStream;

import javax.swing.JOptionPane;

import tankz.main.Main;
import tankz.model.map.Map;
import tankz.model.map.MapIO;
import tankz.storage.Database;

public class CampaignGame extends Game {
	private int levelID = -1;
	private static final String path = "/resources/maps/campaign_%d.xml";
	
	public CampaignGame(int levelID) {
		super();
		setLevelID(levelID);
	}

	public int getLevelID() {
		return levelID;
	}

	public void setLevelID(int levelID) {
		this.levelID = levelID;
	}
	
	@Override
	public void afterGameAction() {
		if(isGameLost()){
			int result = JOptionPane.showConfirmDialog(Main.getMainFrame(), 
					getGameOverMessage()+"\n\nWould you like to retry this level?",
					"Retry?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(result == JOptionPane.OK_OPTION){
				try {
					CampaignGame newGame = new CampaignGame(levelID);
					newGame.setMap(getActualMap());
					Main.getMainFrame().getGameField().setGame(newGame);
					newGame.start();
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(Main.getMainFrame(), "An error occurred!\n\n"+e);
				}
			}else{
				Main.getMainFrame().setMenuVisible(true);
			}
		}else if (isGameWon()) {
			try {
				int nextLevelID = levelID+1;
				if(Database.getInstance().levelExists(nextLevelID)) {
					if(!Database.getInstance().isLevelUnlocked(nextLevelID)) {
						Database.getInstance().unlockLevel(nextLevelID);
					}
					int result = JOptionPane.showConfirmDialog(Main.getMainFrame(), 
							getGameOverMessage()+"\n\nWould you like to go on to the next level?",
							"Go on?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(result == JOptionPane.OK_OPTION){
						CampaignGame newGame = new CampaignGame(nextLevelID);
						String mapPath = String.format(path, nextLevelID);
						InputStream is = this.getClass().getResourceAsStream(mapPath);
						Map nextLevel = MapIO.getInstance().load(is, false);
						newGame.setMap(nextLevel);
						Main.getMainFrame().getGameField().setGame(newGame);
						newGame.start();
					}else {
						Main.getMainFrame().setMenuVisible(true);
					}
				}else {
					JOptionPane.showMessageDialog(Main.getMainFrame(), "Congratulations! You have mastered the campaign!", "Campaign Mastered!", JOptionPane.INFORMATION_MESSAGE);
					Main.getMainFrame().setMenuVisible(true);
				}
			} catch (Exception e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(Main.getMainFrame(), "ERROR!\n\n"+e, "ERROR!", JOptionPane.ERROR_MESSAGE);
			}
		}else{
//			JOptionPane.showMessageDialog(Main.getMainFrame(), getGameOverMessage());
			Main.getMainFrame().setMenuVisible(true);
		}
	}
}
