package tankz.model.game;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import tankz.controller.input.InputListener;
import tankz.main.Main;
import tankz.model.Direction;
import tankz.model.Projectile;
import tankz.model.Tank;
import tankz.model.map.NoMapException;
import tankz.net.client.Client;
import tankz.net.protocol.Protocol;
import tankz.net.protocol.Protocol.ACTION;

public class MPClientGame extends Game {

	private Client client = new Client();

	public MPClientGame(Client client) {
		setName("MPClientGame");
		this.client = client;
	}
	
	@Override
	public void init() throws NoMapException {
		//NOTHING TO DO AS INITIALIZED BY SERVER
		System.out.println("Initializing client game");
	}

	@Override
	public void update() {
		if (projectilesToFire.size() > 0) {
			PROJECTILES.addAll(projectilesToFire);
			projectilesToFire.clear();
		}

		if (projectilesToRemove.size() > 0) {
			PROJECTILES.removeAll(projectilesToRemove);
			projectilesToRemove.clear();
		}

		for (Projectile p : PROJECTILES) {
			p.increasePosition(true);
		}

		InputListener inputListener = Main.getMainFrame().getInputListener();
		executeInputActions(inputListener);

		checkGameState();
	}

	@Override
	public void executeInputActions(InputListener inputListener) {
		if (inputListener.getTankToControl() != null) {
			// Set aiming point of controlled tank
			Point aimPoint = inputListener.getMousePosition();
			Tank tank = inputListener.getTankToControl();
			Direction direction = inputListener.getMoveDirection();
			if (!tank.getAimingPoint().equals(aimPoint)) {
				tank.setAimingPoint(aimPoint);
				try {
					client.sendPacket(Protocol.buildAimPacket(aimPoint));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Move Tank
			if (direction != null) {
				inputListener.getTankToControl().move(direction, true);
				try {
					client.sendPacket(Protocol.buildMovePacket(direction));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			// Mouse Actions
			if (inputListener.wasMouseButtonClicked(MouseEvent.BUTTON1)) {
				inputListener.getTankToControl().shoot(aimPoint);
				try {
					client.sendPacket(Protocol.buildShootPacket(aimPoint));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("No tank defined to control.");
		}
		executeServerInput();
	}

	private void executeServerInput() {
		ArrayList<DatagramPacket> handledPackets = new ArrayList<DatagramPacket>();
		for (DatagramPacket packet : client.getPacketBuffer()) {
			try {
				receive(packet);
				handledPackets.add(packet);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for(DatagramPacket packet : handledPackets) {
			client.getPacketBuffer().remove(packet);
		}
	}

	private void receive(DatagramPacket packet) throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(Protocol.BUFFER_LENGTH);
		buffer.put(packet.getData());
		buffer.position(0);
		ACTION action = ACTION.values()[buffer.getInt()];
		switch (action) {
		case SET_POS_TANK:
			int entityID=buffer.getInt();
			Tank tank = getTankByID(entityID);
			tank.setPosition(buffer.getInt(), buffer.getInt());
			break;
		case SET_POS_PROJECTILE:
			entityID=buffer.getInt();
			Projectile p = getProjectileByID(entityID);
			if(p!=null) {
				p.setPosition(buffer.getInt(), buffer.getInt());
			}
			break;
		default:
			System.out.println("Received unknown packet from server " + packet.getAddress().getHostAddress() + ":"
					+ packet.getPort() + ": " + Arrays.toString(buffer.array()));
			client.getPacketBuffer().remove(packet);
			break;
		}
	}


}
