package tankz.model.map;

import java.awt.Dimension;

public class MapInfo {
	private Dimension size = null;
	private String name = MapIO.NO_VALUE;
	private String description = MapIO.NO_VALUE;
	private String author = MapIO.NO_VALUE;
	private String difficulty = MapIO.NO_VALUE;
	private int numberOfSpawns = 0;
	
	public MapInfo(){}
	
	public MapInfo(Dimension size, String name, String description,
			String author, String difficulty) {
		this.size = size;
		this.name = name;
		this.description = description;
		this.author = author;
		this.difficulty = difficulty;
	}

	public MapInfo(int width, int height, String name, String description,
			String author, String difficulty) {
		Dimension size = new Dimension(width, height);
		this.size = size;
		this.name = name;
		this.description = description;
		this.author = author;
		this.difficulty = difficulty;
	}
	
	public MapInfo(Dimension size, String name, String description,
			String author, String difficulty, int numberOfSpawns) {
		this.size = size;
		this.name = name;
		this.description = description;
		this.author = author;
		this.difficulty = difficulty;
		this.numberOfSpawns = numberOfSpawns;
	}

	public MapInfo(int width, int height, String name, String description,
			String author, String difficulty, int numberOfSpawns) {
		Dimension size = new Dimension(width, height);
		this.size = size;
		this.name = name;
		this.description = description;
		this.author = author;
		this.difficulty = difficulty;
		this.numberOfSpawns = numberOfSpawns;
	}
	
	public Dimension getSize() {
		return size;
	}
	public void setSize(Dimension size) {
		this.size = size;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public int getNumberOfSpawns() {
		return numberOfSpawns;
	}

	public void setNumberOfSpawns(int numberOfSpawns) {
		this.numberOfSpawns = numberOfSpawns;
	}
	
	
}
