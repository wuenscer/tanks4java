package tankz.model.map;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

import tankz.model.game.Game;
import tankz.util.ImageLoader;

public class MapElement {
	private Dimension size = new Dimension(Game.getFieldSize(),
			Game.getFieldSize());
	private Point location = new Point(0, 0);
	private boolean projectileCollision = false;
	private boolean walkable = true;
	private boolean spawnpoint = false;
	private boolean humanSpawnpoint = false;
	private String texturePath = "MISSING_TEXTURE";
	private BufferedImage texture = ImageLoader.getInstance().MISSING_TEXTURE;

	private int ID = 0;
	private Map parentMap = null;

	public MapElement() {
	}

	public MapElement(Map parent, Point location,
			boolean projectileCollision, boolean walkable, String texturePath, BufferedImage texture) {
		setParentMap(parent);
		setLocation(location);
		setProjectileCollision(projectileCollision);
		setWalkable(walkable);
		setTexturePath(texturePath);
		setTexture(texture);
	}

	public boolean isSpawnpoint() {
		return spawnpoint;
	}

	public void setSpawnpoint(boolean spawnpoint) {
		this.spawnpoint = spawnpoint;
	}

	public boolean isHumanSpawnpoint() {
		return humanSpawnpoint;
	}

	public void setHumanSpawnpoint(boolean humanSpawnpoint) {
		this.spawnpoint = true;
		this.humanSpawnpoint = humanSpawnpoint;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getID() {
		return ID;
	}
	
	public Dimension getSize() {
		return size;
	}

	public void setSize(Dimension size) {
		this.size = size;
	}

	/**
	 * Returns the location in terms of coordinates
	 * @return
	 */
	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	/**
	 * Returns the location in terms of pixels
	 * @return
	 */
	public Point getLocationOnScreen() {
		return new Point((getLocation().x) * size.width,
				(getLocation().y) * size.height);
	}

	public boolean hasProjectileCollision() {
		return projectileCollision;
	}

	public void setProjectileCollision(boolean projectileCollision) {
		this.projectileCollision = projectileCollision;
	}

	public boolean isWalkable() {
		return walkable;
	}

	public void setWalkable(boolean walkable) {
		this.walkable = walkable;
	}

	public String getTexturePath() {
		return texturePath;
	}

	public void setTexturePath(String texturePath) {
		this.texturePath = texturePath;
	}

	public BufferedImage getTexture() {
		return texture;
	}

	public void setTexture(BufferedImage texture) {
		this.texture = texture;
	}

	public Rectangle getRect() {
		return new Rectangle(getLocationOnScreen(), size);
	}

	public Map getParentMap() {
		return parentMap;
	}

	public void setParentMap(Map parentMap) {
		this.parentMap = parentMap;
		setID(parentMap.getNextElementID());
	}

	@Override
	public String toString() {
		String mapElementString = "";
		mapElementString += "MapElement: ";
//		mapElementString += "ID(" + getID() + ") ";
		mapElementString += "ArrayLocation(" + getLocation().x + "|"
				+ getLocation().y + ") ";
		mapElementString += "ScreenLocation(" + getLocationOnScreen().x + "|"
				+ getLocationOnScreen().y + ") ";
		mapElementString += "Collision(Tanks: " + isWalkable() + ", Projectiles: "+hasProjectileCollision()+") ";
		return mapElementString;
	}

	public ArrayList<MapElement> getNeighbours() {
		ArrayList<MapElement> neighbours = new ArrayList<MapElement>();
		HashMap<MapElement, Boolean> isNeighbour = new HashMap<MapElement, Boolean>();

		Point p_north = new Point(location.x, location.y - 1);
		Point p_north_east = new Point(location.x + 1, location.y - 1);
		Point p_north_west = new Point(location.x - 1, location.y - 1);
		Point p_east = new Point(location.x + 1, location.y);
		Point p_west = new Point(location.x - 1, location.y);
		Point p_south = new Point(location.x, location.y + 1);
		Point p_south_east = new Point(location.x + 1, location.y + 1);
		Point p_south_west = new Point(location.x - 1, location.y + 1);

		MapElement m_north = getParentMap().getMapElementByLocation(p_north);
		MapElement m_north_east = getParentMap().getMapElementByLocation(
				p_north_east);
		MapElement m_north_west = getParentMap().getMapElementByLocation(
				p_north_west);
		MapElement m_east = getParentMap().getMapElementByLocation(p_east);
		MapElement m_west = getParentMap().getMapElementByLocation(p_west);
		MapElement m_south = getParentMap().getMapElementByLocation(p_south);
		MapElement m_south_east = getParentMap().getMapElementByLocation(
				p_south_east);
		MapElement m_south_west = getParentMap().getMapElementByLocation(
				p_south_west);

		isNeighbour.put(m_north, false);
		isNeighbour.put(m_north_east, false);
		isNeighbour.put(m_north_west, false);
		isNeighbour.put(m_east, false);
		isNeighbour.put(m_west, false);
		isNeighbour.put(m_south, false);
		isNeighbour.put(m_south_east, false);
		isNeighbour.put(m_south_west, false);

		if (m_north != null && m_north.isWalkable()) {
			isNeighbour.put(m_north, true);
			neighbours.add(m_north);
		}
		if (m_east != null && m_east.isWalkable()) {
			isNeighbour.put(m_east, true);
			neighbours.add(m_east);
		}
		if (m_south != null && m_south.isWalkable()) {
			isNeighbour.put(m_south, true);
			neighbours.add(m_south);
		}
		if (m_west != null && m_west.isWalkable()) {
			isNeighbour.put(m_west, true);
			neighbours.add(m_west);
		}

		if (m_north_east != null && m_north_east.isWalkable()
				&& isNeighbour.get(m_north) && isNeighbour.get(m_east)) {
			isNeighbour.put(m_north_east, true);
			neighbours.add(m_north_east);
		}
		if (m_north_west != null && m_north_west.isWalkable()
				&& isNeighbour.get(m_north) && isNeighbour.get(m_west)) {
			isNeighbour.put(m_north_west, true);
			neighbours.add(m_north_west);
		}

		if (m_south_east != null && m_south_east.isWalkable()
				&& isNeighbour.get(m_south) && isNeighbour.get(m_east)) {
			isNeighbour.put(m_south_east, true);
			neighbours.add(m_south_east);
		}
		if (m_south_west != null && m_south_west.isWalkable()
				&& isNeighbour.get(m_south) && isNeighbour.get(m_west)) {
			isNeighbour.put(m_south_west, true);
			neighbours.add(m_south_west);
		}

		return neighbours;
	}

	public void render(Graphics2D g) {
		g.drawImage(getTexture(), getLocationOnScreen().x,
				getLocationOnScreen().y, size.width, size.height,
				null);
		/*
		if (!isWalkable()) {
			g.setColor(Color.BLACK);
			g.drawRect(getLocationOnScreen().x, getLocationOnScreen().y,
					DATA.getFieldSize(), DATA.getFieldSize());
		}
		*/
	}
	
	public void clear(){
		setWalkable(true);
		setProjectileCollision(false);
		setTexture(ImageLoader.getInstance().loadTexture("floor.jpg"));
		setTexturePath("floor.jpg");
		getParentMap().removeSpawnPoint(getLocation());
	}

	public void paste(MapElement copiedElement) {
		if(copiedElement != null){
			setProjectileCollision(copiedElement.hasProjectileCollision());
			setWalkable(copiedElement.isWalkable());
			setTexture(copiedElement.getTexture());
			setTexturePath(copiedElement.getTexturePath());
//			if(copiedElement.isSpawnPoint()){
//				new SpawnPoint(this, copiedElement.isHumanSpawnPoint());
//			}else{
//				this.setSpawnPoint(null);
//			}
		}
	}

}
