package tankz.model.map;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

import tankz.model.graph.Graph;
import tankz.util.ImageLoader;

public class MapIO {

	public static final String ELEMENT_ROOTELEMENT = "map";
	public static final String ELEMENT_MAP_DESCRIPTION = "description";
	public static final String ELEMENT_MAPELEMENT = "element";
	public static final String ELEMENT_SPAWNPOINT = "spawnpoint";

	public static final String ATTRIBUTE_MAPNAME = "name";
	public static final String ATTRIBUTE_AUTHOR = "author";
	public static final String ATTRIBUTE_DIFFICULTY = "difficulty";
	public static final String ATTRIBUTE_WIDTH = "width";
	public static final String ATTRIBUTE_HEIGHT = "height";

	public static final String ATTRIBUTE_ROW = "row";
	public static final String ATTRIBUTE_COL = "col";
	public static final String ATTRIBUTE_MAPELEMENT_PROJECTILE_COLLISION = "projectilecollision";
	public static final String ATTRIBUTE_MAPELEMENT_WALKABLE = "walkable";
	public static final String ATTRIBUTE_MAPELEMENT_TEXTURE = "texture";
	public static final String ATTRIBUTE_SPAWNPOINT_HUMAN = "human";

	public static final int MIN_MAPWIDTH = 2;
	public static final int MIN_MAPHEIGHT = 2;

	public static final String NO_VALUE = "Unknown";

	private static MapIO instance = null;
	private Map interpretedMap = null;
	private MapInfo mapInfo = null;
	
	private MapIO() {
	}

	public static MapIO getInstance() {
		if (instance == null) {
			instance = new MapIO();
		}
		return instance;
	}

	public Map load(File xmlMapFilePath, boolean ignoreExceptions)
			throws JDOMException, IOException, InvalidMapException {
		Document mapDocument = null;
		SAXBuilder builder = new SAXBuilder();
		mapDocument = builder.build(xmlMapFilePath);
		return interpret(mapDocument, ignoreExceptions);
	}

	public Map load(InputStream mapIS, boolean ignoreExceptions)
			throws JDOMException, IOException, InvalidMapException {
		Document mapDocument = null;
		SAXBuilder builder = new SAXBuilder();
		mapDocument = builder.build(mapIS);
		return interpret(mapDocument, ignoreExceptions);
	}

	public MapInfo loadInfo(File xmlMapFilePath, boolean ignoreExceptions)
			throws JDOMException, IOException, InvalidMapException {
		Document mapDocument = null;
		SAXBuilder builder = new SAXBuilder();
		mapDocument = builder.build(xmlMapFilePath);
		return getInfo(mapDocument, ignoreExceptions);
	}

	public MapInfo loadInfo(InputStream mapIS, boolean ignoreExceptions)
			throws JDOMException, IOException, InvalidMapException {
		Document mapDocument = null;
		SAXBuilder builder = new SAXBuilder();
		mapDocument = builder.build(mapIS);
		return getInfo(mapDocument, ignoreExceptions);
	}

	private MapInfo getInfo(Document mapDocument, boolean ignoreExceptions)
			throws InvalidMapException {
		if (mapDocument.hasRootElement()) {
			Element rootElement = mapDocument.getRootElement();
			if (rootElement.getName().equalsIgnoreCase(ELEMENT_ROOTELEMENT)) {

				// GET MAP NAME
				// IF THE NAME IS NOT PROVIDED IN THE DOCUMENT, THE NAME WILL BE
				// THE URI OF THE FILE
				String mapname;
				mapname = rootElement.getAttributeValue(ATTRIBUTE_MAPNAME);
				if (mapname == null || mapname.isEmpty()) {
					mapname = NO_VALUE;
				}

				String author;
				author = rootElement.getAttributeValue(ATTRIBUTE_AUTHOR);
				if (author == null || author.isEmpty()) {
					author = NO_VALUE;
				}

				String difficulty;
				difficulty = rootElement
						.getAttributeValue(ATTRIBUTE_DIFFICULTY);
				if (difficulty == null || difficulty.isEmpty()) {
					difficulty = NO_VALUE;
				}

				// GET DESCRIPTION OF MAP
				String description = rootElement
						.getChildText(ELEMENT_MAP_DESCRIPTION);
				description = (description == null || description.isEmpty()) ? NO_VALUE
						: description;

				// GET MAP SIZE
				boolean isSizeProvided = true;
				String sizeError = "Document does not provide correct SIZE INFORMATION:\n";

				// GET MAP WIDTH
				String mapwidth;
				int int_mapwidth = 0;

				if ((mapwidth = rootElement.getAttributeValue(ATTRIBUTE_WIDTH)) == null) {
					sizeError += "- Attribute WIDTH is not provided\n";
					isSizeProvided = false;
				} else {
					try {
						int_mapwidth = Integer.valueOf(mapwidth);
						if (int_mapwidth < MIN_MAPWIDTH) {
							sizeError += "- The value for WIDTH is too small (MINIMUM WIDTH: "
									+ MIN_MAPWIDTH + ")\n";
							isSizeProvided = false;
						}
					} catch (NumberFormatException e) {
						sizeError += "- Invalid value for WIDTH (" + mapwidth
								+ ")\n";
						isSizeProvided = false;
					}
				}

				if (!isSizeProvided) {
					throw new InvalidMapException(sizeError);
				}

				// GET MAP HEIGHT
				String mapheight;
				int int_mapheight = 0;

				if ((mapheight = rootElement
						.getAttributeValue(ATTRIBUTE_HEIGHT)) == null) {
					sizeError += "- Attribute HEIGHT is not provided\n";
					isSizeProvided = false;
				} else {
					try {
						int_mapheight = Integer.valueOf(mapheight);
						if (int_mapheight < MIN_MAPHEIGHT) {
							sizeError += "- The value for HEIGHT is too small (MINIMUM WIDTH: "
									+ MIN_MAPHEIGHT + ")\n";
							isSizeProvided = false;
						}
					} catch (NumberFormatException e) {
						sizeError += "- Invalid value for HEIGHT (" + mapheight
								+ ")\n";
						isSizeProvided = false;
					}
				}

				if (!isSizeProvided) {
					throw new InvalidMapException(sizeError);
				}
				
				mapInfo = new MapInfo(int_mapwidth, int_mapheight,
						mapname, description, author, difficulty);
				
				ArrayList<MapElement> spawnpoints = getSpawnpoints(rootElement);

				return new MapInfo(int_mapwidth, int_mapheight,
						mapname, description, author, difficulty, spawnpoints.size());
			} else {
				throw new InvalidMapException("Invalid root element ("
						+ rootElement.getName() + ") expected "
						+ ELEMENT_ROOTELEMENT);
			}
		} else {
			throw new InvalidMapException("No root element was found!"
					+ " expected " + "<" + ELEMENT_ROOTELEMENT + ">");
		}
	}

	private Map interpret(Document mapDocument, boolean ignoreExceptions)
			throws InvalidMapException {
		MapInfo info = getInfo(mapDocument, ignoreExceptions);
		interpretedMap = new Map(info);
		Element rootElement = mapDocument.getRootElement();
		// GET MAP ELEMENTS
		List<Element> mapElements;
		mapElements = rootElement.getChildren(ELEMENT_MAPELEMENT);
		for (Element element : mapElements) {
			MapElement mapElement = new MapElement();
			mapElement.setParentMap(interpretedMap);
			String malformedError = "Ignored map element:\n";
			boolean isMalformed = false;

			// GET ELEMENT TYPE
			Point location = new Point();
			boolean projectileCollision = false;
			boolean walkable = true;
			String texturePath = "MISSING_TEXTURE";
			BufferedImage texture = ImageLoader.getInstance().MISSING_TEXTURE;

			try {
				location = getLocationByAttributes(element);
			} catch (Exception e) {
				malformedError += e.getMessage();
				isMalformed = true;
			}

			if (!isMalformed) {
				// GET COLLISION
				String collision;
				if ((collision = element
						.getAttributeValue(ATTRIBUTE_MAPELEMENT_PROJECTILE_COLLISION)) == null) {
					// IF NO COLLISION ATTRIBUTE IS PROVIDED, THE MAP ELEMENT IS
					// SUPPOSED TO BE NOT COLLIDABLE
				} else if (collision.equalsIgnoreCase("true")) {
					projectileCollision = true;
				}
			}

			if (!isMalformed) {
				// GET WALKABLE
				String s_walkable;
				if ((s_walkable = element
						.getAttributeValue(ATTRIBUTE_MAPELEMENT_WALKABLE)) == null) {
					// IF NO WALKABLE ATTRIBUTE IS PROVIDED, THE MAP ELEMENT IS
					// SUPPOSED TO BE WALKABLE
				} else if (s_walkable.equalsIgnoreCase("false")) {
					walkable = false;
				}
			}

			if (!isMalformed) {
				// GET TEXTURE
				if ((texturePath = element
						.getAttributeValue(ATTRIBUTE_MAPELEMENT_TEXTURE)) != null) {
					texture = ImageLoader.getInstance()
							.loadTexture(texturePath);
				}
			}

			if (!isMalformed) {
				// set attributes to mapelement
				mapElement.setLocation(location);
				mapElement.setProjectileCollision(projectileCollision);
				mapElement.setWalkable(walkable);
				mapElement.setTexturePath(texturePath);
				mapElement.setTexture(texture);
				interpretedMap.addMapElement(mapElement);
			} else {
				System.err.println(malformedError);
			}
		}

		for(MapElement s : getSpawnpoints(rootElement)){
			interpretedMap.addSpawnPoint(s.getLocation(), s.isHumanSpawnpoint());
		}

		if (!interpretedMap.hasSpawnPoint()) {
			if (!ignoreExceptions) {
				throw new InvalidMapException(
						"Map doesn't have any spawn points.");
			}
		}

		interpretedMap.setGraph(new Graph(interpretedMap));

		return interpretedMap;
	}

	private ArrayList<MapElement> getSpawnpoints(Element rootElement) {
		// GET SPAWN POINTS
		List<Element> spawnpointElements;
		ArrayList<MapElement> spawnpoints = new ArrayList<MapElement>();
		spawnpointElements = rootElement.getChildren(ELEMENT_SPAWNPOINT);
		for (Element element : spawnpointElements) {
			boolean human = false;
			boolean invalid = false;
			String s_human = element
					.getAttributeValue(ATTRIBUTE_SPAWNPOINT_HUMAN);
			if (s_human == null || s_human.isEmpty()) {
				invalid = true;
			} else if (s_human.equalsIgnoreCase("true")) {
				human = true;
			} else if (s_human.equalsIgnoreCase("false")) {
				human = false;
			}
			if (!invalid) {
				try {
					Point spawnPoint = getLocationByAttributes(element);
					MapElement mapElement = new MapElement();
					mapElement.setLocation(spawnPoint);
					mapElement.setHumanSpawnpoint(human);
					spawnpoints.add(mapElement);
					// interpretedMap.addSpawnPoint(spawnPoint, human);
				} catch (Exception e) {
					System.err.println("Invalid Spawnpoint element! ("
							+ e + ")");
				}
			} else {
				System.err.println("Invalid Spawnpoint element! ("
						+ ATTRIBUTE_SPAWNPOINT_HUMAN
						+ " attribute not provided or empty)");
			}

		}
		return spawnpoints;
	}

	private Point getLocationByAttributes(Element element) throws Exception {
		String malformedError = "Ignored point:\n";
		boolean isMalformed = false;
		int int_col = 0;
		int int_row = 0;

		// GET ELEMENT LOCATION
		// GET ROW
		String row;
		if ((row = element.getAttributeValue(ATTRIBUTE_ROW)) == null) {
			malformedError += "- Attribute ROW is not provided\n";
			isMalformed = true;
		} else {
			try {
				int_row = Integer.valueOf(row);
			} catch (NumberFormatException e) {
				malformedError += "- Invalid value for ROW (" + row + ")\n";
				isMalformed = true;
			}
		}

		// GET COL
		String col;
		if ((col = element.getAttributeValue(ATTRIBUTE_COL)) == null) {
			malformedError += "- Attribute COL is not provided\n";
			isMalformed = true;
		} else {
			try {
				int_col = Integer.valueOf(col);
			} catch (NumberFormatException e) {
				malformedError += "- Invalid value for COL (" + col + ")\n";
				isMalformed = true;
			}
		}

		if (!isMalformed) {
			Point location = new Point(int_col, int_row);
			if (isLocationInMap(location)) {
				return location;
			} else {
				malformedError += "Point is located outside of the map";
				throw new Exception(malformedError);
			}
		} else {
			throw new Exception(malformedError);
		}
	}

	private boolean isLocationInMap(Point location) {
		Rectangle r = null;
		if(interpretedMap != null){
			r = interpretedMap.getRect();
		}else if(mapInfo != null){
			r = new Rectangle(mapInfo.getSize());
		}else{
			return false;
		}
		
		return r.contains(location);
		
	}

	public void save(Map mapToSave, File destination)
			throws FileNotFoundException, IOException {
		Document mapDocument = new Document();

		Element root = new Element(ELEMENT_ROOTELEMENT);
		root.setAttribute(ATTRIBUTE_WIDTH,
				Integer.toString(mapToSave.getSize().width));
		root.setAttribute(ATTRIBUTE_HEIGHT,
				Integer.toString(mapToSave.getSize().height));
		root.setAttribute(ATTRIBUTE_MAPNAME, mapToSave.getName());
		root.setAttribute(ATTRIBUTE_AUTHOR, mapToSave.getAuthor());
		root.setAttribute(ATTRIBUTE_DIFFICULTY, mapToSave.getDifficulty());

		Element mapDescription = new Element(ELEMENT_MAP_DESCRIPTION);
		mapDescription.addContent(mapToSave.getDescription());
		root.addContent(mapDescription);

		for (MapElement mapElement : mapToSave.getMapElements()) {
			Element xmlMapElement = new Element(ELEMENT_MAPELEMENT);
			xmlMapElement.setAttribute(ATTRIBUTE_COL,
					Integer.toString(mapElement.getLocation().x));
			xmlMapElement.setAttribute(ATTRIBUTE_ROW,
					Integer.toString(mapElement.getLocation().y));
			xmlMapElement.setAttribute(ATTRIBUTE_MAPELEMENT_WALKABLE,
					Boolean.toString(mapElement.isWalkable()));
			xmlMapElement.setAttribute(
					ATTRIBUTE_MAPELEMENT_PROJECTILE_COLLISION,
					Boolean.toString(mapElement.hasProjectileCollision()));
			xmlMapElement.setAttribute(ATTRIBUTE_MAPELEMENT_TEXTURE,
					mapElement.getTexturePath());
			if (mapElement.isSpawnpoint()) {
				Element spawnPoint = new Element(ELEMENT_SPAWNPOINT);
				spawnPoint.setAttribute(ATTRIBUTE_SPAWNPOINT_HUMAN,
						Boolean.toString(mapElement.isHumanSpawnpoint()));
			}
			root.addContent(xmlMapElement);
		}

		mapDocument.addContent(root);

		XMLOutputter out = new XMLOutputter();
		out.output(mapDocument, new FileOutputStream(destination));
	}

}
