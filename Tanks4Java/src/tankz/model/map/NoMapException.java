package tankz.model.map;

public class NoMapException extends Exception{

	private static final long serialVersionUID = -1317777327052779362L;

	public NoMapException(){
		super();
	}
	
	public NoMapException(String message){
		super(message);
	}
}
