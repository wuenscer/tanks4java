package tankz.model.map;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import tankz.model.game.Game;
import tankz.model.graph.Graph;
import tankz.util.ImageLoader;

public class Map {
	private ArrayList<MapElement> mapElements = new ArrayList<MapElement>();
	//private ArrayList<Point> spawnPoints = new ArrayList<Point>();
	private int mapElementID = 0;
	
	private Dimension size = null;
	private String name = "Unknown";
	private String description = "No description";
	private String author = "Unknown";
	private String difficulty = "Unknown";
	
	private Graph graph = null;
	
	public Map() {}
	
	public Map(Dimension size) {
		this.size = size;
		init();
	}

	public Map(Dimension size, String name) {
		setSize(size);
		setName(name);
		init();
	}
	
	public Map(MapInfo info){
		setInfo(info);
	}

	public int getNextElementID() {
		return mapElementID+=1;
	}
	
	public ArrayList<MapElement> getMapElements() {
		return mapElements;
	}

	public void setMapElements(ArrayList<MapElement> mapElements) {
		this.mapElements = mapElements;
	}

	public ArrayList<MapElement> getSpawnPoints() {
		ArrayList<MapElement> spawnpoints = new ArrayList<MapElement>();
		for(MapElement possibleSpawn : mapElements){
			if(possibleSpawn.isSpawnpoint()){
				spawnpoints.add(possibleSpawn);
			}
		}
		return spawnpoints;
	}

	public Dimension getSize() {
		return size;
	}

	public void setSize(Dimension size) {
		this.size = size;
		init();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public void setInfo(MapInfo info){
		setSize(info.getSize());
		setName(info.getName());
		setDescription(info.getDescription());
		setAuthor(info.getAuthor());
		setDifficulty(info.getDifficulty());
	}
	
	public MapInfo getInfo(){
		return new MapInfo(size.width, size.height, name, description, author, difficulty, getSpawnPoints().size());
	}
	
	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	public void init() {
		if(size != null){
			for(int y = 0; y < size.height; y++){
				for(int x = 0; x < size.width; x++){
					BufferedImage floor = ImageLoader.getInstance().loadTexture("floor.jpg");
					addMapElement(new MapElement(this, new Point(x, y), false, true, "floor.jpg", floor));
				}
			}
		}else{
			System.err.println("Could not initialize map: No size provided!");
		}
	}
	
	public boolean addMapElement(MapElement mapElement) {
		Point elementLocation = mapElement.getLocation();
		getMapElements().remove(getMapElementByLocation(elementLocation));
		return getMapElements().add(mapElement);
	}
	
	public boolean hasSpawnPoint(){
		if(getSpawnPoints().size() > 0){
			return true;
		}
		return false;
	}
	
//	public MapElement getMapElementByID(int ID){
//		for(MapElement me : getMapElements()){
//			if(me.getID() == ID){
//				return me;
//			}
//		}
//		return null;
//	}
	
	public MapElement getMapElementByLocation(Point location){
		for(MapElement me : getMapElements()){
			if(me.getLocation().x == location.x && me.getLocation().y == location.y){
				return me;
			}
		}
		return null;
	}

	public void addSpawnPoint(Point point, boolean human) {
		MapElement elementByPoint = getMapElementByLocation(point);
		elementByPoint.setHumanSpawnpoint(human);
	}
	
	public void removeSpawnPoint(Point point){
		MapElement elementByPoint = getMapElementByLocation(point);
		elementByPoint = (MapElement)elementByPoint;
	}

	public Rectangle getRect() {
		return new Rectangle(getSize());
	}
	
	public Rectangle getScreenRect(){
		return new Rectangle(getSize().width*Game.getFieldSize(),
				getSize().height*Game.getFieldSize());
	}

	public ArrayList<MapElement> getHumanSpawnPoints() {
		ArrayList<MapElement> humanSpawnPoints = new ArrayList<MapElement>();
		for(MapElement spawn : getSpawnPoints()) {
			if(spawn.isHumanSpawnpoint()) {
				humanSpawnPoints.add(spawn);
			}
		}
		return humanSpawnPoints;
	}
}
