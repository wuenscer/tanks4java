package tankz.model.map;

public class InvalidMapException extends Exception {

	private static final long serialVersionUID = -5124182141011531709L;

	public InvalidMapException(){
		super();
	}
	
	public InvalidMapException(String message){
		super(message);
	}
}
