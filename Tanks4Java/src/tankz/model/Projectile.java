package tankz.model;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.Collections;

import tankz.controller.console.ConsoleCommand;
import tankz.main.Main;
import tankz.model.game.Game;
import tankz.model.game.GameEntity;
import tankz.model.map.MapElement;
import tankz.util.ImageLoader;
import tankz.util.MathUtility;

public class Projectile extends GameEntity {

	private Dimension size = new Dimension(1, 1);
	private String name = "shell";
	private int id = 0;
	private Tank parentTank = null;
	private int damage = 10;

	private double speedMultiplier = 1.5;

	private boolean bouncing = false;
	private int bouncingAmount = 0;
	private int bouncingCount = 0;

	private Point start = new Point();
	private Point end = new Point();
	private double posX = 0.0;
	private double posY = 0.0;
	private Double angle = Double.NaN;
	private Double cosOfAngle = 0.0;
	private Double sinOfAngle = 0.0;
	private int additionalRotation = 90;

	public Projectile(int ID) {
		super(ID);
	}

	public Projectile(int ID, Dimension size, String name, Point start, Point end) {
		super(ID);
		this.size = size;
		this.name = name;
		this.start = start;
		this.end = end;
		this.posX = start.getX();
		this.posY = start.getY();
		this.angle = Math.atan2(end.x - start.x, -(end.y - start.y)) - (Math.PI / 2);
	}

	public Dimension getSize() {
		return size;
	}

	public void setSize(Dimension size) {
		this.size = size;
	}

	public double getX() {
		return this.posX;
	}

	public double getY() {
		return this.posY;
	}
	
	public Point getPosition() {
		return new Point((int)getX(), (int)getY());
	}
	
	public void setPosition(int x, int y) {
		this.posX = x;
		this.posY = y;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Tank getParentTank() {
		return parentTank;
	}

	public void setParentTank(Tank parentTank) {
		this.parentTank = parentTank;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public boolean isBouncing() {
		return bouncing;
	}

	public void setBouncing(boolean bouncing) {
		this.bouncing = bouncing;
	}

	public int getBouncingAmount() {
		return bouncingAmount;
	}

	public void setBouncingAmount(int bouncingAmount) {
		this.bouncingAmount = bouncingAmount;
	}

	public int getBouncingCount() {
		return bouncingCount;
	}

	public void setBouncingCount(int bouncingCount) {
		this.bouncingCount = bouncingCount;
	}

	public Point getStart() {
		return start;
	}

	public void setStart(Point start) {
		this.start = start;
		this.posX = start.getX();
		this.posY = start.getY();
	}

	public Point getEnd() {
		return end;
	}

	public void setEnd(Point end) {
		this.end = end;
	}

	public Rectangle getRect() {
		return new Rectangle(new Point((int) getX(), (int) getY()), getSize());
	}

	public Shape getTransformedRect() {
		AffineTransform transformation = new AffineTransform();
		Rectangle rect = getRect();
		// transformation
		transformation.rotate(Math.toRadians(Math.toDegrees(this.angle) + additionalRotation), rect.getCenterX(),
				rect.getMinY());
		Shape transformedRect = transformation.createTransformedShape(getRect());
		return transformedRect;
	}

	public double getSpeedMultiplier() {
		return speedMultiplier;
	}

	public void setSpeedMultiplier(double speedMultiplier) {
		this.speedMultiplier = speedMultiplier;
	}

	public Double getAngle() {
		return angle;
	}

	public void setAngle(Double angle) {
		this.angle = angle;
	}

	public Double getCosOfAngle() {
		return cosOfAngle;
	}

	public void setCosOfAngle(Double cosOfAngle) {
		this.cosOfAngle = cosOfAngle;
	}

	public Double getSinOfAngle() {
		return sinOfAngle;
	}

	public void setSinOfAngle(Double sinOfAngle) {
		this.sinOfAngle = sinOfAngle;
	}

	public boolean increasePosition(boolean checkCollision) {
		// calculate the angle of the projectile
		// (only at first method call)
		if (this.angle.isNaN()) {
			this.angle = Math.atan2(end.x - start.x, -(end.y - start.y)) - (Math.PI / 2);
			this.cosOfAngle = Math.cos(angle) * speedMultiplier;
			this.sinOfAngle = Math.sin(angle) * speedMultiplier;
		}

		// actual increasing of the position
		posX += cosOfAngle;
		posY += sinOfAngle;

		if (checkCollision) {
			return checkCollision();
		}

		return false;
	}

	private boolean checkCollision() {
		boolean collision = false;

		// Check collision with projectiles
		for (Projectile p : Collections.unmodifiableList(getGame().PROJECTILES)) {
			if (!p.equals(this) && this.getTransformedRect().intersects(p.getTransformedRect().getBounds2D())) {
				getGame().removeProjectile(this);
				getGame().removeProjectile(p);
				collision = true;
			}
		}

		// Check collision with tanks
		for (Tank t : getGame().getTanks()) {
			if (!t.isDead()) {
				if (this.getTransformedRect().intersects(t.getRect())) {
					System.out.println("Tank '" + t.getName() + "' was hit by a projectile(ID:" + getId()
							+ ") of type '" + this.getName() + "' by tank '" + getParentTank().getName());
					t.hit(getDamage());
					getGame().removeProjectile(this);
					collision = true;
				}
			}
		}

		// Check collision with map elements
		for (MapElement e : getGame().getActualMap().getMapElements()) {
			if (e.hasProjectileCollision() && this.getTransformedRect().intersects(e.getRect())) {
				if (this.isBouncing() && getBouncingCount() < this.getBouncingAmount()) {
					invertDirection(MathUtility.getImpactDirection(e.getRect(), this.getTransformedRect().getBounds()),
							e);
					setBouncingCount(getBouncingCount() + 1);
				} else {
					getGame().removeProjectile(this);
				}

				collision = true;
			}
		}

		// Check collision with GameField border
		Rectangle gamefieldArea = getGame().getActualMap().getScreenRect();
		if (!gamefieldArea.contains(getRect())) {
			getGame().removeProjectile(this);
			collision = true;
		}

		return collision;
	}

	private Game getGame() {
		return Main.getMainFrame().getGameField().getGame();
	}

	private void invertDirection(Direction impactDirection, MapElement impactedElement) {
		if (impactDirection == Direction.NORTH || impactDirection == Direction.SOUTH) {
			sinOfAngle *= -1;
			angle *= -1;
		} else if (impactDirection == Direction.EAST || impactDirection == Direction.WEST) {
			cosOfAngle *= -1;
			angle *= -1;
			additionalRotation *= -1;
		} else {
//			System.out.println("Projectile hit the edge of an element. Inverting both axes");
			sinOfAngle *= -1;
			cosOfAngle *= -1;
		}

		// INCREASE POSITION SO THERE IS NO
		// COLLISION AFTER TURNING INTO A WALL
		if (impactedElement != null) {
			while (getTransformedRect().intersects(impactedElement.getRect())) {
				increasePosition(false);
			}
		}

	}

	public Projectile copy() {
		Projectile p = new Projectile(0);
		p.setBouncing(isBouncing());
		p.setBouncingAmount(getBouncingAmount());
		p.setBouncingCount(getBouncingCount());
		p.setDamage(getDamage());
		p.setEnd(getEnd());
		p.setName(getName());
		p.setParentTank(getParentTank());
		p.setSize(getSize());
		p.setStart(getStart());
		return p;
	}

	public void render(Graphics2D g) {
		AffineTransform initialTransform = g.getTransform();
		Rectangle rect = getRect();
		g.rotate(Math.toRadians(Math.toDegrees(this.angle) + additionalRotation), rect.getCenterX(), rect.getMinY());
		g.drawImage(ImageLoader.getInstance().loadTexture(name + ".png"), rect.x, rect.y, rect.width, rect.height,
				null);
		g.setTransform(initialTransform);
		if (ConsoleCommand.DEBUG.getValue()) {
			g.setColor(Color.RED);
			g.draw(getTransformedRect());
		}
	}

}
