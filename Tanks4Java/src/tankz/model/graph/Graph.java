package tankz.model.graph;

import java.util.ArrayList;
import java.util.HashMap;

import tankz.model.map.Map;
import tankz.model.map.MapElement;

/**
 * 
 * @author Marc Fernandes 
 * Nutzungsbeispiel: 
 * 		ArrayList<NodeFigure> list = new
 *         ArrayList<NodeFigure>(); list.addAll(map.getRooms().values()); Graph
 *         g = new Graph(list, map.getPaths());
 *         g.printDistancesFromStartNode(g.getNodes().get(0), new
 *         ArrayList<Node>()); g.getShortestPath(g.getNodes().get(0),
 *         g.getNodes().get(6), new ArrayList<Node>());
 */
public class Graph {
	Map parentMap = null;
	ArrayList<Node> nodes = new ArrayList<Node>();
	ArrayList<Edge> edges = new ArrayList<Edge>();
	HashMap<Node, Double> distance = new HashMap<Node, Double>();
	HashMap<Node, Node> predecessor = new HashMap<Node, Node>();
	HashMap<Node, ArrayList<Edge>> node_edges = new HashMap<Node, ArrayList<Edge>>();

	/**
	 * In this map, all possible shortest paths should be stored, 
	 * to save time compared with the calculation of the shortest path at runtime.
	 * 
	 * The paths are stored with a key that follows these rules:
	 * <code>startElementID_endElementID</code>
	 */
	HashMap<String, ArrayList<MapElement>> ways = new HashMap<String, ArrayList<MapElement>>();
	
	public Graph(Map actualMap) {
		parentMap = actualMap;
		System.out.println("Initializing Graph nodes...");
		for (MapElement me : actualMap.getMapElements()) {
			if (me.isWalkable()) {
				Node node = new Node(me, Double.POSITIVE_INFINITY);
				nodes.add(node);
			}
		}
		System.out.println("Done!");
		System.out.println("Initializing nodes edges...");
		for (int i = 0; i < nodes.size(); i++) {
			Node start = nodes.get(i);

			// Get neighbours of node "start"
			ArrayList<MapElement> neighbours = start.getMapElement()
					.getNeighbours();
			if (node_edges.get(start) == null) {
				node_edges.put(start, new ArrayList<Edge>());
			}
			for (MapElement neighbour : neighbours) {
				Node end = getNode(neighbour);
				double distance = start
						.getMapElement()
						.getLocation()
						.distance(end.getMapElement().getLocation().x,
								end.getMapElement().getLocation().y);
				Edge e = new Edge(start, end, distance);
				node_edges.get(start).add(e);
				edges.add(e);
			}
		}
		
		System.out.println("Done!");
		
		initAllWayS();

	}

	public ArrayList<Node> getNodes() {
		return this.nodes;
	}

	public ArrayList<Edge> getEdges() {
		return this.edges;
	}

	private void initialize(Node start, ArrayList<Node> Q) {
//		System.out.println("Initialize nodes with unlimited distance...");
		distance = new HashMap<Node, Double>();
		predecessor = new HashMap<Node, Node>();

		for (int i = 0; i < nodes.size(); i++) {
			if (nodes.get(i).equals(start)) {
				distance.put(nodes.get(i), (double)0.0);
				predecessor.put(nodes.get(i), null);
			} else {
				distance.put(nodes.get(i), Double.POSITIVE_INFINITY);
				predecessor.put(nodes.get(i), null);
			}
			Q.add(nodes.get(i));
		}
//		System.out.println("Done!");
	}

	private void dijkstra(Node start, ArrayList<Node> Q) {
		initialize(start, Q);
		
		while (Q.size() > 0) {
			int tmp_index = -1;
			Node n = null;
			double tmp_dist = Double.POSITIVE_INFINITY;
			for (int i = 0; i < Q.size(); i++) {
				Node n_temp = Q.get(i);
				if (distance.get(n_temp).doubleValue() < tmp_dist) {
					n = n_temp;
					tmp_dist = distance.get(n).doubleValue();
					tmp_index = i;
				}
			}
			if (n == null) {
				break;
			}
			Q.remove(tmp_index);

			ArrayList<Edge> es = node_edges.get(n);
			for (int i = 0; i < es.size(); i++) {
				double dist = es.get(i).getDistance();
				Node end = es.get(i).getEndNode();

				double alt = distance.get(n) + dist;
				if (alt < distance.get(end)) {
					distance.put(end, (double) alt);
					predecessor.put(end, n);
				}
			}
		}
	}

	public void printDistancesFromStartNode(Node start, ArrayList<Node> Q) {
		dijkstra(start, Q);
		for (int i = 0; i < nodes.size(); i++) {
			String name = " - ";
			if (predecessor.get(nodes.get(i)) != null)
				name = ""+predecessor.get(nodes.get(i)).getMapElement().getID()+"";
			System.out.print("Von " + name + " zu "
					+ nodes.get(i).getMapElement().getID() + "=");
			System.out.println(distance.get(nodes.get(i)));
		}
	}

	/**
	 * This method returns a shortest path, that is stored in {@link Graph#ways}.
	 * 
	 * @param startElement
	 * @param endElement
	 * @return The shortest path from startElement to endElement ({@link ArrayList}<{@link MapElement}>)
	 */
	public ArrayList<MapElement> getShortestPath(MapElement startElement, MapElement endElement) {
		ArrayList<MapElement> actualpath = new ArrayList<MapElement>();
		actualpath = ways.get(startElement.getID()+"_"+endElement.getID());
		if(actualpath != null){
			return actualpath;
		}else{
			return new ArrayList<MapElement>();
		}
	}
	
	public ArrayList<MapElement> getShortestPath_calculating(MapElement startElement, MapElement endElement) {
		Node start = getNode(startElement);
		Node end = getNode(endElement);
		ArrayList<Node> Q = new ArrayList<Node>();
		ArrayList<MapElement> retVal = new ArrayList<MapElement>();
		ArrayList<Node> retValtmp = new ArrayList<Node>();
		
		dijkstra(start, Q);
		
		Node pred = predecessor.get(end);
		// double dist = distance.get(end); //This is the complete distance...
		retValtmp.add(end);
		while (pred != null) {
			retValtmp.add(pred);
			pred = predecessor.get(pred);
		}

		/*
		 * Now everything is in the reverse order ...
		 */
		for (int i = retValtmp.size() - 1; i >= 0; i--) {
			retVal.add(retValtmp.get(i).getMapElement());
		}
		
		return retVal;
	}
	
	private void initAllWayS(){
		if(parentMap == null){
			return;
		}
		System.out.println("Initializing all possible ways...");
		for(MapElement start : parentMap.getMapElements()){
			for(MapElement end : parentMap.getMapElements()){
				if(start.isWalkable() && end.isWalkable()){
					String key = start.getID()+"_"+end.getID();
					String reverse_key = end.getID()+"_"+start.getID();
					ArrayList<MapElement> way = null;
					if((way = ways.get(reverse_key)) != null){
						ArrayList<MapElement> reverseWay = new ArrayList<MapElement>();
						for (int i = way.size() - 1; i >= 0; i--) {
							reverseWay.add(way.get(i));
						}
						ways.put(key, reverseWay);
					}else{
//						System.out.println("Calculating path from "+start.getID()+"->"+end.getID());
						ways.put(key, getShortestPath_calculating(start, end));
					}
				}
			}
		}
		System.out.println("Done!");
		System.out.println(ways.size() + "possible ways have been computed.");
	}
	
	private Node getNode(MapElement mapElement) {
		for (int i = 0; i < nodes.size(); i++) {
			if (nodes.get(i).getMapElement().getLocation().equals(mapElement.getLocation())) {
				return nodes.get(i);
			}
		}
		return null;
	}
}
