package tankz.model.graph;

public class Edge {
  private Double distance;
  private Node start;
  private Node end;
  
  public Edge(Node start, Node end, Double dist) {
	  this.distance = dist;	  
	  this.start=start;
	  this.end=end;
  }
  
  public Double getDistance() {
	  return this.distance;
  }
    
  public Node getStartNode() {
	  return this.start;
  }
  
  public Node getEndNode() {
	  return this.end;
  }
}
