package tankz.model.graph;

import tankz.model.map.MapElement;

public class Node {

	private MapElement mapElement = null;
	private Double distance = Double.POSITIVE_INFINITY;

	public Node(MapElement mapElement, Double distance) {
		this.mapElement = mapElement;
		this.distance = distance;
	}

	public Double getDistance() {
		return this.distance;
	}

	public MapElement getMapElement() {
		return this.mapElement;
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof Node){
			Node comparable = (Node)object;
			if(comparable.getMapElement().getLocation().equals(this.getMapElement().getLocation())){
				return true;
			}
		}
		return false;
	}
}
