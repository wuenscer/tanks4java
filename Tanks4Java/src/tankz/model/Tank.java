package tankz.model;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import tankz.controller.ai.AIControl;
import tankz.main.Main;
import tankz.model.game.Game;
import tankz.model.game.GameEntity;
import tankz.model.map.MapElement;
import tankz.util.ImageLoader;
import tankz.util.MathUtility;
import tankz.util.Sounds;

public class Tank  extends GameEntity {
	private Dimension size = new Dimension(Game.getFieldSize(), Game.getFieldSize());
	private Point position = new Point(0, 0);
	private String name = "Tank";

	private int speed = 1;
	private double speedMultiplier = 1.0;
	private boolean dead = false;
	private int maxHealth = 100;
	private int health = maxHealth;

	private Direction walkingDirection = Direction.NORTH;

	//milliseconds
	private long reloadTimeOut = 2000L;
	private long reloadStartTime = 0L;
	private int magazinSize = 3;
	private int roundsInMagazin = magazinSize;

	//milliseconds
	private long shootTimeOut = 300L;
	private long lastShootTime = 0L;

	private Point aimingPoint = new Point();

	private Projectile projectileType = new Projectile(0) {
		{
			setSize(new Dimension(10, 20));
			setBouncing(true);
			setBouncingAmount(1);
			setName("shell");
			setSpeedMultiplier(2.0);
		}
	};

	private boolean human = false;

	public Tank(int ID) {
		super(ID);
	}
	
	public Tank(int ID, Dimension size, Point position, String name) {
		super(ID);
		this.size = size;
		this.position = position;
		this.name = name;
	}

	public Dimension getSize() {
		return size;
	}

	public void setSize(Dimension size) {
		this.size = size;
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}
	
	public void setPosition(int x, int y) {
		this.position.x = x;
		this.position.y = y;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public double getSpeedMultiplier() {
		return speedMultiplier;
	}

	public void setSpeedMultiplier(double speedMultiplier) {
		this.speedMultiplier = speedMultiplier;
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public Direction getWalkingDirection() {
		return walkingDirection;
	}

	public void setWalkingDirection(Direction walkingDirection) {
		this.walkingDirection = walkingDirection;
	}

	public long getReloadTime() {
		return reloadTimeOut;
	}

	public void setReloadTime(long reloadTimeOut) {
		this.reloadTimeOut = reloadTimeOut;
	}

	public int getMagazinSize() {
		return magazinSize;
	}

	public void setMagazinSize(int magazinSize) {
		this.magazinSize = magazinSize;
	}

	public int getRoundsInMagazin() {
		return roundsInMagazin;
	}

	public void setRoundsInMagazin(int roundsInMagazin) {
		this.roundsInMagazin = roundsInMagazin;
	}

	public boolean isReloading(){
		long now = System.currentTimeMillis();
		if((now - reloadStartTime) > reloadTimeOut){
			return false;
		}
		return true; 
	}

	public long getShootTimeOut() {
		return shootTimeOut;
	}

	public void setShootTimeOut(int shootTimeOut) {
		this.shootTimeOut = shootTimeOut;
	}
	
	public boolean isShootTimeOut(){
		if((System.currentTimeMillis() - lastShootTime) > shootTimeOut){
			return false;
		}
		return true; 
	}

	public Point getAimingPoint() {
		return aimingPoint;
	}

	public void setAimingPoint(Point aimingPoint) {
		this.aimingPoint = aimingPoint;
	}

	public boolean isHuman() {
		return human;
	}

	public void setHuman(boolean human) {
		this.human = human;
	}

	public Rectangle getRect() {
		return new Rectangle(getPosition(), size);
	}

	public Point getCenter() {
		return new Point((int) getRect().getCenterX(), (int) getRect().getCenterY());
	}

	public Color getHealthColor() {
		int healthQuarter = getMaxHealth() / 4;
		Color healthColor = Color.GREEN;
		if (health <= (healthQuarter * 3) && health > healthQuarter) {
			healthColor = Color.YELLOW;
		} else if (health <= healthQuarter) {
			healthColor = Color.RED;
		}
		return healthColor;
	}

	public Rectangle getHealthBar() {
		double rectWidth1 = ((double) getSize().width) / ((double) getMaxHealth());
		double rectWidth2 = rectWidth1 * getHealth();
		return new Rectangle(this.getPosition().x, getPosition().y + getSize().height + 1, (int) rectWidth2, 5);
	}

	public void hit(int damage) {
		setHealth(getHealth() - damage);
		if (getHealth() <= 0) {
			setDead(true);
			setHealth(0);
			Sounds.playResourceSound("explosion01.wav");
		}
	}

	public boolean move(Direction direction, boolean setDirection) {
		if (direction != null) {
			if(setDirection){
				setWalkingDirection(direction);
			}
			switch (direction) {
			case NORTH:
				if (!collides(direction)) {
					getPosition().y -= getSpeed();
					return true;
				}
				break;
			case EAST:
				if (!collides(direction)) {
					getPosition().x += getSpeed();
					return true;
				}
				break;
			case WEST:
				if (!collides(direction)) {
					getPosition().x -= getSpeed();
					return true;
				}
				break;
			case SOUTH:
				if (!collides(direction)) {
					getPosition().y += getSpeed();
					return true;
				}
				break;
			case SOUTH_EAST:
				move(Direction.SOUTH, false);
				move(Direction.EAST, false);
				return true;
			case SOUTH_WEST:
				move(Direction.SOUTH, false);
				move(Direction.WEST, false);
				return true;
			case NORTH_EAST:
				move(Direction.NORTH, false);
				move(Direction.EAST, false);
				return true;
			case NORTH_WEST:
				move(Direction.NORTH, false);
				move(Direction.WEST, false);
				return true;
			default:
				return false;
			}
		}
		return false;
	}

	private boolean collides(Direction direction) {
		Rectangle actualRect = getRect();
		Rectangle gameField = Main.getMainFrame().getGameField().getGame().getActualMap().getScreenRect();

		switch (direction) {
		case NORTH:
			actualRect.y -= getSpeed();
			break;
		case EAST:
			actualRect.x += getSpeed();
			break;
		case WEST:
			actualRect.x -= getSpeed();
			break;
		case SOUTH:
			actualRect.y += getSpeed();
			break;
		default:
			break;
		}

		// Check collision with GameField border
		if (gameField.contains(actualRect)) {
			// Check collision with map elements
			for (MapElement mapElement : Main.getMainFrame().getGameField().getGame().getActualMap().getMapElements()) {
				Rectangle mapElementRect = mapElement.getRect();
				if (!mapElement.isWalkable() && actualRect.intersects(mapElementRect)) {
					// System.err.println("tank collides with:
					// "+mapElement.toString());
					return true;
				}
			}

			// check collision with other tanks
			for (Tank t : Main.getMainFrame().getGameField().getGame().getTanks()) {
				if (!t.equals(this) && !t.isDead() && actualRect.intersects(t.getRect())) {
					// System.err.println("tank collides with: other tank");
					return true;
				}
			}
		} else {
			// System.err.println("tank collides with: GameField border");
			return true;
		}

		return false;
	}

	public Projectile getProjectileType() {
		return projectileType;
	}

	public void setProjectileType(Projectile projectileType) {
		this.projectileType = projectileType;
	}

	public void shoot(Point mousePosition) {
		if (!isReloading() && !isShootTimeOut()) {
			Point end = new Point(mousePosition);
			Point2D startAimingLine = new Point2D.Double(getRect().getCenterX(), getRect().getCenterY());
			Point2D endAimingLine = new Point2D.Double(end.x, end.y);

			Line2D aimingLine = new Line2D.Double(startAimingLine, endAimingLine);
			// Direction direction = MathUtility.getImpactDirection(getRect(),
			// aimingLine);
			Point intersectionPoint;
			try {
				intersectionPoint = MathUtility.getIntersectionPoint(getRect(), aimingLine);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
			Projectile projectileToFire = getProjectileType().copy();
			projectileToFire.setParentTank(this);

			projectileToFire.setStart(intersectionPoint);
			projectileToFire.setEnd(end);

			while (projectileToFire.getTransformedRect().intersects(getRect())) {
				projectileToFire.increasePosition(false);
			}

			setRoundsInMagazin(getRoundsInMagazin() - 1);

			Main.getMainFrame().getGameField().getGame().fireProjectile(projectileToFire);

			Sounds.playResourceSound("shot01.wav");

			//Set Reload or shoot timeout
			if (getRoundsInMagazin() == 0) {
				reloadStartTime = System.currentTimeMillis();
				setRoundsInMagazin(magazinSize);
			} else {
				lastShootTime = System.currentTimeMillis();
			}
		}
	}

	/**
	 * 
	 * @param ceiling - why?
	 * @return
	 */
	public Point getArrayLocation(boolean ceiling) {
		Point arrayLocation = new Point();
		double x = ((double) getPosition().x) / ((double) Main.getMainFrame().getGameField().getGame().getFieldSize());
		double y = ((double) getPosition().y) / ((double) Main.getMainFrame().getGameField().getGame().getFieldSize());
		if (ceiling) {
			arrayLocation.x = (int) Math.ceil(x);
			arrayLocation.y = (int) Math.ceil(y);
		} else {
			arrayLocation.x = (int) x;
			arrayLocation.y = (int) y;
		}
		return arrayLocation;
	}

	private double getRotation(Direction direction) {
		double onePiece = 45;
		switch (direction) {
		case NORTH:
			return Math.toRadians(0);
		case NORTH_EAST:
			return Math.toRadians(onePiece);
		case EAST:
			return Math.toRadians(2 * onePiece);
		case SOUTH_EAST:
			return Math.toRadians(3 * onePiece);
		case SOUTH:
			return Math.toRadians(4 * onePiece);
		case SOUTH_WEST:
			return Math.toRadians(-(3 * onePiece));
		case WEST:
			return Math.toRadians(-(2 * onePiece));
		case NORTH_WEST:
			return Math.toRadians(-(onePiece));
		default:
			return Math.toRadians(0);
		}
	}

	public void render(Graphics2D g) {
		AffineTransform initialTransform = g.getTransform();

		if (!isDead()) {
			// Draw tank body
			g.rotate(getRotation(getWalkingDirection()), getRect().getCenterX(), getRect().getCenterY());
			g.drawImage(ImageLoader.getInstance().loadTexture("tank_body.png"), getPosition().x, getPosition().y,
					getSize().width, getSize().height, null);
			g.setTransform(initialTransform);

			// Draw tank tower
			Point center = new Point((int) getRect().getCenterX(), (int) getRect().getCenterY());
			Point end = getAimingPoint();
			double angle = (Math.atan2(end.x - center.x, -(end.y - center.y)) - (Math.PI / 2));
			angle = Math.toRadians(Math.toDegrees(angle) + 90);
			g.rotate(angle, center.x, center.y);
			g.drawImage(ImageLoader.getInstance().loadTexture("tank_tower.png"), getPosition().x, getPosition().y,
					getSize().width, getSize().height, null);
			g.setTransform(initialTransform);

			// Draw name
			g.setColor(Color.RED);
			g.drawString(this.getName(), this.getPosition().x, this.getPosition().y);

			// Draw aiming line
			if (this.equals(Main.getMainFrame().getInputListener().getTankToControl())) {
				g.drawLine((int) getRect().getCenterX(), (int) getRect().getCenterY(), aimingPoint.x, aimingPoint.y);
			}

			// Draw health bar
			g.setColor(this.getHealthColor());
			Rectangle healthBar = this.getHealthBar();
			g.fillRect(healthBar.x, healthBar.y, healthBar.width, healthBar.height);

			g.setTransform(initialTransform);

			// TODO TEST VISUAL DEBUG
			if (!isHuman() && AIControl.isDebugVisual()) {
				Rectangle lane = new Rectangle();
				lane.width = getProjectileType().getSize().width + 1; // +1
																		// because
																		// of
																		// not
																		// precise
																		// firing
																		// of
																		// bullets
				lane.height = (int) getCenter().distance((Point2D) getAimingPoint());
				lane.x = getCenter().x - (lane.width / 2);
				lane.y = getCenter().y - lane.height;

				Area a = new Area(lane);
				AffineTransform af = new AffineTransform();
				af.rotate(angle, lane.getCenterX(), lane.getMaxY());
				a = a.createTransformedArea(af);

				g.fillRect(a.getBounds().x, a.getBounds().y, a.getBounds().width, a.getBounds().height);
			}

		} else {
			// Draw dead tank
			g.drawImage(ImageLoader.getInstance().loadTexture("tank_dead.png"), getPosition().x, getPosition().y,
					Main.getMainFrame().getGameField().getGame().getFieldSize(), Main.getMainFrame().getGameField().getGame().getFieldSize(), null);
		}

	}
	
	@Override
	public String toString() {
		return getName() + "("+getID()+")";
	}
}
