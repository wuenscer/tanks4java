package tankz.storage;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import tankz.util.Utils;

public class Database {

	/**
	 * Folder in which the database file is located
	 * Should be "installpath/data/"
	 */
	public static String databaseFolder = Utils.getFolderOfJAR().getParent()
			+ File.separator + "data" + File.separator;
	
	/**
	 * The name of the database file ("data")
	 */
	public static final String databaseName = "data";

	/**
	 * The Database instance for which connections are opened
	 */
	private static Database instance = null;
	
	/**
	 * Properties for the database connection (username, password, etc)
	 */
	private static Properties properties = new Properties();

	/**
	 * The default constructor for the database instance<br><br>
	 * This constructor loads the database driver and creates the database
	 * with its structure in case it doesn't exist.
	 * 
	 * @throws SQLException if the structure of the database cannot be created
	 * @throws ClassNotFoundException if the database driver cannot be loaded
	 */
	private Database() throws ClassNotFoundException, SQLException {
//		DriverManager.setLogWriter(new PrintWriter(System.out));
		Class.forName("org.h2.Driver");
		properties.put("user", "tankz");
		properties.put("password", "tankz");
		try{
			getConnection().close();
		}catch(SQLException exception){
			populateStructure();
		}
		updateLevels();
	}

	/**
	 * Creates and returns the database instance
	 * 
	 * @return {@link Database} the database instance
	 * @throws ClassNotFoundException see {@link #Database()}
	 * @throws SQLException see {@link #Database()}
	 */
	public static Database getInstance() throws ClassNotFoundException,
			SQLException {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}

	/**
	 * Returns a {@link Connection} to the database instance if the database exists
	 * @return {@link Connection} to the database instance
	 * @throws SQLException if the database doesn't exist or another access error occurs
	 */
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:h2:"
				+ databaseFolder + databaseName + ";IFEXISTS=TRUE", properties);
	}

	/**
	 * Queries an existing database instance with the given query
	 * @param query {@link String} an SQL query
	 * @return {@link ResultSet} of the query 
	 * @throws SQLException if a database access error occurs, 
	 * this method is called on a closed Statement, 
	 * the given SQL statement produces anything other 
	 * than a single ResultSet object
	 */
	public ResultSet query(String query) throws SQLException{
		Connection connection = getConnection();
		Statement stmt = connection.createStatement();
		ResultSet result = stmt.executeQuery(query);
		//connection.close();
		return result;
	}
	
	/**
	 * executes any SQL statement and creates the database if it doesn't exist 
	 * @param sql {@link String}
	 * @throws SQLException if a database access error occurs, this method is called on a closed Statement
	 */
	public void execute(String sql) throws SQLException{
		Connection connection = getConnection();
		Statement stmt = connection.createStatement();
		stmt.execute(sql);
		connection.close();
	}
	
	/**
	 * Executes any SQL statement on the given {@link Connection}.
	 * @param sql {@link String} an SQL statement
	 * @param connection {@link Connection}
	 * @throws SQLException if a database access error occurs, this method is called on a closed Statement
	 */
	public void execute(String sql, Connection connection) throws SQLException{
		Statement stmt = connection.createStatement();
		stmt.execute(sql);
		connection.close();
	}
	
	/**
	 * Creates the database structure
	 * @throws SQLException if a database access error occurs, this method is called on a closed Statement
	 */
	public void populateStructure() throws SQLException{
		String tableStructure =
			"CREATE TABLE CAMPAIGN ("+
				"LEVEL_ID INT PRIMARY KEY,"+
				"LEVEL_NAME VARCHAR NOT NULL,"+
				"UNLOCKED INT DEFAULT 0"+
			")";
		execute(tableStructure, DriverManager.getConnection("jdbc:h2:"
				+ databaseFolder + databaseName, properties));
	}
	
	/**
	 * Gets the amount of levels in the database 
	 * and searches for new ones in the file system
	 * to add them to the database as locked
	 * @throws SQLException
	 */
	public void updateLevels() throws SQLException{
		int levelNo = 0;
		
		String select = 
				"SELECT COUNT(*) FROM CAMPAIGN";
		ResultSet rs = query(select);
		rs.next();
		levelNo = rs.getInt(1);
		rs.close();
		
		insertLevels(levelNo);
		
		unlockLevel(0);
	}
	
	
	public void resetLevels() throws SQLException{
		String deleteLevels = "DELETE FROM CAMPAIGN";
		execute(deleteLevels);
		
		insertLevels(0);
		
		unlockLevel(0);
	}
	
	public void insertLevels(int minimumLevelNumber) throws SQLException{
		int levelNo = minimumLevelNumber;
		String path = "/resources/maps/campaign_%d.xml";
		String formattedPath;
		while(this.getClass().getResource(formattedPath = String.format(path, levelNo)) != null){
			String insert = 
				"INSERT INTO CAMPAIGN VALUES ("+levelNo+",'"+formattedPath+"', 0);";
			execute(insert);
			levelNo++;
		}
	}
	
	public void unlockLevel(int levelID) throws SQLException{
		String unlock = "UPDATE CAMPAIGN SET UNLOCKED = 1 WHERE LEVEL_ID = "+levelID;
		execute(unlock);
	}
	
	public boolean isLevelUnlocked(int levelID) throws SQLException{
		ResultSet rs = query("SELECT UNLOCKED FROM CAMPAIGN WHERE LEVEL_ID = "+levelID);
		rs.first();
		int state = rs.getInt(1);
		rs.close();
		if(state == 1){
			return true;
		}
		return false;
	}
	
	public boolean levelExists(int levelID) throws SQLException{
		ResultSet rs = query("SELECT COUNT(LEVEL_ID) FROM CAMPAIGN WHERE LEVEL_ID = "+levelID);
		rs.first();
		int state = rs.getInt(1);
		rs.close();
		if(state == 1){
			return true;
		}
		return false;
	}
}
